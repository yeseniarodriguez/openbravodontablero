/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.cargareembolso.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity are_i_reembolso (stored in table are_i_reembolso).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class are_i_reembolso extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "are_i_reembolso";
    public static final String ENTITY_NAME = "are_i_reembolso";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_FACTNUMESTAB = "factNumEstab";
    public static final String PROPERTY_FACTPUNTOEMISION = "factPuntoEmision";
    public static final String PROPERTY_FACTDOCUMENTO = "factDocumento";
    public static final String PROPERTY_FACTTAXID = "factTaxid";
    public static final String PROPERTY_ARECDOCTYPE = "aRECDoctype";
    public static final String PROPERTY_ARETIPO = "aRETipo";
    public static final String PROPERTY_ARETAXID = "aRETaxid";
    public static final String PROPERTY_ARENUMESTAB = "aRENumEstab";
    public static final String PROPERTY_AREPUNTOEMISION = "aREPuntoEmision";
    public static final String PROPERTY_AREDOCUMENTNO = "aREDocumentno";
    public static final String PROPERTY_AREFECHAEMISION = "aREFechaEmision";
    public static final String PROPERTY_AREAUTSRI = "aREAutSri";
    public static final String PROPERTY_AREBASECERO = "aREBaseCero";
    public static final String PROPERTY_AREBASEDOCE = "aREBaseDoce";
    public static final String PROPERTY_AREBASEEXENTO = "aREBaseExento";
    public static final String PROPERTY_AREVALORIVA = "aREValorIva";
    public static final String PROPERTY_AREVALORICE = "aREValorIce";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_MENSAJE = "mensaje";

    public are_i_reembolso() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_AREBASECERO, new BigDecimal(0));
        setDefaultValue(PROPERTY_AREBASEDOCE, new BigDecimal(0));
        setDefaultValue(PROPERTY_AREBASEEXENTO, new BigDecimal(0));
        setDefaultValue(PROPERTY_AREVALORIVA, new BigDecimal(0));
        setDefaultValue(PROPERTY_AREVALORICE, new BigDecimal(0));
        setDefaultValue(PROPERTY_PROCESAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getFactNumEstab() {
        return (String) get(PROPERTY_FACTNUMESTAB);
    }

    public void setFactNumEstab(String factNumEstab) {
        set(PROPERTY_FACTNUMESTAB, factNumEstab);
    }

    public String getFactPuntoEmision() {
        return (String) get(PROPERTY_FACTPUNTOEMISION);
    }

    public void setFactPuntoEmision(String factPuntoEmision) {
        set(PROPERTY_FACTPUNTOEMISION, factPuntoEmision);
    }

    public String getFactDocumento() {
        return (String) get(PROPERTY_FACTDOCUMENTO);
    }

    public void setFactDocumento(String factDocumento) {
        set(PROPERTY_FACTDOCUMENTO, factDocumento);
    }

    public String getFactTaxid() {
        return (String) get(PROPERTY_FACTTAXID);
    }

    public void setFactTaxid(String factTaxid) {
        set(PROPERTY_FACTTAXID, factTaxid);
    }

    public DocumentType getARECDoctype() {
        return (DocumentType) get(PROPERTY_ARECDOCTYPE);
    }

    public void setARECDoctype(DocumentType aRECDoctype) {
        set(PROPERTY_ARECDOCTYPE, aRECDoctype);
    }

    public String getARETipo() {
        return (String) get(PROPERTY_ARETIPO);
    }

    public void setARETipo(String aRETipo) {
        set(PROPERTY_ARETIPO, aRETipo);
    }

    public String getARETaxid() {
        return (String) get(PROPERTY_ARETAXID);
    }

    public void setARETaxid(String aRETaxid) {
        set(PROPERTY_ARETAXID, aRETaxid);
    }

    public String getARENumEstab() {
        return (String) get(PROPERTY_ARENUMESTAB);
    }

    public void setARENumEstab(String aRENumEstab) {
        set(PROPERTY_ARENUMESTAB, aRENumEstab);
    }

    public String getAREPuntoEmision() {
        return (String) get(PROPERTY_AREPUNTOEMISION);
    }

    public void setAREPuntoEmision(String aREPuntoEmision) {
        set(PROPERTY_AREPUNTOEMISION, aREPuntoEmision);
    }

    public String getAREDocumentno() {
        return (String) get(PROPERTY_AREDOCUMENTNO);
    }

    public void setAREDocumentno(String aREDocumentno) {
        set(PROPERTY_AREDOCUMENTNO, aREDocumentno);
    }

    public Date getAREFechaEmision() {
        return (Date) get(PROPERTY_AREFECHAEMISION);
    }

    public void setAREFechaEmision(Date aREFechaEmision) {
        set(PROPERTY_AREFECHAEMISION, aREFechaEmision);
    }

    public String getAREAutSri() {
        return (String) get(PROPERTY_AREAUTSRI);
    }

    public void setAREAutSri(String aREAutSri) {
        set(PROPERTY_AREAUTSRI, aREAutSri);
    }

    public BigDecimal getAREBaseCero() {
        return (BigDecimal) get(PROPERTY_AREBASECERO);
    }

    public void setAREBaseCero(BigDecimal aREBaseCero) {
        set(PROPERTY_AREBASECERO, aREBaseCero);
    }

    public BigDecimal getAREBaseDoce() {
        return (BigDecimal) get(PROPERTY_AREBASEDOCE);
    }

    public void setAREBaseDoce(BigDecimal aREBaseDoce) {
        set(PROPERTY_AREBASEDOCE, aREBaseDoce);
    }

    public BigDecimal getAREBaseExento() {
        return (BigDecimal) get(PROPERTY_AREBASEEXENTO);
    }

    public void setAREBaseExento(BigDecimal aREBaseExento) {
        set(PROPERTY_AREBASEEXENTO, aREBaseExento);
    }

    public BigDecimal getAREValorIva() {
        return (BigDecimal) get(PROPERTY_AREVALORIVA);
    }

    public void setAREValorIva(BigDecimal aREValorIva) {
        set(PROPERTY_AREVALORIVA, aREValorIva);
    }

    public BigDecimal getAREValorIce() {
        return (BigDecimal) get(PROPERTY_AREVALORICE);
    }

    public void setAREValorIce(BigDecimal aREValorIce) {
        set(PROPERTY_AREVALORICE, aREValorIce);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public String getMensaje() {
        return (String) get(PROPERTY_MENSAJE);
    }

    public void setMensaje(String mensaje) {
        set(PROPERTY_MENSAJE, mensaje);
    }

}
