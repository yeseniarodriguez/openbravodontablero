/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.importaciondatos;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity IDT_co_asiento_manual (stored in table IDT_co_asiento_manual).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class idtCoAsientoManual extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "IDT_co_asiento_manual";
    public static final String ENTITY_NAME = "IDT_co_asiento_manual";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CUENTACONTABLE = "cuentaContable";
    public static final String PROPERTY_DEBE = "debe";
    public static final String PROPERTY_HABER = "haber";
    public static final String PROPERTY_TERCERO = "tercero";
    public static final String PROPERTY_DESCRIPCION = "descripcion";
    public static final String PROPERTY_DESCRIPCIONLINEA = "descripcionLinea";
    public static final String PROPERTY_ERRORMSG = "errorMsg";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_SUCURSAL = "sucursal";
    public static final String PROPERTY_REFERENCIA = "referencia";

    public idtCoAsientoManual() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getCuentaContable() {
        return (String) get(PROPERTY_CUENTACONTABLE);
    }

    public void setCuentaContable(String cuentaContable) {
        set(PROPERTY_CUENTACONTABLE, cuentaContable);
    }

    public BigDecimal getDebe() {
        return (BigDecimal) get(PROPERTY_DEBE);
    }

    public void setDebe(BigDecimal debe) {
        set(PROPERTY_DEBE, debe);
    }

    public BigDecimal getHaber() {
        return (BigDecimal) get(PROPERTY_HABER);
    }

    public void setHaber(BigDecimal haber) {
        set(PROPERTY_HABER, haber);
    }

    public String getTercero() {
        return (String) get(PROPERTY_TERCERO);
    }

    public void setTercero(String tercero) {
        set(PROPERTY_TERCERO, tercero);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

    public String getDescripcionLinea() {
        return (String) get(PROPERTY_DESCRIPCIONLINEA);
    }

    public void setDescripcionLinea(String descripcionLinea) {
        set(PROPERTY_DESCRIPCIONLINEA, descripcionLinea);
    }

    public String getErrorMsg() {
        return (String) get(PROPERTY_ERRORMSG);
    }

    public void setErrorMsg(String errorMsg) {
        set(PROPERTY_ERRORMSG, errorMsg);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public String getSucursal() {
        return (String) get(PROPERTY_SUCURSAL);
    }

    public void setSucursal(String sucursal) {
        set(PROPERTY_SUCURSAL, sucursal);
    }

    public String getReferencia() {
        return (String) get(PROPERTY_REFERENCIA);
    }

    public void setReferencia(String referencia) {
        set(PROPERTY_REFERENCIA, referencia);
    }

}
