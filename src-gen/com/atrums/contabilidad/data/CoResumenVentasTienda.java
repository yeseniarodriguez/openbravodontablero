/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.contabilidad.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity co_resumen_ventas_tienda (stored in table co_resumen_ventas_tienda).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class CoResumenVentasTienda extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "co_resumen_ventas_tienda";
    public static final String ENTITY_NAME = "co_resumen_ventas_tienda";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_SUCURSAL = "sucursal";
    public static final String PROPERTY_TOTALVENTAS = "totalventas";
    public static final String PROPERTY_COSTO = "costo";
    public static final String PROPERTY_CONTRIBUCION = "contribucion";
    public static final String PROPERTY_FACTURAS = "facturas";
    public static final String PROPERTY_NC = "nc";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_IPF = "ipf";
    public static final String PROPERTY_PVF = "pvf";
    public static final String PROPERTY_EFECTIVO = "efectivo";
    public static final String PROPERTY_CHEQUE = "cheque";
    public static final String PROPERTY_TARJETA = "tarjeta";
    public static final String PROPERTY_DESCRIP = "descrip";
    public static final String PROPERTY_GRANTOTALVENTA = "grantotalventa";
    public static final String PROPERTY_FHASTA = "fhasta";
    public static final String PROPERTY_VAREFEC = "vAREfec";
    public static final String PROPERTY_VARCHE = "vARChe";
    public static final String PROPERTY_VARTAR = "vARTar";
    public static final String PROPERTY_PORCENTAJEVENTA = "porcentajeVenta";
    public static final String PROPERTY_MARGEN = "margen";

    public CoResumenVentasTienda() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public String getSucursal() {
        return (String) get(PROPERTY_SUCURSAL);
    }

    public void setSucursal(String sucursal) {
        set(PROPERTY_SUCURSAL, sucursal);
    }

    public BigDecimal getTotalventas() {
        return (BigDecimal) get(PROPERTY_TOTALVENTAS);
    }

    public void setTotalventas(BigDecimal totalventas) {
        set(PROPERTY_TOTALVENTAS, totalventas);
    }

    public BigDecimal getCosto() {
        return (BigDecimal) get(PROPERTY_COSTO);
    }

    public void setCosto(BigDecimal costo) {
        set(PROPERTY_COSTO, costo);
    }

    public BigDecimal getContribucion() {
        return (BigDecimal) get(PROPERTY_CONTRIBUCION);
    }

    public void setContribucion(BigDecimal contribucion) {
        set(PROPERTY_CONTRIBUCION, contribucion);
    }

    public BigDecimal getFacturas() {
        return (BigDecimal) get(PROPERTY_FACTURAS);
    }

    public void setFacturas(BigDecimal facturas) {
        set(PROPERTY_FACTURAS, facturas);
    }

    public BigDecimal getNc() {
        return (BigDecimal) get(PROPERTY_NC);
    }

    public void setNc(BigDecimal nc) {
        set(PROPERTY_NC, nc);
    }

    public BigDecimal getCantidad() {
        return (BigDecimal) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(BigDecimal cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public BigDecimal getIpf() {
        return (BigDecimal) get(PROPERTY_IPF);
    }

    public void setIpf(BigDecimal ipf) {
        set(PROPERTY_IPF, ipf);
    }

    public BigDecimal getPvf() {
        return (BigDecimal) get(PROPERTY_PVF);
    }

    public void setPvf(BigDecimal pvf) {
        set(PROPERTY_PVF, pvf);
    }

    public BigDecimal getEfectivo() {
        return (BigDecimal) get(PROPERTY_EFECTIVO);
    }

    public void setEfectivo(BigDecimal efectivo) {
        set(PROPERTY_EFECTIVO, efectivo);
    }

    public BigDecimal getCheque() {
        return (BigDecimal) get(PROPERTY_CHEQUE);
    }

    public void setCheque(BigDecimal cheque) {
        set(PROPERTY_CHEQUE, cheque);
    }

    public BigDecimal getTarjeta() {
        return (BigDecimal) get(PROPERTY_TARJETA);
    }

    public void setTarjeta(BigDecimal tarjeta) {
        set(PROPERTY_TARJETA, tarjeta);
    }

    public String getDescrip() {
        return (String) get(PROPERTY_DESCRIP);
    }

    public void setDescrip(String descrip) {
        set(PROPERTY_DESCRIP, descrip);
    }

    public BigDecimal getGrantotalventa() {
        return (BigDecimal) get(PROPERTY_GRANTOTALVENTA);
    }

    public void setGrantotalventa(BigDecimal grantotalventa) {
        set(PROPERTY_GRANTOTALVENTA, grantotalventa);
    }

    public Date getFhasta() {
        return (Date) get(PROPERTY_FHASTA);
    }

    public void setFhasta(Date fhasta) {
        set(PROPERTY_FHASTA, fhasta);
    }

    public BigDecimal getVAREfec() {
        return (BigDecimal) get(PROPERTY_VAREFEC);
    }

    public void setVAREfec(BigDecimal vAREfec) {
        set(PROPERTY_VAREFEC, vAREfec);
    }

    public BigDecimal getVARChe() {
        return (BigDecimal) get(PROPERTY_VARCHE);
    }

    public void setVARChe(BigDecimal vARChe) {
        set(PROPERTY_VARCHE, vARChe);
    }

    public BigDecimal getVARTar() {
        return (BigDecimal) get(PROPERTY_VARTAR);
    }

    public void setVARTar(BigDecimal vARTar) {
        set(PROPERTY_VARTAR, vARTar);
    }

    public BigDecimal getPorcentajeVenta() {
        return (BigDecimal) get(PROPERTY_PORCENTAJEVENTA);
    }

    public void setPorcentajeVenta(BigDecimal porcentajeVenta) {
        set(PROPERTY_PORCENTAJEVENTA, porcentajeVenta);
    }

    public BigDecimal getMargen() {
        return (BigDecimal) get(PROPERTY_MARGEN);
    }

    public void setMargen(BigDecimal margen) {
        set(PROPERTY_MARGEN, margen);
    }

}
