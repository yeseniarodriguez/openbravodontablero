/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.contabilidad.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity co_detalle_ventas_producto (stored in table co_detalle_ventas_producto).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class CoDetalleVentasProducto extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "co_detalle_ventas_producto";
    public static final String ENTITY_NAME = "co_detalle_ventas_producto";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_SUCURSAL = "sucursal";
    public static final String PROPERTY_VENTAS = "ventas";
    public static final String PROPERTY_TABLEROS = "tableros";
    public static final String PROPERTY_BORDOS = "bordos";
    public static final String PROPERTY_HERRAJES = "herrajes";
    public static final String PROPERTY_MADERA = "madera";
    public static final String PROPERTY_ADHESIVOS = "adhesivos";
    public static final String PROPERTY_OTROS = "otros";
    public static final String PROPERTY_SERVICIOS = "servicios";
    public static final String PROPERTY_DESCRIP = "descrip";
    public static final String PROPERTY_AGLOMERADO = "aglomerado";
    public static final String PROPERTY_TRIPLEX = "triplex";
    public static final String PROPERTY_MDF = "mdf";
    public static final String PROPERTY_VOLAGLOMERADO = "vOLAglomerado";
    public static final String PROPERTY_VOLTRIPLEX = "vOLTriplex";
    public static final String PROPERTY_VOLMDF = "vOLMdf";
    public static final String PROPERTY_TOTALVOL = "totalvol";

    public CoDetalleVentasProducto() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public String getSucursal() {
        return (String) get(PROPERTY_SUCURSAL);
    }

    public void setSucursal(String sucursal) {
        set(PROPERTY_SUCURSAL, sucursal);
    }

    public BigDecimal getVentas() {
        return (BigDecimal) get(PROPERTY_VENTAS);
    }

    public void setVentas(BigDecimal ventas) {
        set(PROPERTY_VENTAS, ventas);
    }

    public BigDecimal getTableros() {
        return (BigDecimal) get(PROPERTY_TABLEROS);
    }

    public void setTableros(BigDecimal tableros) {
        set(PROPERTY_TABLEROS, tableros);
    }

    public BigDecimal getBordos() {
        return (BigDecimal) get(PROPERTY_BORDOS);
    }

    public void setBordos(BigDecimal bordos) {
        set(PROPERTY_BORDOS, bordos);
    }

    public BigDecimal getHerrajes() {
        return (BigDecimal) get(PROPERTY_HERRAJES);
    }

    public void setHerrajes(BigDecimal herrajes) {
        set(PROPERTY_HERRAJES, herrajes);
    }

    public BigDecimal getMadera() {
        return (BigDecimal) get(PROPERTY_MADERA);
    }

    public void setMadera(BigDecimal madera) {
        set(PROPERTY_MADERA, madera);
    }

    public BigDecimal getAdhesivos() {
        return (BigDecimal) get(PROPERTY_ADHESIVOS);
    }

    public void setAdhesivos(BigDecimal adhesivos) {
        set(PROPERTY_ADHESIVOS, adhesivos);
    }

    public BigDecimal getOtros() {
        return (BigDecimal) get(PROPERTY_OTROS);
    }

    public void setOtros(BigDecimal otros) {
        set(PROPERTY_OTROS, otros);
    }

    public BigDecimal getServicios() {
        return (BigDecimal) get(PROPERTY_SERVICIOS);
    }

    public void setServicios(BigDecimal servicios) {
        set(PROPERTY_SERVICIOS, servicios);
    }

    public String getDescrip() {
        return (String) get(PROPERTY_DESCRIP);
    }

    public void setDescrip(String descrip) {
        set(PROPERTY_DESCRIP, descrip);
    }

    public BigDecimal getAglomerado() {
        return (BigDecimal) get(PROPERTY_AGLOMERADO);
    }

    public void setAglomerado(BigDecimal aglomerado) {
        set(PROPERTY_AGLOMERADO, aglomerado);
    }

    public BigDecimal getTriplex() {
        return (BigDecimal) get(PROPERTY_TRIPLEX);
    }

    public void setTriplex(BigDecimal triplex) {
        set(PROPERTY_TRIPLEX, triplex);
    }

    public BigDecimal getMdf() {
        return (BigDecimal) get(PROPERTY_MDF);
    }

    public void setMdf(BigDecimal mdf) {
        set(PROPERTY_MDF, mdf);
    }

    public BigDecimal getVOLAglomerado() {
        return (BigDecimal) get(PROPERTY_VOLAGLOMERADO);
    }

    public void setVOLAglomerado(BigDecimal vOLAglomerado) {
        set(PROPERTY_VOLAGLOMERADO, vOLAglomerado);
    }

    public BigDecimal getVOLTriplex() {
        return (BigDecimal) get(PROPERTY_VOLTRIPLEX);
    }

    public void setVOLTriplex(BigDecimal vOLTriplex) {
        set(PROPERTY_VOLTRIPLEX, vOLTriplex);
    }

    public BigDecimal getVOLMdf() {
        return (BigDecimal) get(PROPERTY_VOLMDF);
    }

    public void setVOLMdf(BigDecimal vOLMdf) {
        set(PROPERTY_VOLMDF, vOLMdf);
    }

    public BigDecimal getTotalvol() {
        return (BigDecimal) get(PROPERTY_TOTALVOL);
    }

    public void setTotalvol(BigDecimal totalvol) {
        set(PROPERTY_TOTALVOL, totalvol);
    }

}
