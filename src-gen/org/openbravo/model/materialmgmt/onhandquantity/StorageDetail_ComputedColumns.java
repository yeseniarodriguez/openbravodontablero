/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package org.openbravo.model.materialmgmt.onhandquantity;

import java.math.BigDecimal;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Virtual entity class to hold computed columns for entity MaterialMgmtStorageDetail.
 *
 * NOTE: This class should not be instantiated directly.
 */
public class StorageDetail_ComputedColumns extends BaseOBObject implements ClientEnabled, OrganizationEnabled {
    private static final long serialVersionUID = 1L;
    public static final String ENTITY_NAME = "StorageDetail_ComputedColumns";
    
    public static final String PROPERTY_ATECCOPRICELIST = "ateccoPricelist";
    public static final String PROPERTY_ATECCOPRICELISTIVA = "ateccoPricelistIva";
    public static final String PROPERTY_ATECCOPRICELISTTAR = "ateccoPricelisttar";
    public static final String PROPERTY_ATECCOPRICELISTTARIVA = "ateccoPricelisttarIva";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public BigDecimal getAteccoPricelist() {
      return (BigDecimal) get(PROPERTY_ATECCOPRICELIST);
    }

    public void setAteccoPricelist(BigDecimal ateccoPricelist) {
      set(PROPERTY_ATECCOPRICELIST, ateccoPricelist);
    }
    public BigDecimal getAteccoPricelistIva() {
      return (BigDecimal) get(PROPERTY_ATECCOPRICELISTIVA);
    }

    public void setAteccoPricelistIva(BigDecimal ateccoPricelistIva) {
      set(PROPERTY_ATECCOPRICELISTIVA, ateccoPricelistIva);
    }
    public BigDecimal getAteccoPricelisttar() {
      return (BigDecimal) get(PROPERTY_ATECCOPRICELISTTAR);
    }

    public void setAteccoPricelisttar(BigDecimal ateccoPricelisttar) {
      set(PROPERTY_ATECCOPRICELISTTAR, ateccoPricelisttar);
    }
    public BigDecimal getAteccoPricelisttarIva() {
      return (BigDecimal) get(PROPERTY_ATECCOPRICELISTTARIVA);
    }

    public void setAteccoPricelisttarIva(BigDecimal ateccoPricelisttarIva) {
      set(PROPERTY_ATECCOPRICELISTTARIVA, ateccoPricelisttarIva);
    }
    public Client getClient() {
      return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
      set(PROPERTY_CLIENT, client);
    }
    public Organization getOrganization() {
      return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
      set(PROPERTY_ORGANIZATION, organization);
    }
}
