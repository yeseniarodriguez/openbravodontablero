package com.atrums.nomina.ad_process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URLStreamHandler;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.omg.CORBA.portable.OutputStream;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

public class NoEnviarUsuarioTwiins implements Process {

	static Logger log4j = Logger.getLogger(NoEnviarUsuarioTwiins.class);
	final OBError msg = new OBError();

	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub

		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();

		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(), varsAux.getOrg());

		String strNoContratoEmpleadoId = null;
		String token = null;

        token = solicitudToken(conn);
		
		try {
			if (bundle.getParams().get("NO_Contrato_Empleado_ID") != null) {
				strNoContratoEmpleadoId = bundle.getParams().get("NO_Contrato_Empleado_ID").toString();
				System.out.println("Contrato Empleado: "+strNoContratoEmpleadoId);

				if (enviarUsuario(strNoContratoEmpleadoId, token, conn)) {
					log4j.info(this.msg.getMessage());
					// QUIPDatoswsData.methodUpdateMensaje(conn, this.msg.getMessage(),
					// strCBpartnerId);
					bundle.setResult(this.msg);
				} else {
					log4j.info(this.msg.getMessage());
					// QUIPDatoswsData.methodUpdateMensaje(conn, this.msg.getMessage(),
					// strCBpartnerId);
					bundle.setResult(this.msg);
				}
			}

		} catch (Exception ex) {
			// TODO: handle exception
			this.msg.setType("Error");
			this.msg.setMessage(ex.getMessage());
			this.msg.setTitle("@Error@");
			bundle.setResult(this.msg);
			return;
		} finally {
			strNoContratoEmpleadoId = null;

			OBDal.getInstance().rollbackAndClose();
		}

	}

	public String solicitudToken(ConnectionProvider conn) throws IOException {
		String token = null;
		String status = null;
		String message = null;

		try {
			/********************************** URL pruebas *******************************/
			URL url = new URL("http://dontableroprueba.twiinshrm.com/talento_api/index.php/loginexterno_controller");
			
			/********************************** URL produccion ****************************/
			//URL url = new URL("https://dontablero.twiinshrm.com/talento_api/index.php/loginexterno_controller");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			
			/********************************* Usuario pruebas ******************************/
			//String jsonInputString = "{\"usuarioUser\": \"Prueba\", \"usuarioPassword\": \"Pru3b4\"}";
			
			/********************************* Usuario produccion ***************************/
			String jsonInputString = "{\"usuarioUser\": \"Atrums\", \"usuarioPassword\": \"HB3vlasIBLL5\"}";
			
			System.out.println(jsonInputString);

			try (java.io.OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}

			try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());

				String[] elementos = response.toString().replace("{", "").replace("}", "").replace("[", "")
						.replace("]", "").replace("\"", "").split(",");

				for (int i = 0; i < elementos.length; i++) {
					String[] elementosRes = elementos[i].split(":");
					for (int j = 0; j < elementosRes.length; j++) {
						if (elementosRes[j].toString().equals("status")) {
							status = elementosRes[j + 1];
						}
						if (elementosRes[j].toString().equals("message")) {
							message = elementosRes[j + 1];
						}
						if (elementosRes[j].toString().equals("token")) {
							token = elementosRes[j + 1];
						}
					}
					elementosRes = null;
				}

				System.out.println("status: " + status);
				System.out.println("message: " + message);
				System.out.println("Token: " + token);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return token;
	}

	public boolean enviarUsuario(String strNoContratoEmpleadoId, String token, ConnectionProvider conn) {

		NOUsuariosTwiinsData[] usuariosDT = null;
		String strOperacionInsert = null;
		String strUsuario = null;
		String status = null;
		String message = null;
		String usuario = null;
		String password = null;
		String estado = null;
		boolean resultado = false;

		OBContext.setAdminMode(true);

		try {
			usuariosDT = NOUsuariosTwiinsData.select(conn,strNoContratoEmpleadoId);

			if (usuariosDT.length == 0 || usuariosDT.equals(null)) {
				resultado = false;
				this.msg.setType("Error");
				this.msg.setMessage("No existen usuarios para enviar...");
				this.msg.setTitle("@Error@");
				return false;
			}else {
		

			for (int k = 0; k < usuariosDT.length; k++) {
				
				/*************************** URL Pruebas ********************************/
				/*URL url = new URL(null, "https://apiprueba.twiinshrm.com/v2/dontableroprueba/usuario/ingresar",
						new sun.net.www.protocol.https.Handler());
						
				  strUsuario = "Prueba";*/
				
				/*************************** URL Produccion ********************************/
				URL url = new URL(null, "https://api.twiinshrm.com/v2/dontablero/usuario/ingresar",
						new sun.net.www.protocol.https.Handler());

				strUsuario = "Atrums";
				/***************************************************************************/
				
				strOperacionInsert = "token/usuarioUser/uoe_empresa/dge_tipoidentificacion/dge_noidentificacion/dge_primernombre/dge_apellidopaterno/dge_email/dge_telefono/uoe_departamento/uoe_area/uoe_jerarquia/uoe_cargo/ufe_oficinalocal/dpe_genero/dpe_estadocivil/dpe_fecnacimiento/dpe_emailpersonal/dpe_ciudadnacimiento/dpe_paisnacimiento/dd_provincia/dd_canton/dd_calleprincipal/dd_telfconvencional/dd_telfcelular/ee_fechaingreso/ee_estado/dpt_sueldobase/dpt_estanomina/dps_formapago/dps_banco/dps_tipocuenta/dps_numerocuenta/den_regimenlaboral/dm_discapacidad";
				
				strOperacionInsert = strOperacionInsert.replace("token", "\"token\": \"" + token + "\",");
				
				strOperacionInsert = strOperacionInsert.replace("usuarioUser",
						"\"usuarioUser\": \"" + strUsuario + "\",");
				
				strOperacionInsert = strOperacionInsert.replace("uoe_empresa",
						"\"UOE_empresa\": \"" + usuariosDT[k].uoeEmpresa.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_tipoidentificacion",
						"\"DGE_tipoIdentificacion\": \"" + usuariosDT[k].dgeTipoidentificacion.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_noidentificacion",
						"\"DGE_noIdentificacion\": \"" + usuariosDT[k].dgeNoidentificacion.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_primernombre",
						"\"DGE_primerNombre\": \"" + usuariosDT[k].dgePrimernombre.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_apellidopaterno",
						"\"DGE_apellidoPaterno\": \"" + usuariosDT[k].dgeApellidopaterno.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_email",
						"\"DGE_email\": \"" + usuariosDT[k].dgeEmail.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dge_telefono",
						"\"DGE_telefono\": \"" + usuariosDT[k].dgeTelefono.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("uoe_departamento",
						"\"UOE_departamento\": \"" + usuariosDT[k].uoeDepartamento.trim().replace("/", "*") + "\",");
				strOperacionInsert = strOperacionInsert.replace("uoe_area",
						"\"UOE_area\": \"" + usuariosDT[k].uoeArea.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("uoe_jerarquia",
						"\"UOE_jerarquia\": \"" + usuariosDT[k].uoeJerarquia.trim().replace("/", "*") + "\",");
				strOperacionInsert = strOperacionInsert.replace("uoe_cargo",
						"\"UOE_cargo\": \"" + usuariosDT[k].uoeCargo.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("ufe_oficinalocal",
						"\"UFE_oficinaLocal\": \"" + usuariosDT[k].ufeOficinalocal.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_genero",
						"\"DPE_genero\": \"" + usuariosDT[k].dpeGenero.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_estadocivil",
						"\"DPE_estadoCivil\": \"" + usuariosDT[k].dpeEstadocivil.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_fecnacimiento",
						"\"DPE_fecNacimiento\": \"" + usuariosDT[k].dpeFecnacimiento.trim().replace("/", "*") + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_emailpersonal",
						"\"DPE_emailPersonal\": \"" + usuariosDT[k].dpeEmailpersonal.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_ciudadnacimiento",
						"\"DPE_ciudadNacimiento\": \"" + usuariosDT[k].dpeCiudadnacimiento.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpe_paisnacimiento",
						"\"DPE_paisNacimiento\": \"" + usuariosDT[k].dpePaisnacimiento.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dd_provincia",
						"\"DD_provincia\": \"" + usuariosDT[k].ddProvincia.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dd_canton",
						"\"DD_canton\": \"" + usuariosDT[k].ddCanton.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dd_calleprincipal",
						"\"DD_callePrincipal\": \"" + usuariosDT[k].ddCalleprincipal.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dd_telfconvencional",
						"\"DD_telfConvencional\": \"" + usuariosDT[k].ddTelfconvencional.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dd_telfcelular",
						"\"DD_telfCelular\": \"" + usuariosDT[k].ddTelfcelular.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("ee_fechaingreso",
						"\"EE_fechaIngreso\": \"" + usuariosDT[k].eeFechaingreso.trim().replace("/", "*") + "\",");
				strOperacionInsert = strOperacionInsert.replace("ee_estado",
						"\"EE_estado\": \"" + usuariosDT[k].eeEstado.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpt_sueldobase",
						"\"DPT_sueldoBase\": \"" + usuariosDT[k].dptSueldobase.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dpt_estanomina",
						"\"DPT_estaNomina\": \"" + usuariosDT[k].dptEstanomina.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dps_formapago",
						"\"DPS_formaPago\": \"" + usuariosDT[k].dpsFormapago.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dps_banco",
						"\"DPS_banco\": \"" + usuariosDT[k].dpsBanco.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dps_tipocuenta",
						"\"DPS_tipoCuenta\": \"" + usuariosDT[k].dpsTipocuenta.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("dps_numerocuenta",
						"\"DPS_numeroCuenta\": \"" + usuariosDT[k].dpsNumerocuenta.trim() + "\",");
				strOperacionInsert = strOperacionInsert.replace("den_regimenlaboral",
						"\"DEN_regimenLaboral\": \"" + usuariosDT[k].denRegimenlaboral.trim() + "\",");		
                strOperacionInsert = strOperacionInsert.replace("dm_discapacidad",
						"\"DM_discapacidad\": \"" + usuariosDT[k].dmDiscapacidad.trim() + "\"");
				
				strOperacionInsert = "{" + strOperacionInsert.replace("/", "") + "}";
				strOperacionInsert = strOperacionInsert.replace("*", "/");

				System.out.println(strOperacionInsert);
				
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
			      /*con.setDoOutput(true);
			      con.setDoInput(true);
			      con.setInstanceFollowRedirects(false);
			 	  con.setRequestMethod("POST");*/
			 	  con.setRequestProperty("Content-Type", "application/json; utf-8");
			 	  con.setRequestProperty("Accept", "application/json");
			 	  con.setDoOutput(true);

			 	   try(java.io.OutputStream os = con.getOutputStream()) {
					    byte[] input = strOperacionInsert.getBytes("utf-8");
					    os.write(input, 0, input.length);	
					}

					try(BufferedReader br = new BufferedReader(
							  new InputStreamReader(con.getInputStream(), "utf-8"))) {
							    StringBuilder response = new StringBuilder();
							    String responseLine = null;
							    while ((responseLine = br.readLine()) != null) {
							        response.append(responseLine.trim()); 
							    }
							

					String varResponse = response.toString();
					System.out.println(varResponse);
					NOUsuariosTwiinsData.actualizaTwiinsTercero(conn, varResponse, "Y",
							usuariosDT[k].cBpartnerId);

					String[] elementos = varResponse.replace("{", "").replace("}", "").replace("[", "")
							.replace("]", "").replace("\"", "").split(",");

					for (int i = 0; i < elementos.length; i++) {
						String[] elementosRes = elementos[i].split(":");
						for (int j = 0; j < elementosRes.length; j++) {
							if (elementosRes[j].toString().equals("status")) {
								status = elementosRes[j + 1];
							}
							if (elementosRes[j].toString().equals("message")) {
								message = elementosRes[j + 1];	
							}
							if (elementosRes[j].toString().equals("usuario")) {
								usuario = elementosRes[j + 1];
							}
							if (elementosRes[j].toString().equals("password")) {
								password = elementosRes[j + 1];
							}
							if (elementosRes[j].toString().equals("estado")) {
								estado = elementosRes[j + 1];
							}
						}
						elementosRes = null;
					}
					
					

					elementos = null;

					String guardaMensaje = null;
					String condicion = "Y";

					if (varResponse.length() > 255) {
						guardaMensaje = varResponse;
						guardaMensaje = guardaMensaje.substring(0, 255);
					} else {
						guardaMensaje = varResponse;
					}
					
					if (!usuario.toString().equals(null) && !usuario.toString().equals("")) {
						guardaMensaje = ("Usuario:" + usuario + " Password:" + password + " Estado:" + estado + "Fecha:"
								+ new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
						if (guardaMensaje.length() > 255) {
							guardaMensaje = guardaMensaje.substring(0, 255);
						}
						condicion = "N";
						
						NOUsuariosTwiinsData.actualizaTwiinsTercero(conn, guardaMensaje, condicion,
								usuariosDT[k].cBpartnerId);
						guardaMensaje =null;
						
					}else {
						condicion = "Y";
						
						NOUsuariosTwiinsData.actualizaTwiinsTercero(conn, guardaMensaje, condicion,
								usuariosDT[k].cBpartnerId);	
						guardaMensaje =null;
					}
					
					System.out.println("status: " + status);
					System.out.println("message: " + message);
					
					guardaMensaje =null;

					if (status.equals("200")) {
						resultado = true;
					} else {
						resultado = false;
					}

				}

			}
			

			this.msg.setType("Success");
			this.msg.setTitle("Mensaje");
			this.msg.setMessage("Información enviada a TWIINS");
			resultado = true;
			return true;
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			// TODO: handle exception
			this.msg.setType("Error");
			this.msg.setMessage(ex.getMessage());
			this.msg.setTitle("@Error@");
			return false;
		} finally {
			strOperacionInsert = null;
			usuariosDT = null;
			OBContext.restorePreviousMode();
		}

		return resultado;

	}
}
