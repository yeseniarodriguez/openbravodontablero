<?xml version="1.0" encoding="UTF-8" ?>
<!--
 ******************************************************************************
 * The contents of this file are subject to the   Compiere License  Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * You may obtain a copy of the License at http://www.compiere.org/license.html
 * Software distributed under the License is distributed on an  "AS IS"  basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * The Original Code is                  Compiere  ERP & CRM  Business Solution
 * The Initial Developer of the Original Code is Jorg Janke  and ComPiere, Inc.
 * Portions created by Jorg Janke are Copyright (C) 1999-2001 Jorg Janke, parts
 * created by ComPiere are Copyright (C) ComPiere, Inc.;   All Rights Reserved.
 * Contributor(s): Openbravo SLU
 * Contributions are Copyright (C) 2001-2011 Openbravo S.L.U.
 ******************************************************************************
-->



<SqlClass name="NOCBPartnerData" package="com.atrums.nomina.ad_process">
 
  <SqlMethod name="select" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
		select upper(suc.name) as organizacion,
		       upper(case when pe.periodno = 1 then 'Enero'
		            when pe.periodno = 2 then 'Febrero'
		            when pe.periodno = 3 then 'Marzo'
		            when pe.periodno = 4 then 'Abril'
		            when pe.periodno = 5 then 'Mayo'
		            when pe.periodno = 6 then 'Junio'
		            when pe.periodno = 7 then 'Julio'
		            when pe.periodno = 8 then 'Agosto'
		            when pe.periodno = 9 then 'Septiembre'
		            when pe.periodno = 10 then 'Octubre'
		            when pe.periodno = 11 then 'Noviembre'
		            when pe.periodno = 12 then 'Diciembre'
		       end) as mes,
		       y.year as anio,
		       rp.no_rol_pago_provision_id as rolpago, 
		       em.name as tercero,  
		       us.email as email,
		       round(rp.total_neto, 2)  as valorPagoServicio
		  from no_rol_pago_provision rp, ad_user us, c_period pe, c_bpartner em, ad_org suc,
		       c_year y             
		 where us.c_bpartner_id = rp.c_bpartner_id
		   and pe.c_period_id = rp.c_period_id
		   and pe.c_year_id = y.c_year_id
		   and rp.c_bpartner_id = em.c_bpartner_id
		   and us.em_no_rol_mail = 'Y'
		   and rp.ispago='Y'
		   and rp.isactive = 'Y' 
		   and suc.ad_org_id = rp.ad_org_id
		   and rp.no_rol_pago_provision_id = ?
      ]]>
    </Sql>
    <Parameter name="rolpagoId"/>
  </SqlMethod>
  
  
  <SqlMethod name="selectEmployee" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      
      	SELECT part.c_bpartner_id as tercero
      	  FROM C_BPARTNER part
		LEFT OUTER JOIN AD_USER  ON part.c_bpartner_id = AD_USER.c_bpartner_id
		WHERE  AD_USER.c_bpartner_id IS NULL
      ]]>
    </Sql>
  </SqlMethod>
  
  
   <SqlMethod name="selectPagoServicio" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
		select upper(c.name) as organizacion,
		       upper(case when to_char(date(p.startdate) - interval '1 month', 'mm') = '01' then 'Enero'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '02' then 'Febrero'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '03' then 'Marzo'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '04' then 'Abril'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '05' then 'Mayo'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '06' then 'Junio'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '07' then 'Julio'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '08' then 'Agosto'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '09' then 'Septiembre'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '10' then 'Octubre'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '11' then 'Noviembre'
            when to_char(date(p.startdate) - interval '1 month', 'mm') = '12' then 'Diciembre'
       end) as mes,
               extract(year from date(p.startdate) - interval '1 month') as anio,
		       rq.no_registra_quinc_line_id as rolpago,
		       bp.name as tercero, 
		       us.email as email,
		       rq.valor as valorPagoServicio
		  from no_registra_quincena q, no_registra_quinc_line rq, ad_client cl, ad_org suc,
		       c_period p, c_year y, c_bpartner bp, no_contrato_empleado ce, ad_user us,
               ad_org c
		 where q.no_registra_quincena_id = rq.no_registra_quincena_id
		   and cl.ad_client_id = q.ad_client_id
		   and suc.ad_org_id = q.ad_org_id
           and ce.ad_org_id = c.ad_org_id
		   and q.c_period_id = p.c_period_id
		   and p.c_year_id = y.c_year_id
		   and bp.c_bpartner_id = rq.c_bpartner_id
		   and ce.c_bpartner_id = rq.c_bpartner_id
		   and us.c_bpartner_id = bp.c_bpartner_id
		   and rq.no_registra_quinc_line_id = ?
      ]]>
    </Sql>
    <Parameter name="rolpagoId"/>
  </SqlMethod>
  
  
</SqlClass>
