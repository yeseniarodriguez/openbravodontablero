<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RPT_RBInventarioValorado" pageWidth="750" pageHeight="28842" columnWidth="710" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="16b4aea3-6135-4ab8-89f7-60b30e5ad4ab">
	<property name="ireport.zoom" value="1.3310000000000064"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="20"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["http://localhost:8008/openbravoaf/web"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_ORG" class="java.lang.String"/>
	<parameter name="DATE_FROM" class="java.util.Date"/>
	<parameter name="DATE_TO" class="java.util.Date"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="PRODUCTO" class="java.lang.String"/>
	<parameter name="AUX_PRODUCTO" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{PRODUCTO}==null || $P{PRODUCTO}.equals("")) ? "" : "and t.m_product_id = '" + $P{PRODUCTO} + "'"]]></defaultValueExpression>
	</parameter>
	<parameter name="CATEGORIA" class="java.lang.String"/>
	<parameter name="AUX_CATEGORIA" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{CATEGORIA}==null || $P{CATEGORIA}.equals( "" )) ? "" : "and t.m_product_id IN (select m_product_id from m_product where m_product_category_id = '" + $P{CATEGORIA}+ "')"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select a.m_transaction_id,
        coalesce(a.Sucursal,'') as Sucursal,
        coalesce(a.categoria,'') as categoria,
        a.movementdate,
        a.movementtype,
        coalesce(a.producto,'') as producto,
        coalesce(a.entrada,0) as entrada,
        coalesce(a.salida,0) as salida,
        coalesce(a.unitcost,0) as unitcost,
        coalesce(a.cost,0) as cost,
        coalesce((sum(amtsourcedr)-sum(amtsourcecr)),0) as saldo,
        'C32139D1C76941788E92B8D13F5174C2' AS org_id
from (select o.name as Sucursal,
            (select name from m_product_category where m_product_category_id IN (select m_product_category_id from m_product where m_product_id = t.m_product_id)) as categoria,
            t.movementdate,
            case when t.movementtype='C-' then 'Despacho' when t.movementtype='V+' then 'Ingreso' when t.movementtype='I+' then 'Inventario +' when t.movementtype='I-' then 'Inventario -' end as movementtype,
            p.name as producto,
            case when t.movementqty > 0 then t.movementqty else 0 end as entrada,
            case when t.movementqty < 0 then t.movementqty else 0 end as salida,
            (select coalesce(cost,0) from m_costing where m_product_id = t.m_product_id and datefrom <= t.movementdate and dateto >= t.movementdate order by dateto desc limit 1) as unitcost,
            (select coalesce(cost,0) from m_costing where m_product_id = t.m_product_id and datefrom <= t.movementdate and dateto >= t.movementdate order by dateto desc limit 1) * t.movementqty as cost,
            p.m_product_id,
            t.m_transaction_id
        from m_transaction t
             join ad_org o on (t.ad_org_id = o.ad_org_id)
             left join m_product p on (t.m_product_id = p.m_product_id)
        where t.ad_client_id IN ('60BCDFE538D94442895A861EF02829D7')
        and t.movementtype in ('V+','C-', 'I+', 'I-')
        and date(t.movementdate) >= date($P{DATE_FROM})
        and date(t.movementdate) <= date($P{DATE_TO})
order by 3, 6, 4) a
left join fact_acct fa on (fa.m_product_id = a.m_product_id and acctvalue =
        (select ev.value from m_product_acct pa
                              inner join c_validcombination v on (pa.p_asset_acct = v.c_validcombination_id)
                              inner join c_elementvalue ev on (v.account_id = ev.c_elementvalue_id)
                         where pa.m_product_id = a.m_product_id
                         and pa.c_acctschema_id IN (select c_acctschema_id from c_acctschema where isactive = 'Y'))
        and date(fa.dateacct) < date($P{DATE_FROM}))
group by 1,2,3,4,5,6,7,8,9,10
order by 3, 6, 4]]>
	</queryString>
	<field name="sucursal" class="java.lang.String"/>
	<field name="categoria" class="java.lang.String"/>
	<field name="movementdate" class="java.sql.Timestamp"/>
	<field name="movementtype" class="java.lang.String"/>
	<field name="producto" class="java.lang.String"/>
	<field name="entrada" class="java.math.BigDecimal"/>
	<field name="salida" class="java.math.BigDecimal"/>
	<field name="unitcost" class="java.math.BigDecimal"/>
	<field name="cost" class="java.math.BigDecimal"/>
	<field name="saldo" class="java.math.BigDecimal"/>
	<field name="org_id" class="java.lang.String"/>
	<variable name="cost_sum" class="java.math.BigDecimal" resetType="Group" resetGroup="producto" calculation="Sum">
		<variableExpression><![CDATA[$F{cost}]]></variableExpression>
	</variable>
	<variable name="Total_Prod" class="java.math.BigDecimal" resetType="Group" resetGroup="producto">
		<variableExpression><![CDATA[$F{saldo}.add( $V{cost_sum} )]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="Total_Cat" class="java.math.BigDecimal" resetType="Group" resetGroup="categoria" incrementType="Group" incrementGroup="producto" calculation="Sum">
		<variableExpression><![CDATA[$V{Total_Prod}]]></variableExpression>
	</variable>
	<variable name="Total_Gen" class="java.math.BigDecimal" incrementType="Group" incrementGroup="categoria" calculation="Sum">
		<variableExpression><![CDATA[$V{Total_Cat}]]></variableExpression>
	</variable>
	<group name="categoria">
		<groupExpression><![CDATA[$F{categoria}]]></groupExpression>
		<groupHeader>
			<band height="26">
				<textField>
					<reportElement style="SubTitle" x="0" y="1" width="396" height="25" forecolor="#006633" uuid="d3252410-26bc-44f7-a26e-b4b4f72c6761"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{categoria}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="710" height="1" uuid="6fffdc51-ed04-489d-8f7c-b2aee1f21bf7"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="25">
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="396" y="0" width="247" height="25" forecolor="#006633" uuid="a874a806-99bf-48cf-9153-254c21f4beac"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="13" isUnderline="true"/>
					</textElement>
					<text><![CDATA[Total Categoría $:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="643" y="0" width="67" height="25" forecolor="#006633" uuid="b229b613-787e-43ef-906a-3045eaf61b1b"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="13" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Total_Cat}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="producto">
		<groupExpression><![CDATA[$F{producto}]]></groupExpression>
		<groupHeader>
			<band height="52">
				<textField>
					<reportElement style="SubTitle" x="36" y="1" width="448" height="20" forecolor="#006600" uuid="6d99af8b-5619-488d-8ae7-d800c1a4b664"/>
					<textElement verticalAlignment="Middle">
						<font size="16" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{producto}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement style="Column header" x="0" y="37" width="94" height="15" forecolor="#000000" uuid="e82ea509-899f-494f-9bad-72e41ce922d9"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Sucursal]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="94" y="37" width="80" height="15" forecolor="#000000" uuid="276ba816-fabb-4a90-89ef-3d6135c22541"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Fecha]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="174" y="37" width="84" height="15" forecolor="#000000" uuid="d166c084-aad3-4446-aa05-97e45869db7f"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Movimiento]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="258" y="37" width="69" height="15" forecolor="#000000" uuid="35ec2004-5567-485b-9610-2ab0dfe184ab"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Entrada]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="327" y="37" width="69" height="15" forecolor="#000000" uuid="99b9b8bf-1a10-4ac6-a8c7-686893cee0fa"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Salida]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="396" y="37" width="88" height="15" forecolor="#000000" uuid="ff5b6007-9a0a-4aa8-b887-48cd79b8a180"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Costo Unit.]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="396" y="21" width="178" height="16" forecolor="#000000" uuid="20a9bab8-2faa-454f-ac36-605ec14850cc"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Saldo Anterior $:]]></text>
				</staticText>
				<rectangle>
					<reportElement mode="Opaque" x="0" y="1" width="36" height="20" forecolor="#CCCCCC" backcolor="#CCCCCC" uuid="ecbbd30e-0366-4ae4-8589-80389f84c478"/>
				</rectangle>
				<textField pattern="#,##0.00">
					<reportElement style="Detail" x="574" y="21" width="69" height="16" uuid="93f475c2-f301-4ede-a5f5-61e5b82dd342"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{saldo}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="710" height="1" uuid="78a87eac-ed9d-425f-b621-28cc2e512ee1"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="35">
				<textField pattern="#,##0.00">
					<reportElement x="574" y="15" width="69" height="20" uuid="5980f890-51fc-44be-98aa-8ca582c4c3dd"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Total_Prod}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement style="Column header" x="396" y="15" width="178" height="20" forecolor="#000000" uuid="79831265-b0be-4eea-b405-69abde759f6c"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isItalic="false"/>
					</textElement>
					<text><![CDATA[Producto - Saldo Actual $:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="505" y="0" width="69" height="15" uuid="ed561c7f-00e5-465f-8668-8ad40a80f3da"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{cost_sum}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement style="Column header" x="396" y="0" width="109" height="15" forecolor="#000000" uuid="8553621a-8e8c-431e-977a-0f392c8ac6c6"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Movimientos: $]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="49" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="0" width="574" height="49" forecolor="#006600" uuid="d137fc14-fc6a-4d25-9d95-e1fa30607ae0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="25"/>
				</textElement>
				<text><![CDATA[Inventario - Valoración Movimientos (Detallado)]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="15" width="574" height="1" uuid="7f1b2fc1-492c-478f-b997-98ddcf79d770"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement style="Detail" x="0" y="0" width="94" height="15" uuid="2f832ef1-404a-4920-a39e-e0cda2342cb0"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{sucursal}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement style="Detail" x="94" y="0" width="80" height="15" uuid="760bf53d-9ee0-44a4-a3c7-f4fa9fbdb2f5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{movementdate}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="174" y="0" width="84" height="15" uuid="2396e7e7-9192-481d-87f6-a5ad7c138cfb"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{movementtype}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="258" y="0" width="69" height="15" uuid="6135f5b4-3c7d-4aad-aa28-81c65ca21d0c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{entrada}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="327" y="0" width="69" height="15" uuid="ddff5c20-32c9-4bac-bc51-af3db31c54f7"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{salida}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="396" y="0" width="88" height="15" uuid="8c7daba3-6f6c-4e2d-bf82-12897d45ee42"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{unitcost}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="505" y="0" width="69" height="15" uuid="56707942-323e-461a-8553-8e931687f559"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{cost}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="45" splitType="Stretch">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="710" height="1" uuid="7f06866c-3db0-4ec0-9f93-548335d5fa81"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement style="Column header" mode="Transparent" x="396" y="1" width="247" height="24" forecolor="#000000" uuid="fabbd1df-cf38-484b-b7cc-2aaff0eebe1c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="13" isUnderline="false"/>
				</textElement>
				<text><![CDATA[TOTAL GENERAL:]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="643" y="1" width="67" height="24" uuid="47b79002-911a-4196-b0ee-b68f38cdf8dd"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Total_Gen}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="574" y="0" width="69" height="20" uuid="aaa59d87-0fe3-4db9-ba0a-0081a9978418"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Pág. "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="643" y="0" width="67" height="20" uuid="1abc9e5c-99f9-4596-9f3a-e8d0f1bfd756"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement style="Column header" x="0" y="0" width="174" height="20" uuid="522025d4-2535-4413-9aef-c822f73d7ef2"/>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
