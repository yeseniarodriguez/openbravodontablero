package com.atrums.compras.montos.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.model.ATECCO_Temporal;

public class ATECCO_Validar extends HttpSecureAppServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig config) {
		super.init(config);
		boolHist = false;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (vars.commandIn("DEFAULT")) {
			String strProcessId = vars.getStringParameter("inpProcessId");
			String strWindow = vars.getStringParameter("inpwindowId");
			String strTab = vars.getStringParameter("inpTabId");
			String strKey = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
			printPage(response, vars, strKey, strWindow, strTab, strProcessId);
		} else if (vars.commandIn("SAVE")) {
			String strWindow = vars.getStringParameter("inpWindowId");
			String strOrder = vars.getStringParameter("inpcOrderId");
			String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
			String strTab = vars.getStringParameter("inpTabId");

			String strMetodo = vars.getStringParameter("inpMetodo");
			boolean descuentoPago = true;

			List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();

			String strMetodoNota = vars.getStringParameter("inpAplicaNotaCredito").equals("Y") ? "Y" : "N";

			if (strMetodoNota.equals("Y")) {
				String StrPagoNota = vars.getStringParameter("inpMontoPagoNotaCredito");

				ATECCO_Temporal newTemp = new ATECCO_Temporal();
				newTemp.setAmount(Double.parseDouble(StrPagoNota));
				newTemp.setTipoPago("Nota de Crédito".toUpperCase());
				temp.add(newTemp);
			}

			if (strMetodo.equals("Mixto") || strMetodo.equals("Retencion")) {

				String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

				if (strPagoEfectivo != null) {
					if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
						newTemp.setTipoPago("Efectivo".toUpperCase());
						temp.add(newTemp);
					}
				}

				String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

				if (strPagoElectronico != null) {
					if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoElectronico));
						newTemp.setTipoPago("Dinero Electrónico".toUpperCase());
						temp.add(newTemp);
					}
				}

				String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
				String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
				String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

				if (strPagoCheque != null) {
					if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoCheque));
						newTemp.setNro_cheque(strNroCheque);
						newTemp.setBanco_id(strBancoCheque);
						newTemp.setTipoPago("Cheque".toUpperCase());
						temp.add(newTemp);
					}
				}

				String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
				String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia").trim();
				String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

				if (strPagoTransferencia != null) { //YRO 15-05-2024
					if (Double.parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
						newTemp.setReferenceno(new BigInteger(strNroTransferencia.trim()).toString());
						newTemp.setBanco_id(strBancoTransferencia);
						newTemp.setTipoPago("Transferencia/Depósito".toUpperCase());
						temp.add(newTemp);
					}
					
					
				}

				String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
				String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
				String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
				String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

				if (strPagoTarjeta != null) {
					if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
						descuentoPago = false;
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
						newTemp.setReferenceno(strNroTarjeta);
						newTemp.setTarjeta_id(strBancoTarjeta);
						newTemp.setCondicion_id(strBancoCondicion);
						newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
						temp.add(newTemp);
					}
				}

			} else if (strMetodo.equals("Efectivo")) {
				String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

				if (strPagoEfectivo != null) {
					if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
						newTemp.setTipoPago("Efectivo".toUpperCase());
						temp.add(newTemp);
					}
				}
			} else if (strMetodo.equals("Electronico")) {
				String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

				if (strPagoElectronico != null) {
					if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoElectronico));
						newTemp.setTipoPago("Dinero Electrónico".toUpperCase());
						temp.add(newTemp);
					}
				}
			} else if (strMetodo.equals("Cheque")) {
				String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
				String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
				String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

				if (strPagoCheque != null) {
					if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoCheque));
						newTemp.setNro_cheque(strNroCheque);
						newTemp.setBanco_id(strBancoCheque);
						newTemp.setTipoPago("Cheque".toUpperCase());
						temp.add(newTemp);
					}
				}
			} else if (strMetodo.equals("Tarjeta")) {
				String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
				String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
				String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
				String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

				if (strPagoTarjeta != null) {
					if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
						descuentoPago = false;
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
						newTemp.setReferenceno(strNroTarjeta);
						newTemp.setTarjeta_id(strBancoTarjeta);
						newTemp.setCondicion_id(strBancoCondicion);
						newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
						temp.add(newTemp);
					}
				}
			} else if (strMetodo.equals("Transferencia")) {
				String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
				String strNroTransferencia = (vars.getStringParameter("inpNroPagoTransferencia")).trim();
				String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

				if (strPagoTransferencia != null) {

					if (Double.parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
						ATECCO_Temporal newTemp = new ATECCO_Temporal();
						newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
						newTemp.setReferenceno(new BigInteger(strNroTransferencia.trim()).toString());
						newTemp.setBanco_id(strBancoTransferencia);
						newTemp.setTipoPago("Transferencia/Depósito".toUpperCase());
						temp.add(newTemp);
					}

				}
			}

			String strWindowPath = Utility.getTabURL(strTab, "R", true);
			if (strWindowPath.equals("")) {
				strWindowPath = strDefaultServlet;
			}

			OBError myError = processButton(vars, strKey, strOrder, strMetodo, temp, descuentoPago, strWindow, strTab,
					response);
			log4j.debug(myError.getMessage());
			vars.setMessage(strTab, myError);
			printPageClosePopUp(response, vars, strWindowPath);
		}
	}

	@SuppressWarnings("resource")
	private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey, String windowId,
			String strTab, String strProcessId) throws IOException, ServletException {

		Connection conn = null;

		try {
			OBContext.setAdminMode(true);

			Order order = OBDal.getInstance().get(Order.class, strKey);

			ComboTableData comboTableDataBanco = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_BANCO_ID", "", null,
					Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
					Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

			ComboTableData comboTableDataTarjeta = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_TARJETA_ID", "",
					null, Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
					Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

			ComboTableData comboTableDataCondicion = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_CONDICION_ID",
					"", null, Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
					Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

			XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Validar")
					.createXmlDocument();
			xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
			xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
			xmlDocument.setParameter("theme", vars.getTheme());
			xmlDocument.setParameter("key", strKey);
			xmlDocument.setParameter("window", windowId);
			xmlDocument.setParameter("tab", strTab);

			String strMetodo = "";

			conn = OBDal.getInstance().getConnection();
			String sql = null;
			PreparedStatement ps = null;
			ResultSet rs = null;

			String mLocatorId = "";

			sql = "SELECT ml.m_locator_id " + "FROM ad_org_warehouse aow "
					+ "INNER JOIN m_warehouse mw ON (aow.m_warehouse_id = mw.m_warehouse_id) "
					+ "INNER JOIN m_locator ml ON (mw.m_warehouse_id = ml.m_warehouse_id) " + "WHERE aow.ad_org_id = '"
					+ order.getOrganization().getId() + "';";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				mLocatorId = rs.getString("m_locator_id");
			}

			boolean sinProducto = false;

			/*
			 * sql = "SELECT col.qtyordered, coalesce(mpsv.qtyonhand, 0) as qtyonhand " +
			 * "FROM c_orderline col " + "LEFT JOIN m_product_stock_v mpsv " +
			 * "ON (col.m_product_id = mpsv.m_product_id " + "AND mpsv.m_locator_id = '" +
			 * mLocatorId + "' " + "AND mpsv.c_uom_id = '100') " +
			 * "WHERE col.c_order_id = '" + order.getId() + "';";
			 */
			sql = "SELECT col.qtyordered, coalesce(mpsv.qtyonhand, 0) as qtyonhand " + "FROM c_orderline col "
					+ "LEFT JOIN m_product_stock_v mpsv " + "ON (col.m_product_id = mpsv.m_product_id "
					+ "AND mpsv.m_locator_id = '" + mLocatorId + "' " + "AND mpsv.c_uom_id = '100') "
					+ "LEFT JOIN m_product p " + "ON (col.m_product_id = p.m_product_id) " + "WHERE col.c_order_id = '"
					+ order.getId() + "' " + "AND p.producttype = 'I' " + ";";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				double movementQty = rs.getDouble("qtyordered");
				double auxQtyHand = rs.getDouble("qtyonhand");

				if (movementQty > auxQtyHand) {
					sinProducto = true;
					break;
				}
			}

			rs.close();
			ps.close();

			if (sinProducto) {
				OBDal.getInstance().rollbackAndClose();
				log4j.warn("Rollback in transaction");

				String strWindowPath = Utility.getTabURL(strTab, "R", true);
				if (strWindowPath.equals("")) {
					strWindowPath = strDefaultServlet;
				}

				OBError myError = new OBError();
				myError.setType("Error");
				myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
				myError.setMessage("El proceso no se ejecuto correctamente: "
						+ "No hay producto necesario en la bodega para realizar la transacción");

				log4j.debug(myError.getMessage());
				vars.setMessage(strTab, myError);
				printPageClosePopUp(response, vars, strWindowPath);
				return;
			}

			String auxValor = "";
			String auxValorNormal = "";

			int intAuxTarjeta = 0;

			sql = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap "
					+ "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
					+ "WHERE aap.c_order_id = '" + strKey + "' " + "AND upper(fpm.name) LIKE '%TARJETA%';";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				intAuxTarjeta = rs.getInt("pagoTarjeta");
			}

			rs.close();

			/**
			 * Codigo Nota de Credito
			 */

			String auxTotalNota = "0";

			sql = "SELECT sum(ci.grandtotal) AS totalNota " + "FROM c_invoice ci "
					+ "INNER JOIN c_doctype cd ON (cd.c_doctype_id = ci.c_doctypetarget_id) "
					+ "WHERE ci.c_bpartner_id = '" + order.getBusinessPartner().getId() + "' AND ci.ad_org_id = '"
					+ order.getOrganization().getId() + "' " + "AND cd.docbasetype = 'ARC' " + "AND cd.issotrx = 'Y' "
					+ "AND ispaid = 'N';";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				auxTotalNota = rs.getString("totalNota");
			}

			rs.close();

			/**
			 * Fin Nota de Credito
			 */

			/**
			 * Codigo Anticipos
			 **/

			String auxAnticipoEfectivo = "0";
			String auxAnticipoCheque = "0";
			String auxAnticipoElectronico = "0";
			String auxAnticipoTransferencia = "0";
			String auxAnticipoTarjeta = "0";

			sql = "SELECT upper(fp.name) AS name, sum(aap.amount) AS total " + "FROM atecco_anticipo_pago aap "
					+ "INNER JOIN fin_paymentmethod fp ON (aap.fin_paymentmethod_id = fp.fin_paymentmethod_id) "
					+ "WHERE aap.c_order_id = '" + strKey + "' " + "GROUP BY fp.name;";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString("name").equals("Efectivo".toUpperCase())) {
					auxAnticipoEfectivo = rs.getString("total");
				} else if (rs.getString("name").equals("Cheque".toUpperCase())) {
					auxAnticipoCheque = rs.getString("total");
				} else if (rs.getString("name").equals("Dinero Electrónico".toUpperCase())) {
					auxAnticipoElectronico = rs.getString("total");
				} else if (rs.getString("name").equals("Transferencia/Depósito".toUpperCase())) {
					auxAnticipoTransferencia = rs.getString("total");
				} else if (rs.getString("name").equals("Tarjeta de Crédito".toUpperCase())) {
					auxAnticipoTarjeta = rs.getString("total");
				}
			}

			rs.close();

			/**
			 * Fin Anticipos
			 **/

			String auxMetodo = order.getPaymentMethod().getName().toUpperCase();

			if (auxMetodo.equals("Nota de Crédito".toUpperCase())) {
				auxMetodo = "Mixto".toUpperCase();
			}

			if (!auxMetodo.equals("Mixto".toUpperCase()) && intAuxTarjeta > 0) {
				sql = "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal " + "FROM c_order co "
						+ "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
						+ "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					auxValorNormal = rs.getString("grandtotal");
				}

				rs.close();

				/*
				 * sql =
				 * "UPDATE c_orderline SET pricelist = round(pricelist * 1.06, 2), priceactual = round(priceactual * 1.06, 2) WHERE c_order_id = '"
				 * + strKey + "'";
				 * 
				 * ps = conn.prepareStatement(sql); ps.executeUpdate();
				 */
			}

			if (auxMetodo.equals("Mixto".toUpperCase())) {
				strMetodo = "Mixto";

				/*
				 * sql =
				 * "SELECT round(grandtotal, 2) AS grandtotal FROM c_order WHERE c_order_id = '"
				 * + strKey + "'";
				 */

				sql = "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal " + "FROM c_order co "
						+ "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
						+ "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					auxValorNormal = rs.getString("grandtotal");
				}

				rs.close();

				if (Double.valueOf(auxValorNormal) < 0) {
					auxValorNormal = "0";
				}

				// Porcentaje Tarjeta de Credito
				/*
				 * sql =
				 * "UPDATE c_orderline SET pricelist = round(pricelist * 1.06, 2), priceactual = round(priceactual * 1.06, 2) WHERE c_order_id = '"
				 * + strKey + "'";
				 */

				// ************************** Modificado 14/07/2021
				// ****************************/
				sql = "UPDATE c_orderline SET pricelist = round(em_atecco_precioefectivo * 1.06, 2), priceactual = round(em_atecco_precioefectivo * 1.06, 2) WHERE c_order_id = '"
						+ strKey + "'";
				// *****************************************************************************/

				ps = conn.prepareStatement(sql);
				ps.executeUpdate();

			} else if (auxMetodo.equals("Cheque".toUpperCase())) {
				strMetodo = "Cheque";
				auxTotalNota = "0";
			} else if (auxMetodo.equals("Transferencia/Depósito".toUpperCase())) {
				strMetodo = "Transferencia";
				auxTotalNota = "0";
				
				
				
				
			} else if (auxMetodo.equals("Tarjeta de Crédito".toUpperCase())) {
				strMetodo = "Tarjeta";
				auxTotalNota = "0";

				/*
				 * sql =
				 * "UPDATE c_orderline SET pricelist = round(pricelist * 1.06, 2), priceactual = round(priceactual * 1.06, 2) WHERE c_order_id = '"
				 * + strKey + "'";
				 */

				// *************************** Modificado 14/07/2021 *************************//
				sql = "UPDATE c_orderline SET pricelist = round(em_atecco_precioefectivo * 1.06, 2), priceactual = round(em_atecco_precioefectivo * 1.06, 2) WHERE c_order_id = '"
						+ strKey + "'";
				// ***************************************************************************//

				ps = conn.prepareStatement(sql);
				ps.executeUpdate();

			} else if (auxMetodo.equals("Dinero Electrónico".toUpperCase())) {
				strMetodo = "Electronico";
				auxTotalNota = "0";
			} else if (auxMetodo.equals("Efectivo".toUpperCase())) {
				strMetodo = "Efectivo";
				auxTotalNota = "0";
			} else if (auxMetodo.equals("Con Retención".toUpperCase())) {
				strMetodo = "Retencion";
			}

			/*
			 * sql =
			 * "SELECT round(grandtotal, 2) AS grandtotal FROM c_order WHERE c_order_id = '"
			 * + strKey + "'";
			 */

			sql = "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal " + "FROM c_order co "
					+ "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
					+ "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				auxValor = rs.getString("grandtotal");
			}

			rs.close();
			ps.close();

			if (Double.valueOf(auxValor) < 0) {
				auxValor = "0";
			}

			if (intAuxTarjeta > 0) {
				auxValorNormal = auxValor;
			}

			if (strMetodo.toUpperCase().equals("Mixto".toUpperCase())) {
				if (intAuxTarjeta > 0) {
					xmlDocument.setParameter("tituloTotalTarjeta", "");
					xmlDocument.setParameter("tituloTotalEfectivo", "Total Tarjeta: " + auxValor);
					xmlDocument.setParameter("montoPagoDesc", auxValor);
				} else {
					xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
					xmlDocument.setParameter("tituloTotalEfectivo", "Total Efectivo: " + auxValorNormal);
					xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
				}
			} else if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {

				if (intAuxTarjeta > 0) {
					xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
					xmlDocument.setParameter("tituloTotalEfectivo", "");
				} else {
					xmlDocument.setParameter("tituloTotalTarjeta", "");
					xmlDocument.setParameter("tituloTotalEfectivo", "Total Efectivo: " + auxValor);
				}
			}

			xmlDocument.setParameter("metodo", strMetodo);
			xmlDocument.setParameter("montoPago", auxValor);

			xmlDocument.setParameter("valorPagoEfectivo", auxValor);

			/**
			 * Nota de Credito
			 */

			xmlDocument.setParameter("montoPagoNotaCredito", auxTotalNota);

			/**
			 * Fin Nota de Credito
			 */

			/**
			 * Anticipos
			 **/

			Double totalEfectivo = Double.valueOf(auxAnticipoEfectivo) + Double.valueOf(auxAnticipoCheque)
					+ Double.valueOf(auxAnticipoElectronico) + Double.valueOf(auxAnticipoTransferencia)
					+ Double.valueOf(auxAnticipoTarjeta);

			xmlDocument.setParameter("montoAnticipoEfectivo", auxAnticipoEfectivo);
			xmlDocument.setParameter("montoAnticipoCheque", auxAnticipoCheque);
			xmlDocument.setParameter("montoAnticipoElectronico", auxAnticipoElectronico);
			xmlDocument.setParameter("montoAnticipoTransferencia", auxAnticipoTransferencia);
			xmlDocument.setParameter("montoAnticipoTarjeta", auxAnticipoTarjeta);

			/**
			 * Fin anicipos
			 **/

			if (strMetodo.toUpperCase().equals("Efectivo".toUpperCase())) {
				xmlDocument.setParameter("montoPagoEfectivo", auxValor);
				xmlDocument.setParameter("montoAnticipoEfectivo", totalEfectivo.toString());
			} else {
				xmlDocument.setParameter("montoPagoEfectivo", "0.00");
			}

			xmlDocument.setParameter("valorPagoElectronico", auxValor);

			if (strMetodo.toUpperCase().equals("Electronico".toUpperCase())) {
				xmlDocument.setParameter("montoPagoElectronico", auxValor);
				xmlDocument.setParameter("montoAnticipoElectronico", totalEfectivo.toString());
			} else {
				xmlDocument.setParameter("montoPagoElectronico", "0.00");
			}

			xmlDocument.setParameter("valorPagoCheque", auxValor);

			if (strMetodo.toUpperCase().equals("Cheque".toUpperCase())) {
				xmlDocument.setParameter("montoPagoCheque", auxValor);
				xmlDocument.setParameter("montoAnticipoCheque", totalEfectivo.toString());
			} else {
				xmlDocument.setParameter("montoPagoCheque", "0.00");
			}

			xmlDocument.setParameter("nroPagoCheque", "0.00");

			xmlDocument.setParameter("valorPagoTransferencia", auxValor);

			if (strMetodo.toUpperCase().equals("Transferencia".toUpperCase())) {
				xmlDocument.setParameter("montoPagoTransferencia", auxValor);
				xmlDocument.setParameter("montoAnticipoTransferencia", totalEfectivo.toString());
			} else {
				xmlDocument.setParameter("montoPagoTransferencia", "0.00");
			}

			xmlDocument.setParameter("nroPagoTransferencia", "0.00");

			xmlDocument.setParameter("valorPagoTarjeta", auxValor);

			if (strMetodo.toUpperCase().equals("Tarjeta".toUpperCase())) {
				xmlDocument.setParameter("montoPagoTarjeta", auxValor);
				xmlDocument.setParameter("montoAnticipoTarjeta", totalEfectivo.toString());
			} else {
				xmlDocument.setParameter("montoPagoTarjeta", "0.00");
			}

			xmlDocument.setParameter("nroPagoTarjeta", "0.00");

			xmlDocument.setParameter("valorPagoRetencion", auxValor);

			xmlDocument.setParameter("montoPagoRetencion", "0.00");

			xmlDocument.setParameter("nroPagoRetencion", "0.00");

			xmlDocument.setParameter("montoPagoRetencionIva", "0.00");
			xmlDocument.setParameter("montoPagoRetencionRenta", "0.00");

			if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {

				/*
				 * 
				 * MODIFICADO ..::ETA::.. PARA CALCULO RETENCIÓN DE RETENCION ARTICULOS Y
				 * SERVICIOS
				 * 
				 */

				/*
				 * IVA
				 */

				BigDecimal retTotal = BigDecimal.ZERO;

				String strRetIvaServicios = "0.00";
				String strRetIvaProductos = "0.00";
				String strRetencionIva = "0.00";
				BigDecimal retIvaTotal = BigDecimal.ZERO;

				/*
				 * OBTIENE EL VALOR DE RETENCIÓN TIPO IVA DE TODOS LOS PRODUCTOS TIPO ARTICULOS
				 */

				sql = "SELECT coalesce((sum(a.porcentajeRet)),0) as valor   " + " FROM ( "
						+ " SELECT (ctr.porcentaje / 100) * colt.taxamt AS porcentajeRet, ctr.porcentaje, colt.taxbaseamt, colt.taxamt, ol.m_product_id"
						+ " FROM c_order co " + " INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id) "
						+ " INNER JOIN m_product p on (p.m_product_id = ol.m_product_id) "
						+ " INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
						+ " INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
						+ " INNER JOIN c_orderlinetax colt ON (ol.c_orderline_id = colt.c_orderline_id and colt.c_order_id = co.c_order_id and colt.c_orderline_id in (select ol.c_orderline_id "
						+ "      	                                                                                            	from c_orderline ol "
						+ "      	                                                                                            	inner join m_product p on (p.m_product_id = ol.m_product_id) "
						+ "      	                                                                                            	where c_order_id = '"
						+ strKey + "'"
						+ "      	                                                                                            	and p.producttype = 'I'))"
						+ " WHERE co.c_order_id = '" + strKey + "' " + " AND ctr.tipo = 'IVA' "
						+ " AND ctr.isactive = 'Y' " + " AND cbr.em_atecco_producto = 'Y' "
						+ " AND p.producttype = 'I' " + " GROUP BY 2,3,4,5) AS a;";

				/*
				 * sql = "SELECT round((ctr.porcentaje / 100) * colt.taxamt, 2) AS porcentaje "
				 * + "FROM c_order co " +
				 * "INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
				 * +
				 * "INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
				 * + "INNER JOiN c_ordertax colt ON (colt.c_order_id = co.c_order_id) " +
				 * "WHERE co.c_order_id = '" + strKey + "' " + "AND ctr.tipo = 'IVA' " +
				 * "AND ctr.isactive = 'Y' " + "LIMIT 1;";
				 */

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					strRetIvaProductos = rs.getString("valor");
				}

				rs.close();
				ps.close();

				/*
				 * OBTIENE EL VALOR DE RETENCIÓN TIPO IVA DE TODOS LOS PRODUCTOS TIPO SERVICIOS
				 */

				sql = "SELECT coalesce(sum(a.porcentajeRet),0) as valor" + " FROM ( "
						+ " SELECT (ctr.porcentaje / 100) * colt.taxamt AS porcentajeRet, ctr.porcentaje, colt.taxbaseamt, colt.taxamt, ol.m_product_id"
						+ " FROM c_order co " + " INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id) "
						+ " INNER JOIN m_product p on (p.m_product_id = ol.m_product_id) "
						+ " INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
						+ " INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
						+ " INNER JOIN c_orderlinetax colt ON (ol.c_orderline_id = colt.c_orderline_id and colt.c_order_id = co.c_order_id and colt.c_orderline_id in (select ol.c_orderline_id"
						+ "      	                                                                                            	from c_orderline ol"
						+ "      	                                                                                            	inner join m_product p on (p.m_product_id = ol.m_product_id)"
						+ "      	                                                                                            	where c_order_id = '"
						+ strKey + "'"
						+ "      	                                                                                            	and p.producttype = 'S'))"
						+ " WHERE co.c_order_id = '" + strKey + "'" + " AND ctr.tipo = 'IVA'"
						+ " AND ctr.isactive = 'Y'" + " AND cbr.em_atecco_servicio = 'Y'" + " AND p.producttype = 'S' "
						+ " GROUP BY 2,3,4,5) AS a;";

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					strRetIvaServicios = rs.getString("valor");
				}

				rs.close();
				ps.close();

				/*
				 * SUMO EL TOTAL DE RETENCIÓN EN IVA Y ASIGNO A VARIABLE PARA MOSTRAR
				 */

				retIvaTotal = (new BigDecimal(strRetIvaProductos).add(new BigDecimal(strRetIvaServicios)));

				retIvaTotal = retIvaTotal.setScale(2, RoundingMode.HALF_UP);

				retTotal = retTotal.add(retIvaTotal);

				strRetencionIva = retIvaTotal.toString();

				/*
				 * FUENTE
				 */

				String strRetFuenteServicios = "0.00";
				String strRetFuenteProductos = "0.00";
				String strRetencionFuente = "0.00";
				String strRetencionTotal = "0.00";

				BigDecimal retFuenteTotal = BigDecimal.ZERO;

				/*
				 * OBTIENE EL VALOR DE RETENCIÓN TIPO FUENTE DE TODOS LOS PRODUCTOS TIPO
				 * ARTICULOS
				 */

				sql = "SELECT coalesce(sum(a.porcentajeRet),0) as valor" + "    FROM ("
						+ "            SELECT (ctr.porcentaje / 100) * ol.linenetamt AS porcentajeRet, ctr.porcentaje, ol.linenetamt, ol.m_product_id"
						+ "            FROM c_order co"
						+ "            INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id)"
						+ "            INNER JOIN m_product p on (p.m_product_id = ol.m_product_id)"
						+ "            INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id)"
						+ "            INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id)"
						+ " 	 WHERE co.c_order_id = '" + strKey + "'" + "    AND ctr.tipo = 'FUENTE'"
						+ "    AND ctr.isactive = 'Y'" + "    AND cbr.em_atecco_producto = 'Y'"
						+ "    AND p.producttype = 'I' " + " GROUP BY 2,3,4) AS a;";

				/*
				 * sql = "SELECT round((ctr.porcentaje / 100) * colt.taxamt, 2) AS porcentaje "
				 * + "FROM c_order co " +
				 * "INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
				 * +
				 * "INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
				 * + "INNER JOiN c_ordertax colt ON (colt.c_order_id = co.c_order_id) " +
				 * "WHERE co.c_order_id = '" + strKey + "' " + "AND ctr.tipo = 'IVA' " +
				 * "AND ctr.isactive = 'Y' " + "LIMIT 1;";
				 */

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					strRetFuenteProductos = rs.getString("valor");
				}

				rs.close();
				ps.close();

				/*
				 * OBTIENE EL VALOR DE RETENCIÓN TIPO FUENTE DE TODOS LOS PRODUCTOS TIPO
				 * SERVICIOS
				 */

				sql = "SELECT coalesce(sum(a.porcentajeRet),0) as valor" + "    FROM ("
						+ "            SELECT (ctr.porcentaje / 100) * ol.linenetamt AS porcentajeRet, ctr.porcentaje, ol.linenetamt, ol.m_product_id"
						+ "            FROM c_order co"
						+ "            INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id)"
						+ "            INNER JOIN m_product p on (p.m_product_id = ol.m_product_id)"
						+ "            INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id)"
						+ "            INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id)"
						+ " 	 WHERE co.c_order_id = '" + strKey + "'" + "    AND ctr.tipo = 'FUENTE'"
						+ "    AND ctr.isactive = 'Y'" + "    AND cbr.em_atecco_servicio = 'Y'"
						+ "    AND p.producttype = 'S' " + "	 GROUP BY 2,3,4) AS a;";

				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					strRetFuenteServicios = rs.getString("valor");
				}

				rs.close();
				ps.close();

				/*
				 * SUMO EL TOTAL DE RETENCIÓN EN FUENTE Y ASIGNO A VARIABLE PARA MOSTRAR
				 */

				retFuenteTotal = new BigDecimal(strRetFuenteProductos).add(new BigDecimal(strRetFuenteServicios));

				retFuenteTotal = retFuenteTotal.setScale(2, RoundingMode.HALF_UP);

				retTotal = retTotal.add(retFuenteTotal);

				strRetencionFuente = retFuenteTotal.toString();
				strRetencionTotal = retTotal.toString();

				/*
				 * String strRetencionFuente = "0.00";
				 *
				 * sql =
				 * "SELECT round((ctr.porcentaje / 100) * colt.taxbaseamt, 2) AS porcentaje " +
				 * "FROM c_order co " +
				 * "INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
				 * +
				 * "INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
				 * + "INNER JOiN c_ordertax colt ON (colt.c_order_id = co.c_order_id) " +
				 * "WHERE co.c_order_id = '" + strKey + "' " + "AND ctr.tipo = 'FUENTE' " +
				 * "AND ctr.isactive = 'Y' " + "LIMIT 1;";
				 * 
				 */

				xmlDocument.setParameter("montoPagoRetencionIva", strRetencionIva);
				xmlDocument.setParameter("montoPagoRetencionRenta", strRetencionFuente);
				xmlDocument.setParameter("montoPagoRetencionTotal", strRetencionTotal);

			}

			xmlDocument.setData("reportATECCOBANCOCHEQUE_ID", "liststructure", comboTableDataBanco.select(false));

			xmlDocument.setData("reportATECCOBANCOTRANSFERENCIA_ID", "liststructure",
					comboTableDataBanco.select(false));

			xmlDocument.setData("reportATECCOBANCOTARJETA_ID", "liststructure", comboTableDataTarjeta.select(false));

			xmlDocument.setData("reportATECCOBANCOCONDICION_ID", "liststructure",
					comboTableDataCondicion.select(false));

			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println(xmlDocument.print());
			out.close();

			conn.rollback();
			conn.close();
		} catch (Exception ex) {
			// TODO: handle exception
			throw new ServletException(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}

	@SuppressWarnings("resource")
	private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder, String strMetodo,
			List<ATECCO_Temporal> temp, boolean descuentoPago, String windowId, String strTab,
			HttpServletResponse response) {
		OBError myError = null;

		Connection conn = null;
		String resultado = null;

		try {
			Order order = OBDal.getInstance().get(Order.class, strOrder);

			conn = OBDal.getInstance().getConnection();
			String sql = null;
			String sqlRef = null;
			String sqlRefAnt = null;
			PreparedStatement ps = null;
			ResultSet rs = null;

			int intAuxTarjeta = 0;

			sql = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap "
					+ "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
					+ "WHERE aap.c_order_id = '" + strOrder + "' " + "AND upper(fpm.name) LIKE '%TARJETA%';";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				intAuxTarjeta = rs.getInt("pagoTarjeta");
			}

			rs.close();

			String auxstrMetodo = strMetodo;
			boolean auxdescuentoPago = descuentoPago;

			if (intAuxTarjeta > 0) {
				auxstrMetodo = "Mixto";
				auxdescuentoPago = false;
			}

			/*********************************************
			 * YRO 29-01-2023
			 ***********************************************************/
			if (auxstrMetodo.equals("Transferencia")) {

				for (int i = 0; i < temp.size(); i++) {
					sqlRef = "select 'El N° de Referencia: '||a.referenceno||', ya se encuentra registrado en la sucursal: '||c.name||', en la Factura N°: '||b.documentno as mensaje"
							+ "  from atecco_temporal_pago a, c_order b, ad_org c"
							+ " where a.c_order_id = b.c_order_id" + "   and b.ad_org_id = c.ad_org_id"
							+ "   and a.fin_paymentmethod_id = 'EC103'" + "   and to_number(replace(trim(a.referenceno),' ','')) = '"
							+ new BigInteger(temp.get(i).getReferenceno().trim()).toString() + "' union all "
						    + "select 'El N° de Referencia: '||a.referenceno||', ya se encuentra registrado en la sucursal: '||c.name||', en el Anticipo N°: '||b.documentno as mensaje"
						    + "  from atecco_anticipo_pago a, c_order b, ad_org c"
						    + " where a.c_order_id = b.c_order_id" + "   and b.ad_org_id = c.ad_org_id"
						    + "   and a.fin_paymentmethod_id = 'EC103'" + "   and to_number(replace(trim(a.referenceno),' ','')) = to_number(replace('"
						    + new BigInteger(temp.get(i).getReferenceno().trim()).toString() + "',' ','')) limit 1;";
				}

				if (sqlRef != null) {
					System.out.println("Valida referencia: " + sqlRef.toString());
					ps = conn.prepareStatement(sqlRef);
					rs = ps.executeQuery();
					
					while (rs.next()) {
						resultado = rs.getString("mensaje");
					}

					rs.close();
				}
			}
			/******************************************************************************************************************/

			if (auxstrMetodo.equals("Efectivo") || auxstrMetodo.equals("Cheque") || auxstrMetodo.equals("Transferencia")
					|| auxstrMetodo.equals("Electronico") || auxstrMetodo.equals("Retencion") || auxdescuentoPago) {
				sql = "UPDATE c_order SET em_atecco_pagodescuento = 'Y' WHERE c_order_id = '" + strOrder + "'";

				ps = conn.prepareStatement(sql);
				ps.executeUpdate();
			} else {
				// Porcentaje Tarjeta de Credito
				/*
				 * sql =
				 * "UPDATE c_orderline SET pricelist = round(pricelist * 1.06, 2), priceactual = round(priceactual * 1.06, 2) WHERE c_order_id = '"
				 * + strOrder + "'";
				 */

				// ****************** Modificado 14/07/2021 ************************/
				sql = "UPDATE c_orderline SET pricelist = round(em_atecco_precioefectivo * 1.06, 2), priceactual = round(em_atecco_precioefectivo * 1.06, 2) WHERE c_order_id = '"
						+ strOrder + "'";
				// ******************************************************/

				ps = conn.prepareStatement(sql);
				ps.executeUpdate();
			}

			if (resultado != null && auxstrMetodo.equals("Transferencia")) {
				log4j.warn("Rollback in transaction");
				myError = new OBError();
				myError.setType("Error");
				myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
				myError.setMessage(resultado.toString());
			} else {

				myError = new OBError();
				myError.setType("Success");
				myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
				// myError.setMessage(Utility.messageBD(this, "RecordsCopied",
				// vars.getLanguage()));

				if (auxstrMetodo.equals("Transferencia") || auxstrMetodo.equals("Mixto")) {
					for (int i = 0; i < temp.size(); i++) {
						
						if(temp.get(i).getTipoPago().equals("TRANSFERENCIA/DEPÓSITO")) {
							sqlRef = "select 'El N° de Referencia: '||a.referenceno||', ya se encuentra registrado en la sucursal: '||c.name||', en la Factura N°: '||b.documentno as mensaje"
									+ "  from atecco_temporal_pago a, c_order b, ad_org c"
									+ " where a.c_order_id = b.c_order_id" + "   and b.ad_org_id = c.ad_org_id"
									+ "   and a.fin_paymentmethod_id = 'EC103'" + "   and to_number(replace(trim(a.referenceno),' ','')) = '"
									+ new BigInteger(temp.get(i).getReferenceno().trim()).toString() + "' union all "
								    + "select 'El N° de Referencia: '||a.referenceno||', ya se encuentra registrado en la sucursal: '||c.name||', en el Anticipo N°: '||b.documentno as mensaje"
								    + "  from atecco_anticipo_pago a, c_order b, ad_org c"
								    + " where a.c_order_id = b.c_order_id" + "   and b.ad_org_id = c.ad_org_id"
								    + "   and a.fin_paymentmethod_id = 'EC103'" + "   and to_number(replace(trim(a.referenceno),' ','')) = to_number(replace('"
								    + new BigInteger(temp.get(i).getReferenceno().trim()).toString() + "',' ','')) limit 1;";
	
							ps = conn.prepareStatement(sqlRef);
							rs = ps.executeQuery();
	
							while (rs.next()) {
								resultado = rs.getString("mensaje");
							}
							rs.close();
							
							//if (resultado != null && auxstrMetodo.equals("Transferencia")) { // YRO 17-02-2023
							if (resultado != null) { // YRO 17-02-2023
								log4j.warn("Rollback in transaction");
								myError = new OBError();
								myError.setType("Error");
								myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
								myError.setMessage(resultado.toString());
							}
						}
						/***********************************************************************/

					}
				}

				if (resultado == null) {
					for (int i = 0; i < temp.size(); i++) {

						ATECCOValidarXmlData.methodInsertarTemp(this, order.getClient().getId(),
								String.valueOf(temp.get(i).getAmount()), temp.get(i).getTarjeta_id(),
								temp.get(i).getCondicion_id(), temp.get(i).getBanco_id(), temp.get(i).getReferenceno(),
								temp.get(i).getNro_cheque(), temp.get(i).getTipoPago(), strOrder);
					}

				} else {
					log4j.warn("Rollback in transaction");
					myError = new OBError();
					myError.setType("Error");
					myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
					myError.setMessage(resultado.toString());
				}

				OBDal.getInstance().commitAndClose();

				ATECCOValidarXmlData.methodUpdateOrder(this, "CO", strOrder);
			}
		} catch (Exception e) {
			OBDal.getInstance().rollbackAndClose();

			try {
				ATECCOValidarXmlData.methodUpdateOrder(this, "SP", strOrder);
				ATECCOValidarXmlData.methodDeleteOrder(this, strOrder);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			e.printStackTrace();
			log4j.warn("Rollback in transaction");
			myError = new OBError();
			myError.setType("Error");
			myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
			if (!resultado.equals(null)) {
				myError.setMessage("El proceso No se ejecuto correctamente: " + resultado.toString());
			} else {
				myError.setMessage("El proceso No se ejecuto correctamente: " + e.getMessage());
			}
			// myError = Utility.translateError(this, vars, vars.getLanguage(),
			// "ProcessRunError");
		} finally {
			OBContext.restorePreviousMode();

			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}

		return myError;
	}

}
