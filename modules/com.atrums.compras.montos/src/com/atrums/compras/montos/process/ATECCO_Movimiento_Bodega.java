package com.atrums.compras.montos.process;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.felectronica.process.ATECFE_Funciones_Aux;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class ATECCO_Movimiento_Bodega extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getGlobalVariable("inpmMovementId", strWindow + "|M_Movement_ID");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      String strWindow = vars.getStringParameter("inpWindowId");
      String strMovement = vars.getStringParameter("inpmMovementId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|M_Movement_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strMovement);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

    Connection conn = null;

    try {
      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Movimiento_Bodega")
          .createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strMovement) {
    OBError myError = null;
    Connection conn = null;
    Boolean emailNoEnviado = true;

    OBContext.setAdminMode(true);

    try {
      conn = OBDal.getInstance().getConnection();
      String sqlQuery = "";
      PreparedStatement ps = null;
      ResultSet rs = null;

      String mLocatorId = "";
      String mProductId = "";
      double movementQty = Double.valueOf("0");

      sqlQuery = "SELECT mml.m_locator_id, mml.m_product_id, movementqty "
          + "FROM m_movementline mml " + "WHERE m_movement_id = '" + strMovement + "';";

      ps = conn.prepareStatement(sqlQuery);
      rs = ps.executeQuery();

      while (rs.next()) {
        mLocatorId = rs.getString("m_locator_id");
        mProductId = rs.getString("m_product_id");
        movementQty = rs.getDouble("movementqty");

        double auxQtyHand = Double.valueOf("0");

        sqlQuery = "SELECT qtyonhand " + "FROM M_Product_Stock_V " + "WHERE m_locator_id = '"
            + mLocatorId + "' " + "AND c_uom_id = '100' " + "AND m_product_id = '" + mProductId
            + "';";

        PreparedStatement ps2 = conn.prepareStatement(sqlQuery);
        ResultSet rs2 = ps2.executeQuery();

        while (rs2.next()) {
          auxQtyHand = rs2.getDouble("qtyonhand");
        }

        rs2.close();
        ps2.close();

        if (movementQty > auxQtyHand) {
          OBDal.getInstance().rollbackAndClose();
          log4j.warn("Rollback in transaction");
          myError = new OBError();
          myError.setType("Error");
          myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
          myError.setMessage("El proceso no se ejecuto correctamente: "
              + "No hay producto necesario en la bodega origen para la transacción");
          return myError;
        } else {
          mLocatorId = "";
          mProductId = "";
          movementQty = Double.valueOf("0");
        }

      }

      rs.close();
      ps.close();

      String strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();

      sqlQuery = "INSERT INTO ad_pinstance("
          + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
          + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
          + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance
          + "', 'DB030337AE0A4578B928A295EA56DBAD', '" + strMovement + "', 'N', now(), "
          + "'100', now(), null, null, '" + vars.getClient() + "', '0', " + "'100', '100', 'Y');";

      ps = conn.prepareStatement(sqlQuery);
      int updateOrder = ps.executeUpdate();
      ps.close();

      if (updateOrder == 1) {
        sqlQuery = "SELECT * FROM atecco_movement_proc_post('" + strPInstance + "')";

        ps = conn.prepareStatement(sqlQuery);
        ps.execute();

        ps.close();

        sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
            + strPInstance + "'";

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();
        int auxresult = 0;
        String auxerrormsg = null;

        while (rs.next()) {
          if (rs.getString("result") != null) {
            auxresult = rs.getInt("result");
          }

          auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
        }

        if (auxerrormsg != null) {
          auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
        } else {
          auxerrormsg = "@Documento procesado@";
        }

        rs.close();
        ps.close();

        if (auxresult == 1) {
          ATECFE_Funciones_Aux opeaux = new ATECFE_Funciones_Aux();

          String auxdoc = null;
          String auxname = null;

          sqlQuery = "SELECT mo.em_co_doctype_id, cpdt.templatelocation || '/' || cpdt.templatefilename AS templatelocation, cpdt.name "
              + "FROM m_movement mo "
              + "INNER JOIN c_doctype cd ON (mo.em_co_doctype_id = cd.c_doctype_id) "
              + "INNER JOIN c_poc_doctype_template cpdt ON (cd.c_doctype_id = cpdt.c_doctype_id) "
              + "WHERE mo.m_movement_id = '" + strMovement + "'";

          ps = conn.prepareStatement(sqlQuery);
          rs = ps.executeQuery();

          while (rs.next()) {
            if (rs.getString("templatelocation") != null) {
              auxdoc = rs.getString("templatelocation");
              auxname = rs.getString("name");
            }
          }

          rs.close();
          ps.close();

          File flPdf = null;

          if (auxdoc != null) {
            flPdf = generarPDF(conn, auxdoc, auxname, strMovement);

            if (flPdf != null) {

              List<File> listdoc = new ArrayList<File>();
              listdoc.add(flPdf);

              
              //String strReceptor = "mespinosa@edimca.com.ec;cflores@edimca.com.ec;jotuna@dontablero.com;dandino@dontablero.com;nmora@dontablero.com";
              
              String strReceptor = "yesenia.rodriguez@atrums.com";
              
              if (opeaux.enviarCorreo(strReceptor, "Movimiento entre Bodegas",
                  "Estimado, \n Se ha procesado un Movimiento entre Bodegas. Documento adjunto",
                  null, listdoc)) {
                OBDal.getInstance().commitAndClose();

                myError = new OBError();
                myError.setType("Success");
                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

                String auxerrormsgTrad = "";

                while ((auxerrormsg.indexOf("@") != -1)
                    && (auxerrormsg.indexOf("@", auxerrormsg.indexOf("@") + 1) != -1)
                    && emailNoEnviado) {
                  int auxIni = auxerrormsg.indexOf("@");
                  int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

                  String auxParteInicio = auxerrormsg.substring(0, auxIni);
                  String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);
                  auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());
                  auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;
                  auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
                }

                auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

                auxerrormsg = auxerrormsgTrad.equals("")
                    ? ("Se envio el correo electronico a: " + strReceptor)
                    : (auxerrormsgTrad + ", Se envio el correo electronico a: " + strReceptor);

                emailNoEnviado = false;
              } else {
                OBDal.getInstance().rollbackAndClose();

                myError = new OBError();
                myError.setType("Error");
                myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

                auxerrormsg = auxerrormsg.equals("")
                    ? ("No se pudo enviar el correo electronico revice su configuración de email")
                    : (auxerrormsg
                        + ", No se pudo enviar el correo electronico revice su configuración de email");
              }
            } else {
              OBDal.getInstance().rollbackAndClose();

              myError = new OBError();
              myError.setType("Error");
              myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

              auxerrormsg = auxerrormsg.equals("") ? ("No se pudo generar el reporte")
                  : (auxerrormsg + ", No se pudo generar el reporte");
            }
          } else {
            OBDal.getInstance().rollbackAndClose();

            myError = new OBError();
            myError.setType("Error");
            myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

            auxerrormsg = auxerrormsg.equals("")
                ? ("No hay tipo de documento para generar el reporte")
                : (auxerrormsg + ", No hay tipo de documento para generar el reporte");
          }
        } else {
          OBDal.getInstance().rollbackAndClose();

          myError = new OBError();
          myError.setType("Error");
          myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
        }

        String auxerrormsgTrad = "";

        while ((auxerrormsg.indexOf("@") != -1)
            && (auxerrormsg.indexOf("@", auxerrormsg.indexOf("@") + 1) != -1) && emailNoEnviado) {
          int auxIni = auxerrormsg.indexOf("@");
          int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

          String auxParteInicio = auxerrormsg.substring(0, auxIni);
          String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);
          auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());
          auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;
          auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
        }

        auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;
        myError.setMessage(auxerrormsgTrad);
      } else {
        OBDal.getInstance().rollbackAndClose();

        myError = new OBError();
        myError.setType("Error");
        myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
        myError.setMessage("Error en procesamiento");
      }

    } catch (Exception e) {
      e.printStackTrace();
      OBDal.getInstance().rollbackAndClose();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getStackTrace());
    } finally {
      OBContext.restorePreviousMode();
      if (conn != null) {
        try {
          conn.close();
          conn = null;
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }

  private File generarPDF(Connection conn, String strBaseDesin, String strNombre, String strEntiID)
      throws ServletException {

    File flTemp = null;

    try {

      String strBaseDesign = strBaseDesin;

      if (!basedesign(strBaseDesign).equals("")) {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("DOCUMENT_ID", strEntiID);
        parameters.put("BASE_DESIGN", basedesign(strBaseDesign));

        strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

        JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, conn);

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        String strFecha = ft.format(date);
        String strNombreArch = strNombre + "-" + strFecha;
        OutputStream out = null;

        flTemp = File.createTempFile(strNombreArch, ".pdf", null);

        out = new FileOutputStream(flTemp);
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        out.write(byteout.toByteArray());
        out.flush();
        out.close();
      }

    } catch (JRException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    return flTemp;
  }

  private String basedesign(String strReporte) {

    String strDirectorio = ATECFE_Funciones_Aux.class.getResource("/").getPath();

    if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
      int intPos = strDirectorio.indexOf("/");

      if (intPos != -1) {
        strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
      }
    }

    String strDirectorioInv = "";

    for (int i = 0; i < strDirectorio.length(); i++) {
      strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
    }

    int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

    if (intPosicion != -1) {
      strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
    } else {
      intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
      if (intPosicion != -1) {
        strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
      } else {
        return "";
      }
    }

    strDirectorio = "";

    for (int i = 0; i < strDirectorioInv.length(); i++) {
      strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
    }

    String strBaseDesign = strReporte.replaceAll("@basedesign@",
        (strDirectorio + "WebContent/src-loc/design"));

    File flRep = new File(strBaseDesign);

    if (flRep.exists()) {
      return strDirectorio + "WebContent/src-loc/design";
    } else {
      strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

      flRep = new File(strBaseDesign);

      if (flRep.exists()) {
        return strDirectorio + "src-loc/design";
      } else {
        return "";
      }
    }
  }

}
