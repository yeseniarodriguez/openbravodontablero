package com.atrums.compras.montos.utility.reporting.printing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.ConfigParameters;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.erpCommon.utility.poc.EmailType;
import org.openbravo.erpCommon.utility.reporting.DocumentType;
import org.openbravo.erpCommon.utility.reporting.Report;
import org.openbravo.erpCommon.utility.reporting.Report.OutputTypeEnum;
import org.openbravo.erpCommon.utility.reporting.ReportManager;
import org.openbravo.erpCommon.utility.reporting.ReportingException;
import org.openbravo.erpCommon.utility.reporting.TemplateData;
import org.openbravo.erpCommon.utility.reporting.TemplateInfo;
import org.openbravo.erpCommon.utility.reporting.TemplateInfo.EmailDefinition;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.xmlEngine.XmlDocument;

@SuppressWarnings("serial")
public class ATECCO_Correos_Compra extends HttpSecureAppServlet {

	private final Map<String, TemplateData[]> differentDocTypes = new HashMap<String, TemplateData[]>();
	private boolean multiReports = false;

	@Override
	public void init(ServletConfig config) {
		super.init(config);
		boolHist = false;
	}

	@SuppressWarnings("unchecked")
	public void post(HttpServletRequest request, HttpServletResponse response, VariablesSecureApp vars,
			DocumentType documentType, String sessionValuePrefix, String strDocumentId,
			ConfigParameters globalParameterss, ConnectionProvider connectionProvider)
			throws IOException, ServletException {
		String localStrDocumentId = strDocumentId;

		if (globalParameters == null) {
			globalParameters = globalParameterss;
		}

		try {
			// Checks are maintained in this way for mulithread safety
			HashMap<String, Boolean> checks = new HashMap<String, Boolean>();
			checks.put("moreThanOneCustomer", Boolean.FALSE);
			checks.put("moreThanOnesalesRep", Boolean.FALSE);

			String documentIds[] = null;
			if (log4j.isDebugEnabled())
				log4j.debug("strDocumentId: " + localStrDocumentId);
			// normalize the string of ids to a comma separated list
			localStrDocumentId = localStrDocumentId.replaceAll("\\(|\\)|'", "");
			if (localStrDocumentId.length() == 0)
				throw new ServletException(Utility.messageBD(connectionProvider, "NoDocument", vars.getLanguage()));

			documentIds = localStrDocumentId.split(",");

			if (log4j.isDebugEnabled())
				log4j.debug("Number of documents selected: " + documentIds.length);

			multiReports = (documentIds.length > 1);

			final ReportManager reportManager = new ReportManager(connectionProvider, globalParameters.strFTPDirectory,
					strReplaceWithFull, globalParameters.strBaseDesignPath, globalParameters.strDefaultDesignPath,
					globalParameters.prefix, multiReports);

			for (int i = 0; i < documentIds.length; i++) {
				PocData[] pocData = PocData.getNameSucursal(connectionProvider, documentIds[i]);

				if (pocData.length > 0) {
					for (PocData documentEmail : pocData) {
						final String documentId = documentEmail.documentId;
						if (log4j.isDebugEnabled())
							log4j.debug("Processing document with id: " + documentEmail.documentId);

						String templateInUse = "default";
						String emails = null;
						//CAMBIOS PARA QUE TOME EL CORREO DE LA SUCURSAL ETA 19/02/2019
						if (documentEmail.nameTemplate.toUpperCase().indexOf("SIN FORMATO") != -1) {
							PocData[] correoSucursal=PocData.getEmailSucursal(connectionProvider, documentEmail.orgId);
							//emails=correoSucursal[0].contactEmail;
							emails = "yesenia.rodriguez@atrums.com";
						} else {
							//emails = "mespinosa@edimca.com.ec;cflores@edimca.com.ec;aprado@dontablero.com";
							emails = "yesenia.rodriguez@atrums.com";
						}

						templateInUse = documentEmail.templateId;

						final Report report = buildReport(response, vars, documentId, reportManager, documentType,
								OutputTypeEnum.EMAIL, templateInUse, connectionProvider);

						// if there is only one document type id the user should be
						// able to choose between different templates
						final String templateId = documentEmail.templateId;
						try {
							final TemplateInfo usedTemplateInfo = new TemplateInfo(connectionProvider,
									report.getDocTypeId(), report.getOrgId(), vars.getLanguage(), templateId);
							report.setTemplateInfo(usedTemplateInfo);
						} catch (final ReportingException e) {
							throw new ServletException("Error trying to get template information", e);
						}

						// Check if the document is not in status 'draft'
						if (!report.isDraft()) {
							// Check if the report is already attached
							if (!report.isAttached()) {
								// get the Id of the entities table, this is used to
								// store the file as an OB attachment
								final String tableId = ToolsData.getTableId(connectionProvider,
										report.getDocumentType().getTableName());

								// If the user wants to archive the document
								if (vars.getStringParameter("inpArchive").equals("Y")) {
									// Save the report as a attachment because it is
									// being transferred to the user
									try {
										reportManager.createAttachmentForReport(connectionProvider, report, tableId,
												vars, ReportManager.GENERATED_BY_EMAILING);
									} catch (final ReportingException exception) {
										throw new ServletException(exception);
									}
								} else {
									reportManager.saveTempReport(report, vars);
								}
							} else {
								if (log4j.isDebugEnabled())
									log4j.debug("Document is not attached.");
							}
							final String senderAddress = vars.getStringParameter("fromEmail");

							if (emails != null) {
								sendDocumentEmail(report, vars,
										(Vector<Object>) request.getSession().getAttribute("files"), senderAddress,
										checks, documentType, documentEmail.documentId, emails, connectionProvider);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// Catching the exception here instead of throwing it to HSAS because this is
			// used in multi
			// part request making the mechanism to detect popup not to work.
			log4j.error("Error captured: ", e);
			bdErrorGeneralPopUp(request, response, "Error",
					Utility.translateError(connectionProvider, vars, vars.getLanguage(), e.getMessage()).getMessage());
		}
	}

	public Report buildReport(HttpServletResponse response, VariablesSecureApp vars, String strDocumentId,
			final ReportManager reportManager, DocumentType documentType, OutputTypeEnum outputType,
			ConnectionProvider connectionProvider) {
		return buildReport(response, vars, strDocumentId, reportManager, documentType, outputType, "default",
				connectionProvider);
	}

	public Report buildReport(HttpServletResponse response, VariablesSecureApp vars, String strDocumentId,
			final ReportManager reportManager, DocumentType documentType, OutputTypeEnum outputType, String templateId,
			ConnectionProvider connectionProvider) {
		String localStrDocumentId = strDocumentId;
		Report report = null;
		if (localStrDocumentId != null) {
			localStrDocumentId = localStrDocumentId.replaceAll("\\(|\\)|'", "");
		}
		try {
			report = new Report(connectionProvider, documentType, localStrDocumentId, vars.getLanguage(), templateId,
					multiReports, outputType);
		} catch (final ReportingException e) {
			log4j.error(e);
		} catch (final ServletException e) {
			log4j.error(e);
		}

		reportManager.setTargetDirectory(report);
		return report;
	}

	void sendDocumentEmail(Report report, VariablesSecureApp vars, Vector<Object> object, String senderAddress,
			HashMap<String, Boolean> checks, DocumentType documentType, String documentId, String strEmail,
			ConnectionProvider connectionProvider) throws IOException, ServletException {
		final String attachmentFileLocation = report.getTargetLocation();
		String emailSubject = null, emailBody = null;
		final String ourReference = report.getOurReference();
		final String cusReference = report.getCusReference();
		if (log4j.isDebugEnabled())
			log4j.debug("our document ref: " + ourReference);
		if (log4j.isDebugEnabled())
			log4j.debug("cus document ref: " + cusReference);

		String toEmail = null;

		toEmail = strEmail;

		if (differentDocTypes.size() > 1) {
			try {
				EmailDefinition emailDefinition = report.getDefaultEmailDefinition();
				emailSubject = emailDefinition.getSubject();
				emailBody = emailDefinition.getBody();
			} catch (ReportingException e) {
				log4j.error(e.getMessage(), e);
			}
		} else {
			emailSubject = vars.getStringParameter("emailSubject");
			emailBody = vars.getStringParameter("emailBody");
		}

		// TODO: Move this to the beginning of the print handling and do nothing
		// if these conditions fail!!!)

		// Replace special tags

		emailSubject = emailSubject.replaceAll("@cus_ref@", Matcher.quoteReplacement(cusReference));
		emailSubject = emailSubject.replaceAll("@our_ref@", Matcher.quoteReplacement(ourReference));
		emailSubject = emailSubject.replaceAll("@bp_nam@", Matcher.quoteReplacement(report.getBPName()));
		emailSubject = emailSubject.replaceAll("@doc_date@", Matcher.quoteReplacement(report.getDocDate()));
		emailSubject = emailSubject.replaceAll("@doc_nextduedate@", Matcher.quoteReplacement(report.getMinDueDate()));
		emailSubject = emailSubject.replaceAll("@doc_lastduedate@", Matcher.quoteReplacement(report.getMaxDueDate()));
		emailSubject = emailSubject.replaceAll("@doc_desc@", Matcher.quoteReplacement(report.getDocDescription()));

		emailBody = emailBody.replaceAll("@cus_ref@", Matcher.quoteReplacement(cusReference));
		emailBody = emailBody.replaceAll("@our_ref@", Matcher.quoteReplacement(ourReference));
		emailBody = emailBody.replaceAll("@bp_nam@", Matcher.quoteReplacement(report.getBPName()));
		emailBody = emailBody.replaceAll("@doc_date@", Matcher.quoteReplacement(report.getDocDate()));
		emailBody = emailBody.replaceAll("@doc_nextduedate@", Matcher.quoteReplacement(report.getMinDueDate()));
		emailBody = emailBody.replaceAll("@doc_lastduedate@", Matcher.quoteReplacement(report.getMaxDueDate()));
		emailBody = emailBody.replaceAll("@doc_desc@", Matcher.quoteReplacement(report.getDocDescription()));

		String host = null;
		boolean auth = true;
		String username = null;
		String password = null;
		String connSecurity = null;
		int port = 25;

		OBContext.setAdminMode(true);
		try {
			final EmailServerConfiguration mailConfig = OBDal.getInstance().get(EmailServerConfiguration.class,
					vars.getStringParameter("fromEmailId"));

			host = mailConfig.getSmtpServer();

			if (!mailConfig.isSMTPAuthentification()) {
				auth = false;
			}
			username = mailConfig.getSmtpServerAccount();
			password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
			connSecurity = mailConfig.getSmtpConnectionSecurity();
			port = mailConfig.getSmtpPort().intValue();
		} finally {
			OBContext.restorePreviousMode();
		}

		final String recipientTO = toEmail;
		final String recipientCC = vars.getStringParameter("ccEmail");
		final String recipientBCC = vars.getStringParameter("bccEmail");
		final String replyTo = null;
		final String contentType = "text/plain; charset=utf-8";

		if (log4j.isDebugEnabled()) {
			log4j.debug("From: " + senderAddress);
			log4j.debug("Recipient TO (contact email): " + recipientTO);
			log4j.debug("Recipient CC: " + recipientCC);
			log4j.debug("Recipient BCC (user email): " + recipientBCC);
			log4j.debug("Reply-to (sales rep email): " + replyTo);
		}

		List<File> attachments = new ArrayList<File>();
		attachments.add(new File(attachmentFileLocation));

		if (object != null) {
			final Vector<Object> vector = object;
			for (int i = 0; i < vector.size(); i++) {
				final AttachContent objContent = (AttachContent) vector.get(i);
				final File file = prepareFile(objContent, ourReference);
				attachments.add(file);
			}
		}

		try {
			EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress, recipientTO,
					recipientCC, recipientBCC, replyTo, emailSubject, emailBody, contentType, attachments, new Date(),
					null);
		} catch (Exception exception) {
			log4j.error(exception);
			final String exceptionClass = exception.getClass().toString().replace("class ", "");
			String exceptionString = "Problems while sending the email" + exception;
			exceptionString = exceptionString.replace(exceptionClass, "");
			throw new ServletException(exceptionString);
		} finally {
			// Delete the temporary files generated for the email attachments
			for (File attachment : attachments) {
				if (attachment.exists() && !attachment.isDirectory()) {
					attachment.delete();
				}
			}
		}

		// Store the email in the database
		Connection conn = null;
		try {
			conn = connectionProvider.getTransactionConnection(); // this.getTransactionConnection();

			// First store the email message
			final String newEmailId = SequenceIdData.getUUID();
			if (log4j.isDebugEnabled())
				log4j.debug("New email id: " + newEmailId);

			EmailData.insertEmail(conn, connectionProvider, newEmailId, vars.getClient(), report.getOrgId(),
					vars.getUser(), EmailType.OUTGOING.getStringValue(), replyTo, recipientTO, recipientCC,
					recipientBCC, Utility.formatDate(new Date(), "yyyyMMddHHmmss"), emailSubject, emailBody,
					report.getBPartnerId(),
					ToolsData.getTableId(connectionProvider, report.getDocumentType().getTableName()), documentId);

			conn.commit();
		} catch (final NoConnectionAvailableException exception) {
			log4j.error(exception);
			throw new ServletException(exception);
		} catch (final SQLException exception) {
			log4j.error(exception);
			try {
				conn.rollback();
			} catch (final Exception ignored) {
			}

			throw new ServletException(exception);
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
			}
		}

	}

	private File prepareFile(AttachContent content, String documentId) throws ServletException {
		try {
			final String attachPath = OBPropertiesProvider.getInstance().getOpenbravoProperties()
					.getProperty("attach.path") + "/tmp";
			final File f = new File(attachPath, content.getFileName());
			final InputStream inputStream = content.getFileItem().getInputStream();
			final OutputStream out = new FileOutputStream(f);
			final byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			return f;
		} catch (final Exception e) {
			throw new ServletException("Error trying to get the attached file", e);
		}

	}

	void createPrintStatusPage(HttpServletResponse response, VariablesSecureApp vars, int nrOfEmailsSend)
			throws IOException, ServletException {
		XmlDocument xmlDocument = null;
		xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/utility/reporting/printing/PrintStatus")
				.createXmlDocument();
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
		xmlDocument.setParameter("theme", vars.getTheme());
		xmlDocument.setParameter("language", vars.getLanguage());
		xmlDocument.setParameter("nrOfEmailsSend", "" + nrOfEmailsSend);

		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();

		out.println(xmlDocument.print());
		out.close();
	}
}
