package com.atrums.contabilidad.erpCommon.ad_reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.utils.FormatUtilities;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * @author ATRUMS-IT
 *
 */
public class CoEnviarLibroMayor implements Process {
  static Logger log4j = Logger.getLogger(CoEnviarLibroMayor.class);
  final OBError msg = new OBError();

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    CODatosLibroMayorData[] coEnvioReporteId;

    String fechainicio = null;
    String fechafin = null;
    String cuenta = null;
    String cuentaHasta = null;
    String email = null;
    String enviar_reporte_id = null;
    String empresa = null;
    String strCuenta = null;
    String strNombreCuenta = null;
    String strCuentaHasta = null;
    String strNombreCuentaHasta = null;
    boolean resultado = false;

    try {
    	coEnvioReporteId = CODatosLibroMayorData.selecionarDatosReporte(conn,
          varsAux.getClient());

      if (coEnvioReporteId.length > 0) {

    	  fechainicio = coEnvioReporteId[0].fechainicio;
    	  fechafin = coEnvioReporteId[0].fechafin;
    	  cuenta = coEnvioReporteId[0].cElementvalueId;
    	  cuentaHasta = coEnvioReporteId[0].cElementvaluehastaId;
    	  email = coEnvioReporteId[0].email;
    	  enviar_reporte_id = coEnvioReporteId[0].coEnvioReportesId;
    	  empresa = coEnvioReporteId[0].empresa;
    	  strCuenta = coEnvioReporteId[0].value;
    	  strNombreCuenta = coEnvioReporteId[0].nombreCuenta;
    	  strCuentaHasta = coEnvioReporteId[0].valueHasta;
    	  strNombreCuentaHasta = coEnvioReporteId[0].nombreCuentaHasta;

    	  resultado = enviarEmailReporte(conn, fechainicio, fechafin, cuenta, cuentaHasta, email, empresa, strCuenta.toString()+" - "+strNombreCuenta.toString(), strCuentaHasta.toString()+" - "+strNombreCuentaHasta.toString(), varsAux.getClient());
    	  
    	  if (resultado == true) {
    		  CODatosLibroMayorData.actualizarEnvioLibro(conn, enviar_reporte_id);
    	  }else {
    		  CODatosLibroMayorData.actualizarNoEnvioLibro(conn, enviar_reporte_id);
    	  }
    	  
      }      
    } catch (Exception ex) {
      Log.error(ex.getMessage(), ex);
      if (resultado == false) {
    	  CODatosLibroMayorData.actualizarNoEnvioLibro(conn, enviar_reporte_id);
      }
    } finally {
      // TODO: handle finally clause
    	coEnvioReporteId = null;
        OBDal.getInstance().rollbackAndClose();
    }
  } 
  
  public boolean enviarEmailReporte(ConnectionProvider connectionProvider, String fechainicio,
		  String fechafin, String cuenta, String cuentaHasta, String email, String empresa, String strCuenta, String strCuentaHasta, String adClientId) throws Exception {

		File flXls = null;
		String strContenido = "";
		String type = null;

        
        type = "text/html; charset=utf-8";
        
        if (cuenta.equals(cuentaHasta)) {
        	strContenido = "Estimado Usuario.\n"
            		+ "Reciba adjunto el Informe de Libro Mayor, correspondiente a la Cuenta N°: " + strCuenta +".\n"
            		+ "La información se encuentra en el rango de fechas: " + fechainicio + " a "+ fechafin +".";
        }else {
        	strContenido = "Estimado Usuario.\n"
            		+ "Reciba adjunto el Informe de Libro Mayor, correspondiente a la Cuenta N°: " + strCuenta +", hasta la Cuenta N°: " + strCuentaHasta + ". \n"
            		+ "La información se encuentra en el rango de fechas: " + fechainicio + " a "+ fechafin +".";
        }
        
        if (!cuenta.equals(cuentaHasta)) {
        	strCuenta = strCuenta + " a " +strCuentaHasta;
        }
        
                
        flXls = generarXls(connectionProvider,
				"@basedesign@/com/atrums/contabilidad/ad_reports/Rpt_Co_ReporteLibroMayor.jrxml", strCuenta.toString(), fechainicio, fechafin, cuenta, cuentaHasta, email, empresa, adClientId);
        List<File> lisdoc = new ArrayList<File>();
        lisdoc.add(flXls);
        
        System.out.println(lisdoc);
        
        if (enviarCorreo(email, "Informe Libro Mayor - Cta. " + strCuenta,
			strContenido, type, lisdoc, false)) {
        	flXls.delete();
		    return true;
		}else {
			return false;
		}
         
	}
  
  
  @SuppressWarnings("deprecation")
public static File generarXls(ConnectionProvider conn, String strBaseDesin, String strNombre,
		  String fechainicio, String fechafin, String cuenta, String cuentaHasta, String email, String empresa, String adClientId) throws ServletException {

	    File flTemp = null;
	    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");

	    try {

	      String strBaseDesign = strBaseDesin;

	      if (!basedesign(strBaseDesign).equals("")) {

	        Map<String, Object> parameters = new HashMap<String, Object>();
	        parameters.put("DateFrom", formato.parse(fechainicio));
	        parameters.put("DateTo", formato.parse(fechafin));
	        parameters.put("ElementValueId", cuenta);
	        parameters.put("ElementValueHastaId", cuentaHasta);
	        parameters.put("AdClientId", adClientId);
	        parameters.put("Empresa", empresa);
	        parameters.put("BASE_DESIGN", basedesign(strBaseDesign));
	        parameters.put("IS_IGNORE_PAGINATION", true);

	        strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

	        JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);
	        Connection con = conn.getTransactionConnection();
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, con);
	        

	        JRXlsxExporter xlsExporter = new JRXlsxExporter();
	        
	        String strNombreArch = strNombre;
	        OutputStream out = null;

	        flTemp = File.createTempFile(strNombreArch+"_", ".xlsx", null);
	        out = new FileOutputStream(flTemp);
	        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
	        
	        try {	
	        	SimpleExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
	            SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(out);
	            xlsExporter.setExporterInput(exporterInput);
	            xlsExporter.setExporterOutput(exporterOutput);
	            xlsExporter.exportReport();
	            
	            
	          } catch (Exception ex) {
	            ex.printStackTrace();
	          }
	        
            
	        out.write(byteout.toByteArray());
	        out.flush();
	        out.close();
	      }

	    } catch (JRException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } catch (NoConnectionAvailableException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } catch (SQLException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } catch (Exception e) {
	      // TODO: handle exception
	      e.printStackTrace();
	    }
	    return flTemp;
	  }

	  /**
	   * @param strReporte
	   * @return
	   */
	  public static String basedesign(String strReporte) {

	    String strDirectorio = CoEnviarLibroMayor.class.getResource("/").getPath();

	    if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
	      int intPos = strDirectorio.indexOf("/");

	      if (intPos != -1) {
	        strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
	      }
	    }

	    String strDirectorioInv = "";

	    for (int i = 0; i < strDirectorio.length(); i++) {
	      strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
	    }

	    int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

	    if (intPosicion != -1) {
	      strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
	    } else {
	      intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
	      if (intPosicion != -1) {
	        strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
	      } else {
	        return "";
	      }
	    }

	    strDirectorio = "";

	    for (int i = 0; i < strDirectorioInv.length(); i++) {
	      strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
	    }

	    String strBaseDesign = strReporte.replaceAll("@basedesign@",
	        (strDirectorio + "WebContent/src-loc/design"));

	    File flRep = new File(strBaseDesign);

	    if (flRep.exists()) {
	      return strDirectorio + "WebContent/src-loc/design";
	    } else {
	      strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

	      flRep = new File(strBaseDesign);

	      if (flRep.exists()) {
	        return strDirectorio + "src-loc/design";
	      } else {
	        return "";
	      }
	    }
	  }

	  /**
	   * @param recipient
	   * @param subject
	   * @param body
	   * @param type
	   * @param attachments
	   * @return
	   */
	  public static boolean enviarCorreo(final String recipient, final String subject,
	      final String body, final String type, final List<File> attachments, boolean soporte) {
	    try {
	      Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
	      final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

	      if (mailConfig == null) {
	        return false;
	      }

	      final String username = mailConfig.getSmtpServerAccount();
	      final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(),
	          false);
	      final String connSecurity = mailConfig.getSmtpConnectionSecurity();
	      final int port = mailConfig.getSmtpPort().intValue();
	      final String senderAddress = mailConfig.getSmtpServerSenderAddress();
	      final String host = mailConfig.getSmtpServer();
	      final boolean auth = mailConfig.isSMTPAuthentification();

	      String auxRecipient = recipient;

	      if (soporte) {
	        auxRecipient = username; // username;
	      }

	      EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
	          auxRecipient, null, null, null, subject, body, type, attachments, null, null);
	      return true;
	    } catch (Exception e) {
	      // TODO: handle exception
	      log4j.error(e.getMessage());
	      return false;
	    }
	  }



}
