package com.atrums.contabilidad.erpCommon.ad_reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.hibernate.id.UUIDGenerator;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.report.ReportingUtils;
import org.openbravo.client.application.report.ReportingUtils.ExportType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.ScrollableFieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.TreeData;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.erpCommon.utility.*;

import com.atrums.declaraciontributaria.ecuador.util.UtilProcesoProcedure;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * @author ATRUMS-IT
 *
 */
public class CoEnviarReportesContabilidad implements Process {
	static Logger log4j = Logger.getLogger(CoEnviarReportesContabilidad.class);
	final OBError msg = new OBError();
	private HttpServletResponse response;
	

	@Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    CODatosReporteData[] coDatosReporte;
    CODatosReporteData[] accionActual;
    CODatosLibroMayorData[] coAdjuntarArchivo;

    String coOpcionDescargaReporteId = null;
    String fechainicio = null;
    String fechafin = null;
    String ejecuta = null;
    String email = null;
    String reporte = null;
    String direccionReporte = null;
    String empresa = null;
    String sucursal = null;
    String strCuenta = null;
    String strNombreCuenta = null;
    String strCuentaHasta = null;
    String strNombreCuentaHasta = null;
    String cuenta = null;
    String cuentaHasta = null;
    String cbPartnerId = null;
    boolean resultado = false;

    try {
    	//Verifica si existe algún reporte en ejecución
    	
    	accionActual = CODatosReporteData.validarReportesEnEjecucionCont(conn, varsAux.getClient());
    	
    	if(accionActual.length == 0) {
    		ejecuta = "Ejecutar";
    	}else {
    		ejecuta = "En ejecucion...";
    	}
    	
    	System.out.println("ejecuta: " + ejecuta);
    	
    	if(ejecuta.equals("Ejecutar"))
		{
		      coDatosReporte = CODatosReporteData.selecionarReporteContabilidad(conn,
		          varsAux.getClient());
		
		      if (coDatosReporte.length > 0) {
		
		    	  coOpcionDescargaReporteId = coDatosReporte[0].coOpcionDescargaReporteId;
		    	  
		    	  CODatosReporteData.actualizarReporteEnEjecucionCont(conn, coOpcionDescargaReporteId);
		    	   
		    	  fechainicio = coDatosReporte[0].fechainicio;
		    	  fechafin = coDatosReporte[0].fechafin;
		    	  email = coDatosReporte[0].email;
		    	  reporte = coDatosReporte[0].nombrereporte;
		    	  direccionReporte = coDatosReporte[0].direccionReporte;
		    	  empresa = coDatosReporte[0].empresa;
		    	  strCuenta = coDatosReporte[0].value;
		    	  strNombreCuenta = coDatosReporte[0].nombreCuenta;
		    	  strCuentaHasta = coDatosReporte[0].valueHasta;
		    	  strNombreCuentaHasta = coDatosReporte[0].nombreCuentaHasta;
		    	  cuenta = coDatosReporte[0].cElementvalueId;
		    	  cuentaHasta = coDatosReporte[0].cElementvaluehastaId;
		    	  sucursal = coDatosReporte[0].adOrgId;
		    	  cbPartnerId  = coDatosReporte[0].cBpartnerId;
		    	  
		    	  if(!cbPartnerId.equals(null) && !cbPartnerId.equals("")) {
		    		  cbPartnerId = " ('"+cbPartnerId+"')";
		    	  }else {
		    		  cbPartnerId = "";
		    	  }
		
		    	  //Prueba
		    	  resultado = enviarEmailReporte(conn, varsAux, fechainicio, fechafin, reporte, direccionReporte, cuenta, cuentaHasta, email, empresa, strCuenta, strCuentaHasta, varsAux.getClient(), sucursal, cbPartnerId, coOpcionDescargaReporteId);

		    	  if (resultado == true) {
		    		  /*********** DIRECTORIO AMBIENTE LOCAL ******************************/
		    		  //String dirInicial = "C:/_Desarrollo/AplicacionesFuentes/dontablero/openbravodontablero/attachments/";
		    		  /*********** DIRECTORIO PRODUCCION **********************************/
		    		  String dirInicial = "/opt/OpenbravoTablero/attachments/";
		    		  
		    		  
		    		  String directorio = SubCadenas(coOpcionDescargaReporteId);
		    		  crearDirectorio(dirInicial,directorio);
		    		  
		    		  CODatosReporteData.actualizarReporteEnviadoCont(conn, coOpcionDescargaReporteId);

		    	  }else {
		    		  CODatosReporteData.actualizarReporteErrorCont(conn, coOpcionDescargaReporteId);
		    	  }
		    	  
		      }
			}
		    } catch (Exception ex) {
		      Log.error(ex.getMessage(), ex);
		      if (resultado == false) {
		    	  CODatosReporteData.actualizarReporteErrorCont(conn, coOpcionDescargaReporteId);
		      }
		    } finally {
		      // TODO: handle finally clause
		    	coDatosReporte = null;
		        OBDal.getInstance().rollbackAndClose();
		    }  
  }

  public boolean enviarEmailReporte(ConnectionProvider connectionProvider, VariablesSecureApp varsAux,String fechainicio,
				  String fechafin, String reporte, String direccionreporte, String cuenta, String cuentaHasta, String email, 
				  String empresa, String strCuenta, String strCuentaHasta, String adClientId,
				  String adAdOrgId, String cbPartnerId, String coOpcionDescargaReporteId) throws Exception {


		File flXls = null;
		String strContenido = "";
		String type = null;
	    CODatosReporteData[] coInformacionATS;
	    String strNombreFile = "";
	    String strNombreXml = "";
	    String strFechaInicio = "";
	    String strFechaFin = "";
	    String strAdOrgId = "";
	    String identificadorRep = "";
	    String[] arrFecha = null;
	    String nombreATS = null;
		

		type = "text/html; charset=utf-8";

		identificadorRep = reporte.split("_")[0];
		System.out.println("identificadorRep: " +  identificadorRep);
		System.out.println("fechainicio: " +  fechainicio);
		
		if (identificadorRep.equals("Archivo ATS")) {
			arrFecha = fechainicio.toString().split("-", 3);
			nombreATS = (arrFecha[1].toString()).concat(arrFecha[2].toString());
			reporte = "ATS_".concat(nombreATS); 	  	
		}
		
		System.out.println("reporte: " + reporte);
		
		if (!identificadorRep.equals("Libro Mayor") && !identificadorRep.equals("Libro Mayor PDF")) {
				strContenido = "\nEstimado Usuario."  
						+ "\n\nEn el presente correo, se envia adjunto el reporte correspondiente a " + reporte  + "."
						+ " El reporte se encuentra generado en el rango de fechas: " + fechainicio + " a " + fechafin + "."
						+ " \n\nSaludos Cordiales,"
						+ "\nATRUMS IT";
		}else {
			
			if (identificadorRep.equals("Libro Mayor PDF")) {
				strContenido = "\nEstimado Usuario. "  
						+ "\n\nEn el presente correo, se Notifica que el reporte correspondiente a Libro Mayor en formato PDF"  
						+ " , generado en el rango de fechas: " + fechainicio + " a " + fechafin + ", se encuentra disponible para su descarga."
						+ ". El reporte ha sido generado de la cuenta " + strCuenta + " a la cuenta " + strCuentaHasta
						+ "\n\nEl reporte lo puede DESCARGAR en el sistema, en la linea del reporte solicitado."
						+ "\n\nSaludos Cordiales,"
						+ "\nATRUMS IT";
			}else {
	        	strContenido = "\nEstimado Usuario. "  
						+ "\n\nEn el presente correo, se Notifica que el reporte correspondiente a Libro Mayor en formato Excel"  
						+ " , generado en el rango de fechas: " + fechainicio + " a " + fechafin + ", se encuentra disponible para su descarga."
						+ ". El reporte ha sido generado de la cuenta " + strCuenta + " a la cuenta " + strCuentaHasta
						+ "\n\nEl reporte lo puede DESCARGAR en el sistema, en la linea del reporte solicitado."
						+ "\n\nSaludos Cordiales,"
						+ "\nATRUMS IT";
			}
        	
        }
		
        
        if (!cuenta.equals(null)) {
        	strCuenta = strCuenta + " a " +strCuentaHasta;
        }
		
		if(!identificadorRep.equals("Archivo ATS")) {
			
			if (identificadorRep.equals("Libro Mayor") || identificadorRep.equals("Libro Mayor PDF")) {
				HttpServletRequest request = null;
				HttpServletResponse response = null;
				
				if (identificadorRep.equals("Libro Mayor PDF")) {
					printPageDataPDF(request, response, varsAux, fechainicio, fechafin, "", "",
							cuenta, cuentaHasta, adAdOrgId, cbPartnerId, "",
				            "", "", "D6A0D86D33314E079C3319C87257D31F", "1", "", connectionProvider,
				            reporte, email, empresa, strAdOrgId,strCuenta, strCuentaHasta, strContenido,
				            coOpcionDescargaReporteId);
				}else {
					
					printPageDataXLS(request, response, varsAux, fechainicio, fechafin, "", "",
							cuenta, cuentaHasta, adAdOrgId, cbPartnerId, "",
				            "", "", "D6A0D86D33314E079C3319C87257D31F", "1", "", connectionProvider,
				            reporte, email, empresa, strAdOrgId, strCuenta, strCuentaHasta, strContenido,
				            coOpcionDescargaReporteId);
				}
				
			}else{
				flXls = generarXlS(connectionProvider, "@basedesign@" + direccionreporte, normalizacionPalabras(reporte), fechainicio,
						fechafin, cuenta, cuentaHasta, email, empresa, adClientId, adAdOrgId, coOpcionDescargaReporteId);
				
				List<File> lisdoc = new ArrayList<File>();
				lisdoc.add(flXls);

				System.out.println(lisdoc);

				if (enviarCorreo(email, normalizacionPalabras("DON TABLERO - " + reporte), normalizacionPalabras(strContenido), type, lisdoc, false)) {
					//flXls.delete();
					return true;
				} else {
					return false;
				}
			}
			
		} else {
			
			coInformacionATS = CODatosReporteData.selectConfigATS(connectionProvider);
		    strNombreFile = coInformacionATS[0].nombreArchivo;
		    strNombreXml = coInformacionATS[0].nombreXml;
		    strFechaInicio = fechainicio;
		    strFechaFin = fechafin;
		    strAdOrgId = coInformacionATS[0].adOrgId;
		      
			flXls = generarATS(connectionProvider, strNombreFile, strNombreXml, strFechaInicio, strFechaFin, strAdOrgId, adClientId);	
			
			
			List<File> lisdoc = new ArrayList<File>();
			lisdoc.add(flXls);

			System.out.println(lisdoc);

			if (enviarCorreo(email, normalizacionPalabras("DON TABLERO - " + reporte), normalizacionPalabras(strContenido), type, lisdoc, false)) {
				flXls.delete();
				return true;
			} else {
				return false;
			}
		}
	
		return true;

	}

		public static File generarXlS(ConnectionProvider conn, String strBaseDesin, String strNombre,
				  String fechainicio, String fechafin, String cuenta, String cuentaHasta, String email, 
				  String empresa, String adClientId, String adOrgId, String coOpcionDescargaReporteId) throws ServletException {

		File flTemp = null;
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");

		try {

			String strBaseDesign = strBaseDesin;

			if (!basedesign(strBaseDesign).equals("")) {

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("DateFrom", formato.parse(fechainicio));
				parameters.put("DateTo", formato.parse(fechafin));
				parameters.put("Detalle", "Y");
				parameters.put("AdClientId", adClientId);
				parameters.put("AdOrgId", adOrgId);
				parameters.put("USER_CLIENT", adClientId);
				parameters.put("Empresa", empresa);
				parameters.put("BASE_DESIGN", basedesign(strBaseDesign));
				parameters.put("ElementValueId", cuenta);
		        parameters.put("ElementValueHastaId", cuentaHasta);
				parameters.put("IS_IGNORE_PAGINATION", true);

				strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

				JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);
				Connection con = conn.getTransactionConnection();
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, con);

				JRXlsxExporter xlsExporter = new JRXlsxExporter();

				String strNombreArch = strNombre.replace(" ", "_");
				OutputStream out = null;

				flTemp = File.createTempFile(strNombreArch + "_", ".xlsx", null);
				out = new FileOutputStream(flTemp);
				ByteArrayOutputStream byteout = new ByteArrayOutputStream();

				try {
					SimpleExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
					SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(out);
					xlsExporter.setExporterInput(exporterInput);
					xlsExporter.setExporterOutput(exporterOutput);
					xlsExporter.exportReport();

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				out.write(byteout.toByteArray());
				out.flush();
				out.close();
			}

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoConnectionAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return flTemp;
	}

	/**
	 * @param strReporte
	 * @return
	 */
	public static String basedesign(String strReporte) {

		String strDirectorio = CoEnviarReportesContabilidad.class.getResource("/").getPath();

		if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
			int intPos = strDirectorio.indexOf("/");

			if (intPos != -1) {
				strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
			}
		}

		String strDirectorioInv = "";

		for (int i = 0; i < strDirectorio.length(); i++) {
			strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
		}

		int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

		if (intPosicion != -1) {
			strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
		} else {
			intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
			if (intPosicion != -1) {
				strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
			} else {
				return "";
			}
		}

		strDirectorio = "";

		for (int i = 0; i < strDirectorioInv.length(); i++) {
			strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
		}

		String strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "WebContent/src-loc/design"));

		File flRep = new File(strBaseDesign);

		if (flRep.exists()) {
			return strDirectorio + "WebContent/src-loc/design";
		} else {
			strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

			flRep = new File(strBaseDesign);

			if (flRep.exists()) {
				return strDirectorio + "src-loc/design";
			} else {
				return "";
			}
		}
	}

	/**
	 * @param recipient
	 * @param subject
	 * @param body
	 * @param type
	 * @param attachments
	 * @return
	 */
	public static boolean enviarCorreo(final String recipient, final String subject, final String body,
			final String type, final List<File> attachments, boolean soporte) {
		try {
			Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
			final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

			if (mailConfig == null) {
				return false;
			}

			final String username = mailConfig.getSmtpServerAccount();
			final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
			final String connSecurity = mailConfig.getSmtpConnectionSecurity();
			final int port = mailConfig.getSmtpPort().intValue();
			final String senderAddress = mailConfig.getSmtpServerSenderAddress();
			final String host = mailConfig.getSmtpServer();
			final boolean auth = mailConfig.isSMTPAuthentification();

			String auxRecipient = recipient;

			if (soporte) {
				auxRecipient = username; // username;
			}

			EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress, auxRecipient,
					null, null, null, subject, body, type, attachments, null, null);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			log4j.error(e.getMessage());
			return false;
		}
	}
	
	
	public static boolean enviarCorreoZip(final String recipient, final String subject, final String body,
			final String type, final ZipOutputStream attachments, boolean soporte, String coOpcionDescargaReporteId,
			String nombreArchivo) {
		try {
			Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
			final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

			if (mailConfig == null) {
				return false;
			}

			final String username = mailConfig.getSmtpServerAccount();
			final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
			final String connSecurity = mailConfig.getSmtpConnectionSecurity();
			final int port = mailConfig.getSmtpPort().intValue();
			final String senderAddress = mailConfig.getSmtpServerSenderAddress();
			final String host = mailConfig.getSmtpServer();
			final boolean auth = mailConfig.isSMTPAuthentification();

			String auxRecipient = recipient;

			if (soporte) {
				auxRecipient = username; // username;
			}

			EmailManager.sendEmailZip(host, auth, username, password, connSecurity, port, senderAddress, auxRecipient,
					null, null, null, subject, body, type, attachments, null, null, nombreArchivo);
			
			
			
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			log4j.error(e.getMessage());
			return false;
		}
	}
	
	
	
	  public String normalizacionPalabras(String strPalabras) {

		    String strPalabraAux = "";

		    strPalabraAux = strPalabras.replaceAll("á", "a");
		    strPalabraAux = strPalabraAux.replaceAll("é", "e");
		    strPalabraAux = strPalabraAux.replaceAll("í", "i");
		    strPalabraAux = strPalabraAux.replaceAll("ó", "o");
		    strPalabraAux = strPalabraAux.replaceAll("ú", "u");
		    return strPalabraAux;
		  }
	  
	  private File generarATS(ConnectionProvider conn, String strNombreFile, String strNombreXml, 
			  String strFechaInicio, String strFechaFin, String strAdOrgId, String strAdClientId) throws IOException, ServletException, JRException {
	
		  	File flTemp = null;
		    StringBuffer StrXML = new StringBuffer();
		    String strMessage = "";
		    String strProcedimiento = "";
		    String strResult = "";


		    try {
		    	
		      CODatosReporteData [] cad = CODatosReporteData.selectConfigATS(conn);

		      StrXML.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n");
		      StrXML.append("<" + strNombreXml + ">\n"); // Cabecera del archivo

		      for (int i = 0; i < cad.length; i++) {
		        strProcedimiento = cad[i].procedimiento;
		        if (strProcedimiento == null || strProcedimiento.equals(""))
		          
		        	strResult = "No se a ingresado un Stored Procedure para realizar el Anexo";

		        if (cad[i].tipoInformacion.contentEquals("CB"))// Cabecera
		        {
		          StrXML.append(UtilProcesoProcedure.ejecutaProcedure(conn, strProcedimiento,
		              strAdOrgId + "','" + strFechaInicio, "strXml") + "\n");
		        }
		        if (cad[i].tipoInformacion.contentEquals("FC"))// F. Compras
		        {

		          BuscaFacturaData[] bfd = BuscaFacturaData.selectInvoice(conn, strFechaInicio, strFechaFin,
		        		  strAdClientId, "N");
		          StrXML.append("<compras>\n");
		          for (int j = 0; j < bfd.length; j++) {

		            strResult = UtilProcesoProcedure.ejecutaProcedure(conn, strProcedimiento,
		                bfd[j].cInvoiceId, "strXml");

		            StrXML.append(strResult == null ? "" : "\t" + strResult + "\n");
		          }
		          StrXML.append("</compras>\n");
		        }

		        if (cad[i].tipoInformacion.contentEquals("FV"))// F. Ventas
		        {

		          BuscaFacturaData[] bfd = BuscaFacturaData.selectVentas(conn, strFechaInicio, strFechaFin,
		        		  strAdClientId, "Y");
		          StrXML.append("<ventas>\n");
		          for (int j = 0; j < bfd.length; j++) {

		            StrXML.append("\t" + UtilProcesoProcedure.ejecutaProcedure(conn, strProcedimiento,
		                bfd[j].tpidcliente, bfd[j].idcliente, bfd[j].tipocomprobante,
		                bfd[j].numerocomprobantes, bfd[j].basenograiva, bfd[j].baseimponible,
		                bfd[j].baseimpgrav, bfd[j].montoiva, bfd[j].valorretiva, bfd[j].valorretrenta,
		                bfd[j].fechainicio, bfd[j].fechafin, strAdClientId, "strXml") + "\n");

		          }
		          StrXML.append("</ventas>\n");

		        }

		        if (cad[i].tipoInformacion.contentEquals("ATS_VE"))// Ventas Establecimiento
		        {
		          StrXML.append("<ventasEstablecimiento>\n");

		          ArrayList<String> est = UtilProcesoProcedure.ejecutaProcedure(conn, strProcedimiento,
		        		  strAdClientId, strFechaInicio, strFechaFin, "strXml");

		          for (int m = 0; m < est.size(); m++) {
		            StrXML.append(est.isEmpty() ? "" : "\t" + est.get(m) + "\n");
		          }

		          StrXML.append("</ventasEstablecimiento>\n");
		        }

		        if (cad[i].tipoInformacion.contentEquals("AN"))// Doc. Anulados
		        {

		          ArrayList<String> anul = UtilProcesoProcedure.ejecutaProcedure(conn, strProcedimiento,
		              strFechaInicio, strFechaFin, strAdClientId, "strXml");

		          if (anul.isEmpty())
		            StrXML.append("");
		          else {
		            StrXML.append("<anulados>\n");
		            for (int w = 0; w < anul.size(); w++) {
		              StrXML.append("\t" + anul.get(w) + "\n");
		            }
		            StrXML.append("</anulados>\n");
		          }
		        }

		      }
		      StrXML.append("</" + strNombreXml + ">");

		    } catch (Exception e) {
		      strMessage = e.getMessage();
	  }
		    
		      OutputFormat ofFormat = null;
		      DocumentFactory documentFactory = DocumentFactory.getInstance();
	          Document document = documentFactory.createDocument();
	          
	          String[] arrFecha = strFechaInicio.split("-", 3);
	          String periodoATS = (arrFecha[1].toString()).concat(arrFecha[2].toString()); 	  
		  	  strNombreFile = "ATS_".concat(periodoATS);
		  	  
		  	  System.out.println("strNombreFile: " + strNombreFile);
		  	
		      flTemp = File.createTempFile(strNombreFile, ".xml", null);
		      flTemp.deleteOnExit();
		      ofFormat = OutputFormat.createPrettyPrint();
		      
		      ///////////////////////////////////////////////////////
		      try {
		            // Parse the XML content from the StringBuffer
		            document = DocumentHelper.parseText(StrXML.toString());

		            // Now 'document' represents the XML content in the form of a org.dom4j.Document object

		            // Example: Print the content of the root element
		            //System.out.println("Root element: " + document.getRootElement().getName());

		            // Optionally, you can convert the Document back to a formatted XML string
		            //String formattedXml = formatXml(document);
		            //System.out.println("Formatted XML:\n" + formattedXml);

		        } catch (DocumentException e) {
		            e.printStackTrace();
		        }
		      ///////////////////////////////////////////////////////
		      
		      
		  	  final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flTemp), "ISO-8859-1"), ofFormat);
		      writer.write(document);
		      writer.flush();
		      writer.close();
		  		  
		  	  //byte[] bytes = filetobyte(flTemp);
		      //fileString = new String(bytes, "UTF-8");
		  			  		
			return flTemp;
		  
		  }

	  /**
	   * @param bytes
	   * @return
	   * @throws IOException
	   */
	  public static String mesTxt(String mes) throws IOException {
		  
		  String mesLetras = null;

		  if(mes == "1" || mes == "01") {
			  mesLetras = "Enero";
		  }
		  if(mes == "2" || mes == "02") {
			  mesLetras = "Febrero";
		  }
		  if(mes == "3" || mes == "03") {
			  mesLetras = "Marzo";
		  }
		  if(mes == "4" || mes == "04") {
			  mesLetras = "Abril";
		  }
		  if(mes == "5" || mes == "05") {
			  mesLetras = "Mayo";
		  }
		  if(mes == "6" || mes == "06") {
			  mesLetras = "Junio";
		  }
		  if(mes == "7" || mes == "07") {
			  mesLetras = "Julio";
		  }
		  if(mes == "8" || mes == "08") {
			  mesLetras = "Agosto";
		  }
		  if(mes == "9" || mes == "09") {
			  mesLetras = "Septiembre";
		  }
		  if(mes == "10") {
			  mesLetras = "Octubre";
		  }
		  if(mes == "11") {
			  mesLetras = "Noviembre";
		  }
		  if(mes == "12") {
			  mesLetras = "Diciembre";
		  }
		  
		  return mesLetras;
	  }  
	  
	  private void printPageDataXLS(HttpServletRequest request, HttpServletResponse response,
		      VariablesSecureApp vars, String strDateFrom, String strDateTo, String strAmtFrom,
		      String strAmtTo, String strcelementvaluefrom, String strcelementvalueto, String strOrg,
		      String strcBpartnerId, String strmProductId, String strcProjectId, String strGroupBy,
		      String strcAcctSchemaId, String strPageNo, String strShowOpenBalances,
		      ConnectionProvider conn,
		      String reporte, String email, String empresa, String sucursal, String strCuenta, String strCuentaHasta,
		      String strContenido, String coOpcionDescargaReporteId) throws IOException,
		      ServletException {
		    String localStrcelementvalueto = strcelementvalueto;
		    log4j.debug("Output: XLS");
		    //response.setContentType("text/html; charset=UTF-8");
		    String strTreeOrg = TreeData.getTreeOrg(conn, vars.getClient());
		    String strOrgFamily = getFamily(conn, strTreeOrg, strOrg);
		    String toDatePlusOne = DateTimeData.nDaysAfter(conn, strDateTo, "1");
		    

		    String strGroupByText = (strGroupBy.equals("BPartner") ? Utility.messageBD(conn, "BusPartner",
		        vars.getLanguage()) : (strGroupBy.equals("Product") ? Utility.messageBD(conn, "Product",
		        vars.getLanguage()) : (strGroupBy.equals("Project") ? Utility.messageBD(conn, "Project",
		        vars.getLanguage()) : "")));
		    String strAllaccounts = "Y";

		    if (strcelementvaluefrom != null && !strcelementvaluefrom.equals("")) {
		      if (localStrcelementvalueto.equals(""))
		        localStrcelementvalueto = strcelementvaluefrom;
		      strAllaccounts = "N";
		    }

		    COReportGeneralLedgerData data = null;
		    try {
		    	data = COReportGeneralLedgerData.selectexcel2(conn, "0", strGroupByText, strGroupBy, strAllaccounts,
			          strcelementvaluefrom, localStrcelementvalueto,
			          strOrgFamily,
			          "'" + vars.getClient() + "'",
			          "Y".equals(strShowOpenBalances) ? strDateTo : null, strcAcctSchemaId, strDateFrom,
			          toDatePlusOne, strOrgFamily, strcBpartnerId, strmProductId, strcProjectId, strAmtFrom,
			          strAmtTo, null, null, null, null, null, null);	      

		      // augment data with totals
		      AddTotals dataWithTotals = new AddTotals(data, strGroupBy, strcBpartnerId, strmProductId,
		          strcProjectId, strcAcctSchemaId, strDateFrom, strOrgFamily, conn);

		      String strReportName = "@basedesign@/com/atrums/contabilidad/ad_reports/Rpt_Libro_Mayor_XLS.jrxml";

		      HashMap<String, Object> parameters = new HashMap<String, Object>();

		      String strLanguage = vars.getLanguage();

		      parameters.put("ShowGrouping", new Boolean(!strGroupBy.equals("")));
		      StringBuilder strSubTitle = new StringBuilder();
		      strSubTitle.append(Utility.messageBD(conn, "DateFrom", strLanguage) + ": " + strDateFrom
		          + " - " + Utility.messageBD(conn, "DateTo", strLanguage) + ": " + strDateTo + " (");
		      strSubTitle.append(COReportGeneralLedgerData.selectCompany(conn, vars.getClient()) + " - ");
		      strSubTitle.append(COReportGeneralLedgerData.selectOrganization(conn, strOrg) + ")");
		      parameters.put("REPORT_SUBTITLE", strSubTitle.toString());
		      parameters.put("Previous", Utility.messageBD(conn, "Initial Balance", strLanguage));
		      parameters.put("Total", Utility.messageBD(conn, "Total", strLanguage));
		      parameters.put("PageNo", strPageNo);
		      String strDateFormat;
		      strDateFormat = vars.getJavaDateFormat();
		      parameters.put("strDateFormat", strDateFormat);
		      String localStrOutputType = "XLSX";

		      renderJR(conn, vars, strReportName, null, "xlsx", parameters, dataWithTotals, null, reporte, email, "'" +vars.getClient() +"'", strOrgFamily, strCuenta, strCuentaHasta, strDateFrom, strDateTo, strContenido, localStrOutputType, coOpcionDescargaReporteId);
		    } finally {
		      if (data != null) {
		        data.close();
		      }
		    }
		  }

	  
	  
	  private void printPageDataPDF(HttpServletRequest request, HttpServletResponse response,
		      VariablesSecureApp vars, String strDateFrom, String strDateTo, String strAmtFrom,
		      String strAmtTo, String strcelementvaluefrom, String strcelementvalueto, String strOrg,
		      String strcBpartnerId, String strmProductId, String strcProjectId, String strGroupBy,
		      String strcAcctSchemaId, String strPageNo, String strShowOpenBalances,
		      ConnectionProvider conn,
		      String reporte, String email, String empresa, String sucursal, String strCuenta, String strCuentaHasta,
		      String strContenido, String coOpcionDescargaReporteId) throws IOException,
		      ServletException {
		    String localStrcelementvalueto = strcelementvalueto;
		    log4j.debug("Output: PDF");
		    //response.setContentType("text/html; charset=UTF-8");
		    String strTreeOrg = TreeData.getTreeOrg(conn, vars.getClient());
		    String strOrgFamily = getFamily(conn, strTreeOrg, strOrg);
		    String toDatePlusOne = DateTimeData.nDaysAfter(conn, strDateTo, "1");
		    String strDateFormat;

		    String strGroupByText = (strGroupBy.equals("BPartner") ? Utility.messageBD(conn, "BusPartner",
		        vars.getLanguage()) : (strGroupBy.equals("Product") ? Utility.messageBD(conn, "Product",
		        vars.getLanguage()) : (strGroupBy.equals("Project") ? Utility.messageBD(conn, "Project",
		        vars.getLanguage()) : "")));
		    String strAllaccounts = "Y";

		    if (strcelementvaluefrom != null && !strcelementvaluefrom.equals("")) {
		      if (localStrcelementvalueto.equals(""))
		        localStrcelementvalueto = strcelementvaluefrom;
		      strAllaccounts = "N";
		    }

		    COReportGeneralLedgerData data = null;
		    try {
		    	data = COReportGeneralLedgerData.select2(conn, "0", strGroupByText, strGroupBy, strAllaccounts,
			          strcelementvaluefrom, localStrcelementvalueto,
			          strOrgFamily,
			          "'" + vars.getClient() + "'",
			          "Y".equals(strShowOpenBalances) ? strDateTo : null, strcAcctSchemaId, strDateFrom,
			          toDatePlusOne, strOrgFamily, strcBpartnerId, strmProductId, strcProjectId, strAmtFrom,
			          strAmtTo, null, null, null, null, null, null);	      

		      // augment data with totals
		      AddTotals dataWithTotals = new AddTotals(data, strGroupBy, strcBpartnerId, strmProductId,
		          strcProjectId, strcAcctSchemaId, strDateFrom, strOrgFamily, conn);

		      String strReportName = "@basedesign@/com/atrums/contabilidad/ad_reports/Rpt_Libro_Mayor_PDF.jrxml";

		      HashMap<String, Object> parameters = new HashMap<String, Object>();

		      String strLanguage = vars.getLanguage();

		      parameters.put("ShowGrouping", new Boolean(!strGroupBy.equals("")));
		      StringBuilder strSubTitle = new StringBuilder();
		      strSubTitle.append(Utility.messageBD(conn, "DateFrom", strLanguage) + ": " + strDateFrom
		          + " - " + Utility.messageBD(conn, "DateTo", strLanguage) + ": " + strDateTo + " (");
		      strSubTitle.append(COReportGeneralLedgerData.selectCompany(conn, vars.getClient()) + " - ");
		      strSubTitle.append(COReportGeneralLedgerData.selectOrganization(conn, strOrg) + ")");
		      parameters.put("REPORT_SUBTITLE", strSubTitle.toString());
		      parameters.put("Previous", Utility.messageBD(conn, "Initial Balance", strLanguage));
		      parameters.put("Total", Utility.messageBD(conn, "Total", strLanguage));
		      parameters.put("PageNo", strPageNo);
		      
		      strDateFormat = vars.getJavaDateFormat();
		      parameters.put("strDateFormat", strDateFormat);
		      String localStrOutputType = "PDF";

		      renderJR(conn, vars, strReportName, null, "pdf", parameters, dataWithTotals, null, reporte, email, "'" +vars.getClient() +"'", strOrgFamily, strCuenta, strCuentaHasta, strDateFrom, strDateTo, strContenido, localStrOutputType, coOpcionDescargaReporteId);
		    } finally {
		      if (data != null) {
		        data.close();
		      }
		    }
		  }
	  
	  private String getFamily(ConnectionProvider conn, String strTree, String strChild) throws IOException, ServletException {
		    return Tree.getMembers(conn, strTree, strChild);
		  }
	  
	  private static class AddTotals extends AbstractScrollableFieldProviderFilter {
		    public AddTotals(ScrollableFieldProvider input, String strGroupBy, String strcBpartnerId,
		        String strmProductId, String strcProjectId, String strcAcctSchemaId, String strDateFrom,
		        String strOrgFamily, ConnectionProvider conn) {
		      super(input);
		      this.strGroupBy = strGroupBy;
		      this.strcBpartnerId = strcBpartnerId;
		      this.strmProductId = strmProductId;
		      this.strcProjectId = strcProjectId;
		      this.strcAcctSchemaId = strcAcctSchemaId;
		      this.strDateFrom = strDateFrom;
		      this.strOrgFamily = strOrgFamily;
		      this.conn = conn;
		    }
		    
		    
		    String strGroupBy;
		    String strcBpartnerId;
		    String strmProductId;
		    String strcProjectId;
		    String strcAcctSchemaId;
		    String strDateFrom;
		    String strOrgFamily;
		    ConnectionProvider conn;
		    String strOld = "";
		    BigDecimal totalDebit = BigDecimal.ZERO;
		    BigDecimal totalCredit = BigDecimal.ZERO;
		    BigDecimal subTotal = BigDecimal.ZERO;
		    COReportGeneralLedgerData subreport[] = new COReportGeneralLedgerData[1];
		    
		    @Override
		    public FieldProvider get() throws ServletException {

		      FieldProvider data = input.get();

		      COReportGeneralLedgerData cur = (COReportGeneralLedgerData) data;

		      // adjust data as needed
		      if (!strOld.equals(cur.groupbyid + cur.id)) {
		        if ("".equals(cur.groupbyid)) {
		          // The argument " " is used to simulate one value and put the optional parameter--> AND
		          // FACT_ACCT.C_PROJECT_ID IS NULL for example
		          subreport = COReportGeneralLedgerData.selectTotal2(conn, strcBpartnerId,
		              (strGroupBy.equals("BPartner") ? " " : null), strmProductId,
		              (strGroupBy.equals("Product") ? " " : null), strcProjectId,
		              (strGroupBy.equals("Project") ? " " : null), strcAcctSchemaId, cur.id, "",
		              strDateFrom, strOrgFamily);
		        } else {
		          subreport = COReportGeneralLedgerData.selectTotal2(conn,
		              (strGroupBy.equals("BPartner") ? "('" + cur.groupbyid + "')" : strcBpartnerId), null,
		              (strGroupBy.equals("Product") ? "('" + cur.groupbyid + "')" : strmProductId), null,
		              (strGroupBy.equals("Project") ? "('" + cur.groupbyid + "')" : strcProjectId), null,
		              strcAcctSchemaId, cur.id, "", strDateFrom, strOrgFamily);
		        }
		        totalDebit = BigDecimal.ZERO;
		        totalCredit = BigDecimal.ZERO;
		        subTotal = BigDecimal.ZERO;
		      }
		      totalDebit = totalDebit.add(new BigDecimal(cur.amtacctdr));
		      cur.totalacctdr = new BigDecimal(subreport[0].totalacctdr).add(totalDebit).toString();
		      totalCredit = totalCredit.add(new BigDecimal(cur.amtacctcr));
		      cur.totalacctcr = new BigDecimal(subreport[0].totalacctcr).add(totalCredit).toString();
		      subTotal = subTotal.add(new BigDecimal(cur.total));
		      cur.totalacctsub = new BigDecimal(subreport[0].total).add(subTotal).toString();
		      cur.previousdebit = subreport[0].totalacctdr;
		      cur.previouscredit = subreport[0].totalacctcr;
		      cur.previoustotal = subreport[0].total;
		      strOld = cur.groupbyid + cur.id;

		      return data;
		    }


	  } 
	 
	  /**
	   * Render a jrxml based report using a {@link ScrollableFieldProvider} as its datasource.
	   * 
	   * @see #renderJR(VariablesSecureApp, HttpServletResponse, String, String, String, HashMap,
	   *      JRDataSource, Map, boolean)
	   */
	  public void renderJR(ConnectionProvider conn, VariablesSecureApp variables,
	      String strReportName, String strFileName, String strOutputType,
	      HashMap<String, Object> designParameters, ScrollableFieldProvider sfp,
	      Map<Object, Object> exportParameters, String reporte, String email, String empresa, String sucursal, String strCuenta, String strCuentaHasta,
	      String strDateFrom, String strDateTo, String strContenido, String localStrOutputType,
	      String coOpcionDescargaReporteId) throws ServletException {
	    renderJR(conn, variables, strReportName, strFileName, strOutputType, designParameters,
	        new JRScrollableFieldProviderDataSource(sfp, variables.getJavaDateFormat()),
	        exportParameters, false, reporte, email, empresa, sucursal, strCuenta, strCuentaHasta,
		      strDateFrom, strDateTo, strContenido, localStrOutputType, coOpcionDescargaReporteId);
	  }
	  

	  public void renderJR(ConnectionProvider conn, VariablesSecureApp variables,
	      String strReportName, String strFileName, String strOutputType,
	      HashMap<String, Object> designParameters, JRDataSource data,
	      Map<Object, Object> exportParameters, boolean forceRefresh,
	      String reporte, String email, String empresa, String sucursal, String strCuenta, String strCuentaHasta,
	      String strDateFrom, String strDateTo, String strContenido, String localStrOutputType,
	      String coOpcionDescargaReporteId) throws ServletException {
	   
	    String localStrReportName = strReportName;
	    localStrOutputType = strOutputType; 
	    String localStrFileName = strFileName;
	    Map<Object, Object> localExportParameters = exportParameters;
	    HashMap<String, Object> localDesignParameters = designParameters;

        /**************************  DIRECTORIO AMBIENTE LOCAL ****************************/
	    //final String strAttach = "C:/_Desarrollo/AplicacionesFuentes/dontablero/openbravodontablero/attachments/";
	    
	    /**************************  DIRECTORIO PRODUCCION ********************************/
	    final String strAttach = "/opt/OpenbravoTablero/attachments/"; //YRO CAMBIAR DIRECTORIO

	    final String strLanguage = variables.getLanguage();
	    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

	    /**************************  DIRECTORIO AMBIENTE LOCAL ****************************/
	    //final String strBaseDesign = "C:/_Desarrollo/AplicacionesFuentes/dontablero/openbravodontablero/modules/com.atrums.contabilidad/src/"; //getBaseDesignPath(strLanguage);
	    
	    /**************************  DIRECTORIO PRODUCCION ********************************/
	    final String strBaseDesign = "/usr/local/tomcat6/webapps/openbravotablero/src-loc/design/"; //YRO CAMBIAR DIRECTORIO
	    
	    localStrReportName = Replace.replace(
	        Replace.replace(localStrReportName, "@basedesign@", strBaseDesign), "@attach@", strAttach);
	    if (localStrFileName == null) {
	      localStrFileName = localStrReportName.substring(localStrReportName.lastIndexOf("/") + 1);
	    }

	    String reportId = null;
	    
	    try {
	      if (localDesignParameters == null)
	        localDesignParameters = new HashMap<String, Object>();

	      localDesignParameters.put("BASE_WEB", "http://localhost:8080/openbravotablero/web");
	      localDesignParameters.put("BASE_DESIGN", strBaseDesign);
	      localDesignParameters.put("ATTACH", strAttach);
	      localDesignParameters.put("USER_CLIENT",empresa);
	      localDesignParameters.put("USER_ORG", sucursal);
	      localDesignParameters.put("LANGUAGE", strLanguage);
	      localDesignParameters.put("LOCALE", locLocale);
	      localDesignParameters.put("REPORT_TITLE","LIBRO MAYOR");
	         PrintJRData.getReportTitle(conn, variables.getLanguage(), "Rpt_Libro_Mayor");

	      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
          final DecimalFormat numberFormat = new DecimalFormat(
	          variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
	      localDesignParameters.put("NUMBERFORMAT", numberFormat);

	      if (localExportParameters == null)
	        localExportParameters = new HashMap<Object, Object>();
	      if (localStrOutputType == null || localStrOutputType.equals(""))
	        localStrOutputType = "html";
	      final ExportType expType = ExportType.getExportType(localStrOutputType.toUpperCase());
	      
	      
	      /********************** 12-04-2024 ****************************/
	      Date objDate = new Date();
	      String strDateFormat = "dd-MM-yyyy-HH-mm"; 
	      SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); 
	      reportId = objSDF.format(objDate);
	      /*************************************************************/

	        
	        ZipOutputStream os;
	             
	        File outputFile = new File(strAttach + "/LibroMayor/" + localStrFileName + "-"
		            + reportId + "." + localStrOutputType);
	        ReportingUtils.exportJR(localStrReportName, expType, localDesignParameters, outputFile,
	            false, conn, data, localExportParameters);
	        
	        FileUtility f = new FileUtility(strAttach + "/LibroMayor/", localStrFileName + "-"
	                + (reportId) + "." + localStrOutputType, false, true);

	        if (f.exists())
	            {
	            	/**********************************************************************/
	            	Organization currentOrg = OBContext.getOBContext().getCurrentOrganization();
	    			String type = "text/html; charset=utf-8";
	    			
	    			/********* Envio archivo formato Excel ************/
	    			List<File> lisdoc = new ArrayList<File>();
	    			lisdoc.add(outputFile);			
	            	//enviarCorreo(email, normalizacionPalabras("DON TABLERO - " + reporte), normalizacionPalabras(strContenido), type, lisdoc, false);
	            	/**************************************************/
	            	

	    			/************ Opción para comprimir ***********/
	    			String nombreArchivo = null;
	    			nombreArchivo = localStrFileName + "-"+ (reportId) + ".zip";
	    			
	    			
	    			
	    			String carpeta = SubCadenas(coOpcionDescargaReporteId);
	    			crearDirectorio(strAttach, carpeta);
	    			
	    			os = new ZipOutputStream(new FileOutputStream(strAttach + carpeta + "/" + nombreArchivo));
	    		
	    			ZipEntry entrada = new ZipEntry(outputFile.getName());
	    			os.putNextEntry(entrada);

	    			FileInputStream fis = new FileInputStream(strAttach + "/LibroMayor/" + localStrFileName + "-"
	    		            + (reportId) + "." + localStrOutputType);
	    			    			
	    			
	    			byte [] buffer = new byte[10240];
	    			int leido=0;
	    			while (0 < (leido=fis.read(buffer))){
	    			   os.write(buffer,0,leido);
	    			}
	    			
	    			fis.close();
	    			os.closeEntry();
	    			os.close();
	    			
	    			//List<ZipOutputStream> lisdocZip = new ArrayList<ZipOutputStream>();
	    			//lisdocZip.add(os);  //Validar Envío 10-04-2024
	            	enviarCorreoZip(email, normalizacionPalabras("DON TABLERO - " + reporte), normalizacionPalabras(strContenido), type, os, false, coOpcionDescargaReporteId, null);

	            	CODatosLibroMayorData.insertarLibroMayor(conn, nombreArchivo, coOpcionDescargaReporteId, carpeta);	
	            	/**********************************************************************/
	              f.deleteFile();
	              outputFile.delete();
	            }

	    } catch (IOException ioe) {
	    	try {
	            FileUtility f = new FileUtility("/opt/openbravotablero/attachments/", localStrFileName + "-"
	                + (reportId) + "." + localStrOutputType, false, true);
	            if (f.exists())
	              f.deleteFile();
	          } catch (IOException ioex) {
	            log4j.error("Error trying to delete temporary report file " + localStrFileName + "-"
	                + (reportId) + "." + localStrOutputType + " : " + ioex.getMessage());
	          }
	    } catch (final Exception e) {
     throw new ServletException(e.getMessage(), e);
	    } finally {
	      try {
	        //os.close();
	      } catch (final Exception e) {
	      }
	    }
	  }
	  
	  
	  public String SubCadenas(String cadena) {
		  
		        String subcadena = "D5ACD79BE6D84F79B8E9E1C2339A3171";
		        // Iterar sobre la cadena y obtener subcadenas de 3 caracteres

		        for (int i = 0; i <= cadena.length() ; i = i + 3) {
		        	if (i < 30) {
		        		subcadena = subcadena + "/" + cadena.substring(i, i + 3);
		        	}else {
		        		subcadena = subcadena + "/" + cadena.substring(i, i + 2);
		        	} 
		        }
		        System.out.println(subcadena);
		        return subcadena;
		    }
	  

	  public void crearDirectorio(String attachments, String repositorio) {
	          // Especifica la ruta del directorio que quieres crear
	          String rutaDirectorio = attachments + "/" + repositorio;

	          // Crea un objeto File con la ruta del directorio
	          File directorio = new File(rutaDirectorio);

	          // Verifica si el directorio no existe y lo crea
	          if (!directorio.exists()) {
	              boolean creado = directorio.mkdirs();
	              if (creado) {
	                  System.out.println("Directorio creado correctamente.");
	              } else {
	                  System.out.println("No se pudo crear el directorio.");
	              }
	          } else {
	              System.out.println("El directorio ya existe.");
	          }
	  }
	  
	  
	  public void CopiarArchivo(String Origen, String Destino) {
	
	          // Especifica la ruta del archivo que deseas copiar
	          String rutaArchivoOrigen = Origen ;
	          
	          // Especifica la ruta del directorio de destino
	          String rutaDirectorioDestino = Destino;

	          try {
	              // Convierte las rutas a objetos Path
	              Path archivoOrigen = Paths.get(rutaArchivoOrigen);
	              Path directorioDestino = Paths.get(rutaDirectorioDestino);
	              
	              // Copia el archivo al directorio de destino
	              Files.copy(archivoOrigen, directorioDestino.resolve(archivoOrigen.getFileName()), StandardCopyOption.REPLACE_EXISTING);
	              
	              System.out.println("Archivo copiado correctamente.");
	          } catch (IOException e) {
	              System.out.println("Error al copiar el archivo: " + e.getMessage());
	          }
	      
	  }



}


