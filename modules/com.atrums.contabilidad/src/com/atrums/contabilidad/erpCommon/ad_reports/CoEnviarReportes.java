package com.atrums.contabilidad.erpCommon.ad_reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.utils.FormatUtilities;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * @author ATRUMS-IT
 *
 */
public class CoEnviarReportes implements Process {
	static Logger log4j = Logger.getLogger(CoEnviarReportes.class);
	final OBError msg = new OBError();

	@Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    CODatosReporteData[] coDatosReporte;
    CODatosReporteData[] accionActual;

    String coOpcionDescargaReporteId = null;
    String fechainicio = null;
    String fechafin = null;
    String mensaje = null;
    String ejecuta = null;
    String email = null;
    String reporte = null;
    String direccionReporte = null;
    String empresa = null;
    boolean resultado = false;

    try {
    	//Verifica si existe algún reporte en ejecución
    	
    	accionActual = CODatosReporteData.validarReportesEnEjecucion(conn, varsAux.getClient());
    	
    	if(accionActual.length == 0) {
    		ejecuta = "Ejecutar";
    	}else {
    		ejecuta = "En ejecucion...";
    	}
    	
    	System.out.println("ejecuta: " + ejecuta);
    	
    	if(ejecuta.equals("Ejecutar"))
		{
		      coDatosReporte = CODatosReporteData.selecionarInfoReporte(conn,
		          varsAux.getClient());
		
		      if (coDatosReporte.length > 0) {
		
		    	  coOpcionDescargaReporteId = coDatosReporte[0].coOpcionDescargaReporteId;
		    	  
		    	  CODatosReporteData.actualizarReporteEnEjecucion(conn, coOpcionDescargaReporteId);
		    	  
		    	  
		    	  fechainicio = coDatosReporte[0].fechainicio;
		    	  fechafin = coDatosReporte[0].fechafin;
		    	  email = coDatosReporte[0].email;
		    	  empresa = varsAux.getClient();
		    	  reporte = coDatosReporte[0].nombrereporte;
		    	  direccionReporte = coDatosReporte[0].direccionReporte;
		
		    	  resultado = enviarEmailReporte(conn, fechainicio, fechafin, reporte, direccionReporte, email, empresa);
		    	  
		    	  if (resultado == true) {
		    		  CODatosReporteData.actualizarReporteEnviado(conn, coOpcionDescargaReporteId);
		    	  }else {
		    		  CODatosReporteData.actualizarReporteError(conn, coOpcionDescargaReporteId);
		    	  }
		    	  
		      }
			}
		    } catch (Exception ex) {
		      Log.error(ex.getMessage(), ex);
		      if (resultado == false) {
		    	  CODatosReporteData.actualizarReporteError(conn, coOpcionDescargaReporteId);
		      }
		    } finally {
		      // TODO: handle finally clause
		    	coDatosReporte = null;
		        OBDal.getInstance().rollbackAndClose();
		    }  
  }

	public boolean enviarEmailReporte(ConnectionProvider connectionProvider, String fechainicio, String fechafin,
			String reporte, String direccionreporte, String email, String adClientId) throws Exception {

		File flXls = null;
		String strContenido = "";
		String type = null;
		

		type = "text/html; charset=utf-8";

		strContenido = "Estimado Usuario. Reciba un cordial saludo. \n" 
		        + "En el presente correo, se envía adjunto el reporte correspondiente a " + reporte + "\n\n"
				+ "El reporte se encuentra generado en el rango de fechas: " + fechainicio + " a " + fechafin + ".\n\n"
				+ "Saludos Cordiales,\n"
				+ "ATRUMS IT";
		
		flXls = generarXlS(connectionProvider, "@basedesign@" + direccionreporte, reporte, fechainicio,
				fechafin, email, adClientId, adClientId);
		List<File> lisdoc = new ArrayList<File>();
		lisdoc.add(flXls);

		System.out.println(lisdoc);

		if (enviarCorreo(email, "DON TABLERO - Reporte " + reporte, strContenido, type, lisdoc, false)) {
			flXls.delete();
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("deprecation")
	public static File generarXlS(ConnectionProvider conn, String strBaseDesin, String strNombre, String fechainicio,
			String fechafin, String email, String empresa, String adClientId) throws ServletException {

		File flTemp = null;
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");

		try {

			String strBaseDesign = strBaseDesin;

			if (!basedesign(strBaseDesign).equals("")) {

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("fechadesde", formato.parse(fechainicio));
				parameters.put("fechahasta", formato.parse(fechafin));
				// parameters.put("AdClientId", adClientId);
				// parameters.put("Empresa", empresa);
				parameters.put("BASE_DESIGN", basedesign(strBaseDesign));
				parameters.put("IS_IGNORE_PAGINATION", true);

				strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

				JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);
				Connection con = conn.getTransactionConnection();
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, con);

				JRXlsxExporter xlsExporter = new JRXlsxExporter();

				String strNombreArch = strNombre;
				OutputStream out = null;

				flTemp = File.createTempFile(strNombreArch + "_", ".xlsx", null);
				out = new FileOutputStream(flTemp);
				ByteArrayOutputStream byteout = new ByteArrayOutputStream();

				try {
					SimpleExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
					SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(out);
					xlsExporter.setExporterInput(exporterInput);
					xlsExporter.setExporterOutput(exporterOutput);
					xlsExporter.exportReport();

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				out.write(byteout.toByteArray());
				out.flush();
				out.close();
			}

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoConnectionAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return flTemp;
	}

	/**
	 * @param strReporte
	 * @return
	 */
	public static String basedesign(String strReporte) {

		String strDirectorio = CoEnviarReportes.class.getResource("/").getPath();

		if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
			int intPos = strDirectorio.indexOf("/");

			if (intPos != -1) {
				strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
			}
		}

		String strDirectorioInv = "";

		for (int i = 0; i < strDirectorio.length(); i++) {
			strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
		}

		int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

		if (intPosicion != -1) {
			strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
		} else {
			intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
			if (intPosicion != -1) {
				strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
			} else {
				return "";
			}
		}

		strDirectorio = "";

		for (int i = 0; i < strDirectorioInv.length(); i++) {
			strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
		}

		String strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "WebContent/src-loc/design"));

		File flRep = new File(strBaseDesign);

		if (flRep.exists()) {
			return strDirectorio + "WebContent/src-loc/design";
		} else {
			strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

			flRep = new File(strBaseDesign);

			if (flRep.exists()) {
				return strDirectorio + "src-loc/design";
			} else {
				return "";
			}
		}
	}

	/**
	 * @param recipient
	 * @param subject
	 * @param body
	 * @param type
	 * @param attachments
	 * @return
	 */
	public static boolean enviarCorreo(final String recipient, final String subject, final String body,
			final String type, final List<File> attachments, boolean soporte) {
		try {
			Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
			final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

			if (mailConfig == null) {
				return false;
			}

			final String username = mailConfig.getSmtpServerAccount();
			final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
			final String connSecurity = mailConfig.getSmtpConnectionSecurity();
			final int port = mailConfig.getSmtpPort().intValue();
			final String senderAddress = mailConfig.getSmtpServerSenderAddress();
			final String host = mailConfig.getSmtpServer();
			final boolean auth = mailConfig.isSMTPAuthentification();

			String auxRecipient = recipient;

			if (soporte) {
				auxRecipient = username; // username;
			}

			EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress, auxRecipient,
					null, null, null, subject, body, type, attachments, null, null);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			log4j.error(e.getMessage());
			return false;
		}
	}

}
