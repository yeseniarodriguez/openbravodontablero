<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_AnalisisDimensionalFacVentas" pageWidth="910" pageHeight="421" whenNoDataType="AllSectionsNoDetail" columnWidth="910" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="a7f0cfce-36b9-4102-a551-d859b70e9277">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="633"/>
	<property name="ireport.y" value="0"/>
	<property name="net.sf.jasperreports.export.xls.detect.cell.type" value="true"/>
	<parameter name="fechadesde" class="java.util.Date"/>
	<parameter name="fechahasta" class="java.util.Date"/>
	<parameter name="USER_CLIENT" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="NUMBERFORMAT" class="java.text.DecimalFormat" isForPrompting="false">
		<defaultValueExpression><![CDATA[new DecimalFormat("#,##0.00",new DecimalFormatSymbols(Locale.US))]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_DESIGN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/var/lib/tomcat/webapps/openbravotablero/src-loc/design/"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select surcursal, totalventas, costo, contribucion,
       facturas, nc,
       cantidad, IPF, PVF, (efectivo+otros) as efectivo, cheque, tarjeta,
       descrip, GrantotalVenta, fhasta, ((efectivo+otros)/totalventas) as var_efec,
       (cheque/totalventas) as var_che, (tarjeta/totalventas) as var_tar
from(
select surcursal, round(sum(total_fact),4) as totalventas,
       sum(costo_fact)*-1 as costo, sum(contribucion_fact) as contribucion,
       round(sum(cantidad_fact),4) as facturas, round(sum(cantidad_nc),4) as nc,
       sum(cantidad_items) as cantidad,
       (case when (sum(cantidad_fact)-sum(cantidad_nc)) <> 0 then
              round(sum(cantidad_items)/(sum(cantidad_fact)-sum(cantidad_nc)),4)
               else
               0
               end)as IPF,
               (case when (sum(cantidad_fact)-sum(cantidad_nc)) <> 0 then
              round(sum(total_fact)/(sum(cantidad_fact)-sum(cantidad_nc)),4)
              else
              0
                      end)
              as PVF,
       sum(efectivo) as efectivo, sum(cheque) as cheque, sum(tarjeta) as tarjeta, sum(otros) as otros,
       descrip,
       (select sum(case when dt.docbasetype = 'ARI' then c.totallines else  (c.totallines)*-1 end)
          from c_invoice c
         inner join c_doctype dt on (c.c_doctype_id=dt.c_doctype_id)
          where c.issotrx = 'Y' and c.docstatus = 'CO' and date(c.dateinvoiced) >= date($P{fechadesde})
            and c.dateinvoiced <=  (CASE WHEN date($P{fechahasta})=date(now())
          then (select date_trunc('minute', now() - interval '1 minute'))
          else (select TO_TIMESTAMP(TO_CHAR(TO_DATE(DATE($P{fechahasta}),'DD-MM-YYYY'),'MM-DD-YYYY')||' 23:59:59','MM-DD-YYYY HH24:MI:SS'))::timestamp
          end)) as GrantotalVenta,
 (CASE WHEN date($P{fechahasta})=date(now())
             THEN (select date_trunc('minute', now() - interval '1 minute'))
             ELSE (select (TO_TIMESTAMP(TO_CHAR(TO_DATE(DATE($P{fechahasta}),'DD-MM-YYYY'),'MM-DD-YYYY')||' 23:59:59','MM-DD-YYYY HH24:MI:SS'))::timestamp with time zone)
             END) as  fhasta

from(
     select surcursal, sum(total_fact) as total_fact,
			round(sum (num_fact),4) as cantidad_fact, 0 as cantidad_nc, round(sum(cantidad),4) as cantidad_items,
            sum(costo)*-1 as costo_fact, sum(total_fact)-sum(costo) as contribucion_fact, ad_org_id,descrip,
            coalesce(sum(efectivo),0) as efectivo, coalesce(sum(cheque),0) as cheque,
            coalesce(sum(tarjeta),0) as tarjeta, coalesce(sum(otros),0) as otros
     from (
          select o.name as surcursal, c.documentno as documento, c.totallines as  total_fact,
                 1 as num_fact, round(sum(il.qtyinvoiced),4) as cantidad ,
                 sum(M_GET_PRODUCT_COST(il.m_product_id,c.dateinvoiced, null)*il.qtyinvoiced) as costo,
                 o.ad_org_id, substring(o.description from 1 for 2) as descrip,
                 (case when ((c.fin_paymentmethod_id = 'EC100' or c.fin_paymentmethod_id = 'EC103')) then c.totallines end) as efectivo,
                 (case when (c.fin_paymentmethod_id = 'EC102') then c.totallines end) as cheque,
                 (case when (c.fin_paymentmethod_id = 'EC105') then c.totallines end) as tarjeta,
                 (case when (c.fin_paymentmethod_id not in ('EC100','EC103','EC102','EC105')) then c.totallines end) as otros
            from c_invoice c
           inner join ad_org o on(c.ad_org_id=o.ad_org_id)
           inner join c_doctype dt on (c.c_doctype_id=dt.c_doctype_id and dt.docbasetype ='ARI')
           inner join c_invoiceline il on (il.c_invoice_id=c.c_invoice_id)
           where c.issotrx = 'Y'
             and c.docstatus = 'CO'
             and c.PROCESSED = 'Y'
             and date(c.dateinvoiced) >= date($P{fechadesde})
             and c.dateinvoiced <=  (CASE WHEN date($P{fechahasta})=date(now()) THEN
                                          (select date_trunc('minute', now() - interval '1 minute'))
                                     ELSE
                                          (select TO_TIMESTAMP(TO_CHAR(TO_DATE(DATE($P{fechahasta}),'DD-MM-YYYY'),'MM-DD-YYYY')||' 23:59:59','MM-DD-YYYY HH24:MI:SS'))::timestamp
                                     END)
          group by 1,2,3,7,8, c.fin_paymentmethod_id
          )as facturas
     group by 1,8,9
    union all
    select  surcursal, sum(total_fact)*-1 as total_nc, 0 as cantidad_fact, round(sum(num_nc),4) as cantidad_nc, round(sum(cantidad_items)*-1,4) as cantidad_nc,
            sum(costo) as costo_nc, (sum (total_fact)-sum(costo))*-1 as contribucion_nc, ad_org_id, descrip,
            sum(efectivo)*(-1) as efectivo, sum(cheque)*(-1) as cheque, sum(tarjeta)*(-1) as tarjeta, sum(otros)*(-1) as otros
    from
        (
         select o.name as surcursal, c.documentno as documento, c.totallines as  total_fact,
                1 as num_nc, round(sum(il.qtyinvoiced),4) as cantidad_items,
                sum(M_GET_PRODUCT_COST(il.m_product_id,c.dateinvoiced, null)*il.qtyinvoiced) as costo,
                o.ad_org_id, substring(o.description from 1 for 2) as descrip,
               (case when ((c.fin_paymentmethod_id = 'EC100' or c.fin_paymentmethod_id = 'EC103')) then c.totallines end) as efectivo,
               (case when (c.fin_paymentmethod_id = 'EC102') then c.totallines end) as cheque,
               (case when (c.fin_paymentmethod_id = 'EC105') then c.totallines end) as tarjeta,
               (case when (c.fin_paymentmethod_id not in ('EC100','EC103','EC102','EC105')) then c.totallines end) as otros
          from c_invoice c
         inner join ad_org o on(c.ad_org_id=o.ad_org_id)
         inner join c_doctype dt on (c.c_doctype_id=dt.c_doctype_id and dt.docbasetype ='ARC')
         inner join c_invoiceline il on (il.c_invoice_id=c.c_invoice_id)
         where c.issotrx = 'Y'
           and c.docstatus = 'CO'
           and c.PROCESSED = 'Y'
           and date(c.dateinvoiced) >= date($P{fechadesde})
           and c.dateinvoiced <=  (CASE WHEN date($P{fechahasta})=date(now()) THEN
                                        (select date_trunc('minute', now() - interval '1 minute'))
                                   ELSE
                                        (select TO_TIMESTAMP(TO_CHAR(TO_DATE(DATE($P{fechahasta}),'DD-MM-YYYY'),'MM-DD-YYYY')||' 23:59:59','MM-DD-YYYY HH24:MI:SS'))::timestamp
                                   END)
         group by 1,2,3,7,c.fin_paymentmethod_id
     )as notasCredito
     group by 1,8,9
 ) as facturasventas
 group by 1,14
 order by to_number(substring(descrip, 1 , 3)) asc
 ) as totales]]>
	</queryString>
	<field name="surcursal" class="java.lang.String"/>
	<field name="totalventas" class="java.math.BigDecimal"/>
	<field name="costo" class="java.math.BigDecimal"/>
	<field name="contribucion" class="java.math.BigDecimal"/>
	<field name="facturas" class="java.math.BigDecimal"/>
	<field name="nc" class="java.math.BigDecimal"/>
	<field name="cantidad" class="java.math.BigDecimal"/>
	<field name="ipf" class="java.math.BigDecimal"/>
	<field name="pvf" class="java.math.BigDecimal"/>
	<field name="efectivo" class="java.math.BigDecimal"/>
	<field name="cheque" class="java.math.BigDecimal"/>
	<field name="tarjeta" class="java.math.BigDecimal"/>
	<field name="descrip" class="java.lang.String"/>
	<field name="grantotalventa" class="java.math.BigDecimal"/>
	<field name="fhasta" class="java.sql.Timestamp"/>
	<field name="var_efec" class="java.math.BigDecimal"/>
	<field name="var_che" class="java.math.BigDecimal"/>
	<field name="var_tar" class="java.math.BigDecimal"/>
	<variable name="cantidad_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{cantidad}]]></variableExpression>
	</variable>
	<variable name="totalventas_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{totalventas}]]></variableExpression>
	</variable>
	<variable name="costo_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{costo}]]></variableExpression>
	</variable>
	<variable name="grantotalventas" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{totalventas}.divide(new BigDecimal("1"),4)]]></variableExpression>
	</variable>
	<variable name="contribucion_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{contribucion}]]></variableExpression>
	</variable>
	<variable name="totalPorcVentas" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{totalventas}.divide($V{grantotalventas},4).multiply(new BigDecimal ("100"))]]></variableExpression>
	</variable>
	<variable name="totalEfectivo" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{efectivo}.divide(new BigDecimal("1"),4)]]></variableExpression>
	</variable>
	<variable name="totalCheque" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{cheque}]]></variableExpression>
	</variable>
	<variable name="totalTarjeta" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{tarjeta}]]></variableExpression>
	</variable>
	<variable name="totalPVentas" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$V{Porcentajeventa}]]></variableExpression>
	</variable>
	<variable name="totalNotas" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{nc}]]></variableExpression>
	</variable>
	<variable name="totalFacturas" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{facturas}]]></variableExpression>
	</variable>
	<variable name="totalFact" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{totalFacturas}.subtract( $V{totalNotas} )]]></variableExpression>
	</variable>
	<variable name="Porcentajeventa" class="java.math.BigDecimal" resetType="Column">
		<variableExpression><![CDATA[$F{totalventas}.divide($F{grantotalventa},4)]]></variableExpression>
	</variable>
	<variable name="totalIpf" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{cantidad_1}.divide( $V{totalFacturas}.subtract( $V{totalNotas}), 4 )]]></variableExpression>
	</variable>
	<variable name="toatlVpf" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{grantotalventas}.divide(($V{totalFacturas}.subtract( $V{totalNotas})),4)]]></variableExpression>
	</variable>
	<title>
		<band height="32">
			<staticText>
				<reportElement x="0" y="5" width="55" height="12" uuid="d0533b1f-428e-42f0-9aec-650fdc6c9041"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Desde:]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="55" height="12" uuid="4313d25e-ee96-44cc-b4f0-56191d6f3d8a"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Hasta:]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Transparent" x="55" y="5" width="55" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="fe68285c-3d50-4c75-8af2-3d8f69894caf"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{fechadesde}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="55" y="20" width="55" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="b9dbca1a-c574-44e6-9876-634e70903770"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{fechahasta}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="17">
			<staticText>
				<reportElement x="165" y="0" width="55" height="15" uuid="375b3969-a485-4883-a6ca-283132a424e4"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Venta]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="220" y="0" width="40" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="84030ac3-26a2-4b8b-983f-00f875696d50"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[# Items]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="260" y="0" width="50" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="9766b59f-dbdf-4a2c-a4c4-00855707f29e"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[# Facturas]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="310" y="0" width="40" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="152b9595-8711-4886-850b-fdeee41a8254"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[# NC]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="350" y="0" width="50" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="6a4c0998-0440-4a56-a935-940de50057c7"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[IPF]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="400" y="0" width="50" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="1b4f2052-69d2-4485-a364-21f8b02bfbea"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[VPF]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="460" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="e2935b36-e000-47d2-8475-758045a3882f"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Efectivo]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="515" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="a599f4e6-8bd1-4a42-8bd3-0eab73398c7f"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Cheque]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="570" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="40b7efe4-3219-46f3-8c6b-faab4deaba43"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Tarjeta]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="700" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="ada1619b-e7e8-4b88-83a1-2c3586f18d9a"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Cheque %]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="645" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="62b298d4-3c19-4584-b54d-55fd5054ec0f"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Efectivo %]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="755" y="0" width="55" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="6b1a8a99-8c1f-4c04-a0f0-d8d85fa892d2"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Tarjeta %]]></text>
			</staticText>
			<staticText>
				<reportElement x="110" y="0" width="55" height="15" uuid="81b1dc80-b66a-42db-b9b8-a9927602503f"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[% de la Venta]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="110" height="15" uuid="ee817cd7-54ec-4728-8f0b-82868b2cffe9"/>
				<box>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Sucursal]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="12">
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement x="220" y="0" width="40" height="12" uuid="2cbe709e-ae10-47e3-b1d8-404c69575387"/>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cantidad}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="110" height="12" uuid="9a5cac40-c827-4743-9354-1719f1a9e71d"/>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{surcursal}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="165" y="0" width="55" height="12" uuid="f7ac4896-6c7e-46fd-bb23-cdeb65ab560e"/>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{totalventas}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="260" y="0" width="50" height="12" uuid="47758234-56a1-49d9-9c80-988c431d9204"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{facturas}.subtract( $F{nc} )]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="310" y="0" width="40" height="12" uuid="23598071-e5f3-4eb8-9952-3e772fd14a68"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nc}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="350" y="0" width="50" height="12" uuid="1334065e-e9f0-4901-9d90-cd4dc396f1c2"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ipf}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="400" y="0" width="50" height="12" uuid="8b356f7d-ec5b-46ac-a830-694b0bf8a54c"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pvf}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="460" y="0" width="55" height="12" uuid="fdd68988-70e1-4686-9304-8e6c7c33e9aa"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{efectivo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="515" y="0" width="55" height="12" uuid="650612ff-2b72-46d6-a58f-ab6a1def21a5"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cheque}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="570" y="0" width="55" height="12" uuid="9a11b332-8ff7-40df-b6ba-d4bc6f0b5955"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tarjeta}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00 %">
				<reportElement x="645" y="0" width="55" height="12" uuid="4a8a4ef3-5107-419c-ad9d-55a390cd3449"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{var_efec}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00 %">
				<reportElement x="700" y="0" width="55" height="12" uuid="044f1842-4a4c-4063-83be-593e8dd93d7e"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{var_che}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00 %">
				<reportElement x="755" y="0" width="55" height="12" uuid="1758adfd-99c1-461c-ab97-54b4b49324f2"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{var_tar}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %" isBlankWhenNull="false">
				<reportElement x="110" y="0" width="55" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="b7d1bcbf-58f0-4af5-a772-14195ba68093"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Porcentajeventa}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="56">
			<staticText>
				<reportElement x="0" y="1" width="110" height="12" uuid="920ac92c-bd78-4f4d-8423-a8b462d62ac5"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL]]></text>
			</staticText>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="165" y="1" width="55" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="d19cbc16-7ec3-4268-8949-2d8f4de20c19"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalventas_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="460" y="1" width="55" height="12" uuid="c80c6fcb-0c23-4a5f-b830-d192b15b7c48"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalEfectivo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="515" y="1" width="55" height="12" uuid="2fe99a9a-4ba9-459e-bdbf-8a1c927fa529"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalCheque}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="570" y="1" width="55" height="12" uuid="82853d24-fd22-416c-9e28-233410647eba"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalTarjeta}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00 %">
				<reportElement x="645" y="1" width="55" height="12" uuid="5583b128-6278-4c88-825a-721939a39f72"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new Double(String.valueOf($V{totalEfectivo}))/(new Double(String.valueOf($V{totalventas_1})))]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %">
				<reportElement x="700" y="1" width="55" height="12" uuid="8f4c6705-9836-47cc-bdd2-ac01abdb9ca2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new Double(String.valueOf($V{totalCheque}))/(new Double(String.valueOf($V{totalventas_1})))]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %">
				<reportElement x="755" y="1" width="55" height="12" uuid="0b320482-5c1e-4df9-a937-e624c9a7a69c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new Double(String.valueOf($V{totalTarjeta}))/(new Double(String.valueOf($V{totalventas_1})))]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="#,##0.00 %">
				<reportElement x="110" y="1" width="55" height="12" uuid="7e388ac6-5710-4c8d-90ee-21ca1dbe205d"/>
				<textElement textAlignment="Right">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new BigDecimal("1")]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0">
				<reportElement x="310" y="1" width="40" height="12" uuid="8f0da26a-134a-4d09-a68e-f4b1c227f555"/>
				<textElement textAlignment="Right">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalNotas}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="0" width="450" height="1" uuid="d10dda71-a9c4-4d13-84fc-2b564f0bcfa6"/>
			</line>
			<textField pattern="#,##0">
				<reportElement x="260" y="1" width="50" height="12" uuid="cda277e6-fbb7-4769-bd2f-d6f1356f3b74"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalFact}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="220" y="1" width="40" height="12" uuid="5b1fbc61-5be9-47ac-8f87-f7b90ed8afea"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{cantidad_1}]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement positionType="Float" x="0" y="16" width="810" height="39" uuid="e6e40581-dfd8-4848-9830-48558ab29f37"/>
				<subreportParameter name="fechadesde">
					<subreportParameterExpression><![CDATA[$P{fechadesde}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fechahasta">
					<subreportParameterExpression><![CDATA[$F{fhasta}.toString()]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/estandar/formatos/erpReports/dontablero/Rpt_Ventas_Region_locales.jasper"]]></subreportExpression>
			</subreport>
			<line>
				<reportElement x="460" y="0" width="165" height="1" uuid="02b49b63-530f-4fe3-a1cc-5533d4aad67a"/>
			</line>
			<line>
				<reportElement x="645" y="0" width="165" height="1" uuid="89a573d6-51e3-4ee3-9421-ff7bdaba6ad4"/>
			</line>
			<textField pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="400" y="1" width="50" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="8dbcff1b-6125-4e46-a446-edfc4455eb81"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{toatlVpf}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="350" y="1" width="50" height="12" forecolor="#000000" backcolor="#FFFFFF" uuid="7c5f8f1f-194c-419f-9684-6759391760bd"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalIpf}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
