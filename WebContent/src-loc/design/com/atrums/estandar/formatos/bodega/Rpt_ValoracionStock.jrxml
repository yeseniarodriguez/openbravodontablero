<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_ValoracionStock" pageWidth="1085" pageHeight="820" columnWidth="1045" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="5684ce4b-16bb-439d-b1f8-f155430ea7ee">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DATE_TO" class="java.util.Date"/>
	<parameter name="AUX_BODEGA" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{BODEGA_ID} == null || $P{BODEGA_ID}.equals("")) ? "" : " AND w.m_warehouse_id ='" + $P{BODEGA_ID} + "'"]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["http://192.168.0.216/openbravo/web"]]></defaultValueExpression>
	</parameter>
	<parameter name="BODEGA_ID" class="java.lang.String"/>
	<parameter name="USER_ORG" class="java.lang.String"/>
	<queryString>
		<![CDATA[select a.organizationid, a.codigo, a.Nombre, a.categoria, a.Descripcion, a.Costo, a.Stock, a.Bodega, (a.Costo * a.Stock) as subtotal
from (
SELECT t.AD_ORG_ID AS organizationid,
       p.name AS Nombre,
       cat.name as Categoria,
       coalesce(p.description, p.description, '-') AS Descripcion,
       coalesce((select c.cost
                   from m_costing c
                  where p.m_product_id = c.m_product_id
                    and date(c.datefrom) < date($P{DATE_TO})
                    and date(c.dateto) >= date($P{DATE_TO}) limit 1), '0') AS Costo,
       sum(t.movementqty) AS Stock,
       (select name from m_warehouse where m_warehouse_id = $P{BODEGA_ID}) as Bodega,
       p.value as codigo
FROM m_product p, m_transaction t, m_product_category cat
WHERE p.ad_client_id IN ($P!{USER_CLIENT})
  AND t.m_product_id = p.m_product_id
  AND cat.m_product_category_id = p.m_product_category_id
  AND t.m_locator_id IN (select m_locator_id from m_locator where m_warehouse_id = $P{BODEGA_ID})
  AND date(t.movementdate) <= date($P{DATE_TO})
GROUP BY 1, 2, 3, 4, 5, 7,8
ORDER BY 3,2
) a]]>
	</queryString>
	<field name="nombre" class="java.lang.String"/>
	<field name="descripcion" class="java.lang.String"/>
	<field name="costo" class="java.math.BigDecimal"/>
	<field name="stock" class="java.math.BigDecimal"/>
	<field name="bodega" class="java.lang.String"/>
	<field name="organizationid" class="java.lang.String"/>
	<field name="categoria" class="java.lang.String"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="codigo" class="java.lang.String"/>
	<variable name="total" class="java.math.BigDecimal" resetType="Group" resetGroup="categoria" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal}]]></variableExpression>
	</variable>
	<variable name="total_general" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal}]]></variableExpression>
	</variable>
	<group name="Bodega">
		<groupExpression><![CDATA[$F{bodega}]]></groupExpression>
		<groupHeader>
			<band height="58">
				<textField>
					<reportElement style="Detail" x="102" y="0" width="210" height="30" uuid="e668c1c9-d2f2-49d7-aa9a-a07ea14d1013"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{bodega}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement style="Column header" x="0" y="0" width="102" height="30" uuid="984193c5-686f-4058-b61f-fb42f8e745a3"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14" isBold="true"/>
					</textElement>
					<text><![CDATA[Bodega]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="0" y="30" width="102" height="28" uuid="ca786f60-cac3-456c-84dd-cbff6f0026aa"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Código]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="562" y="30" width="264" height="28" uuid="91654bf2-b734-445f-9751-3bcd5727b579"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Descripción]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="826" y="30" width="70" height="28" uuid="a4b279e8-c789-4a3e-b549-a28003352fdc"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Costo]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="896" y="30" width="70" height="28" uuid="fe688217-4d80-4c3d-bff2-010c3882d3e5"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Stock]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="102" y="30" width="210" height="28" forecolor="#666666" backcolor="#FFFFFF" uuid="bbcce05c-7628-41e0-9319-daeea09ff11f"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" rotation="None" markup="none">
						<font fontName="SansSerif" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Categoría]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="966" y="30" width="76" height="28" uuid="b4556e37-519b-461c-b3b9-340f4028e4b6"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Subtotal]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="312" y="30" width="250" height="28" forecolor="#666666" backcolor="#FFFFFF" uuid="cac79c37-3b00-4405-a14d-982b599b1318"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" rotation="None" markup="none">
						<font fontName="SansSerif" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Nombre]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="32">
				<staticText>
					<reportElement style="Column header" x="826" y="0" width="140" height="17" uuid="fedc831c-1cb0-454f-b0b4-329ad01e595e"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Bodega:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="966" y="0" width="76" height="17" uuid="21132827-20d2-452c-b132-46381a5d57ac"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right">
						<font size="11" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_general}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="categoria">
		<groupExpression><![CDATA[$F{categoria}]]></groupExpression>
		<groupFooter>
			<band height="27">
				<staticText>
					<reportElement style="Column header" x="826" y="0" width="140" height="17" uuid="fbfb3976-b0f8-4f9d-96f6-7b1be7099aa9"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Categoría:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="966" y="0" width="76" height="17" uuid="2d34dfac-8d42-4058-be51-c78204234180"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="0" width="594" height="40" uuid="cc48e6e9-d868-4234-be59-6c6334b8c0aa"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="24" isUnderline="false"/>
				</textElement>
				<text><![CDATA[Reporte Valoración de Inventario - Bodega]]></text>
			</staticText>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="594" y="0" width="232" height="40" uuid="c4e95e8b-aefd-4ea7-a008-eff6331c487f"/>
				<imageExpression><![CDATA[new java.net.URL($P{BASE_WEB}+"/../utility/ShowImageLogo?logo=yourcompanydoc&orgId="+ $F{organizationid})]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement style="Detail" x="0" y="0" width="102" height="15" uuid="e49fec67-32d3-47ea-8891-87095b09bb25"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{codigo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="Detail" x="562" y="0" width="264" height="15" uuid="5e03160c-0ea9-4d8b-9631-651040dae64d"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{descripcion}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0000">
				<reportElement style="Detail" x="826" y="0" width="70" height="15" uuid="77176442-1bbf-495e-8296-f3a7318e86d4"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{costo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="896" y="0" width="70" height="15" uuid="9a9679ed-0317-4bcf-a64a-2d3d762ca109"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{stock}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="966" y="0" width="76" height="15" uuid="48348fbc-a193-43c2-b282-89e261ee67cf"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{subtotal}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="102" y="0" width="210" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="5b37ad7d-3b57-4f35-b67e-0799443984b9"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{categoria}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="312" y="0" width="250" height="15" uuid="fc13f38d-1c56-4dcb-92b1-82b693fa8342"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textFieldExpression><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="922" y="0" width="80" height="20" uuid="407f4bee-a38e-43b7-ade7-48065e6c20a7"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Página "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="1002" y="0" width="40" height="20" uuid="ca815efa-de6c-44b6-9faf-3c73ffed5976"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy">
				<reportElement style="Column header" x="0" y="0" width="102" height="20" uuid="1eabf823-0800-4bd9-8baf-011bad9a5fd3"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
