<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_AnalisisDimensionalFacVentas" pageWidth="1030" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="990" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="USER_CLIENT" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="fechadesde" class="java.util.Date"/>
	<parameter name="fechahasta" class="java.util.Date"/>
	<parameter name="BASE_DESIGN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/var/lib/tomcat/webapps/ReportesGerencia/"]]></defaultValueExpression>
	</parameter>
	<parameter name="NUMBERFORMAT" class="java.text.DecimalFormat" isForPrompting="false">
		<defaultValueExpression><![CDATA[new DecimalFormat("#,##0.00",new DecimalFormatSymbols(Locale.US))]]></defaultValueExpression>
	</parameter>
	<parameter name="LOCALE" class="java.util.Locale" isForPrompting="false"/>
	<queryString>
		<![CDATA[select organizationid,
       sum(totalventas) as totalventas,
       sum(costo) as costo,
       sum(contribucion) as contribucion,
       sum(cantidad) as cantidad,
	   COALESCE(atecco_valor_metodopago(null,'EC100', date($P{fechadesde}), date($P{fechahasta}), null),0) as efectivo,
       COALESCE(atecco_valor_metodopago(null,'EC102', date($P{fechadesde}), date($P{fechahasta}), null),0) as cheque,
       COALESCE(atecco_valor_metodopago(null,'EC103', date($P{fechadesde}), date($P{fechahasta}), null),0) as transferencia,
       COALESCE(atecco_valor_metodopago(null,'EC105', date($P{fechadesde}), date($P{fechahasta}), null),0) as tarjeta
  from (
          select surcursal,
          round(sum(total_fact),4) as totalventas,
          sum(costo_fact)*-1 as costo,
          sum(contribucion_fact) as contribucion,
          sum(cantidad_fact) as cantidad,
          (select o.ad_org_id
             from ad_client c, ad_org o
            where c.ad_client_id = o.ad_client_id
              and o.isperiodcontrolallowed = 'Y'
              and c.ad_client_id in('60BCDFE538D94442895A861EF02829D7')
            limit 1) as organizationid
from(
        select surcursal,
               sum(total_fact) as total_fact,
               sum(cantidad) as cantidad_fact ,
               sum(costo)*-1 as costo_fact,
               sum(total_fact) - sum(costo) as contribucion_fact
          from
              (select o.name as surcursal,
                      c.documentno as documento,
                      c.totallines as  total_fact,
                      sum(il.qtyinvoiced) as cantidad ,
                      sum(M_GET_PRODUCT_COST(p.m_product_id,c.dateinvoiced, null)*il.qtyinvoiced) as costo
                 from c_invoice c
                inner join ad_org o on(c.ad_org_id=o.ad_org_id)
                inner join c_doctype dt on (c.c_doctype_id=dt.c_doctype_id and dt.docbasetype ='ARI')
                 left join c_invoiceline il on (il.c_invoice_id=c.c_invoice_id)
                 left join m_product p on(il.m_product_id=p.m_product_id)
                where c.issotrx = 'Y'
                  and c.docstatus = 'CO'
                  and c.PROCESSED = 'Y'
                  and date(c.dateinvoiced) >= date($P{fechadesde})
                  and date(c.dateinvoiced) <= date($P{fechahasta})
                group by 1,2,3
                order by 1
              )as facturas
         group by 1
    union all
         select surcursal, sum(total_fact)*-1 as total_nc,
                sum(cantidad)*-1 as cantidad_nc ,sum(costo) as costo_nc,
                (sum(total_fact)-sum(costo))*-1 as contribucion_nc
           from
              (
                 select o.name as surcursal,
                        c.documentno as documento,
                        c.totallines as  total_fact,
                        sum(il.qtyinvoiced) as cantidad ,
                        sum(M_GET_PRODUCT_COST(p.m_product_id,c.dateinvoiced, null)*il.qtyinvoiced) as costo
                 from c_invoice c
                inner join ad_org o on(c.ad_org_id=o.ad_org_id)
                inner join c_doctype dt on (c.c_doctype_id=dt.c_doctype_id and dt.docbasetype ='ARC') left join c_invoiceline il on (il.c_invoice_id=c.c_invoice_id)
                 left join m_product p on(il.m_product_id=p.m_product_id)
                where c.issotrx = 'Y'
                  and c.docstatus = 'CO'
                  and c.PROCESSED = 'Y'
                  and date(c.dateinvoiced) >= date($P{fechadesde})
                  and date(c.dateinvoiced) <= date($P{fechahasta})
                group by 1,2,3
                order by 1
     )as notasCredito
     group by 1
    ) as facturasventas
    group by 1
    order by 1
       ) as datos
       where totalventas <> 0
group by 1
LIMIT 1]]>
	</queryString>
	<field name="organizationid" class="java.lang.String"/>
	<field name="totalventas" class="java.math.BigDecimal"/>
	<field name="costo" class="java.math.BigDecimal"/>
	<field name="contribucion" class="java.math.BigDecimal"/>
	<field name="cantidad" class="java.math.BigDecimal"/>
	<field name="efectivo" class="java.math.BigDecimal"/>
	<field name="cheque" class="java.math.BigDecimal"/>
	<field name="transferencia" class="java.math.BigDecimal"/>
	<field name="tarjeta" class="java.math.BigDecimal"/>
	<variable name="cantidad_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{cantidad}]]></variableExpression>
	</variable>
	<variable name="totalventas_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{totalventas}]]></variableExpression>
	</variable>
	<variable name="costo_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{costo}]]></variableExpression>
	</variable>
	<variable name="contribucion_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{contribucion}]]></variableExpression>
	</variable>
	<variable name="grantotalventas" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{totalventas}]]></variableExpression>
	</variable>
	<variable name="v_efectivo" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{efectivo}]]></variableExpression>
	</variable>
	<variable name="v_cheque" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{cheque}]]></variableExpression>
	</variable>
	<variable name="v_transferencia" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{transferencia}]]></variableExpression>
	</variable>
	<variable name="v_tarjeta" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{tarjeta}]]></variableExpression>
	</variable>
	<title>
		<band height="64">
			<staticText>
				<reportElement x="141" y="0" width="334" height="35"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[ANÁLISIS DIMENSIONAL FACTURAS VENTAS]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="35" width="115" height="29"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Desde:]]></text>
			</staticText>
			<staticText>
				<reportElement x="234" y="35" width="61" height="29"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Hasta:]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Transparent" x="115" y="35" width="119" height="29" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$P{fechadesde}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="295" y="35" width="179" height="29" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$P{fechahasta}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="0" y="0" width="141" height="35"/>
				<imageExpression class="java.awt.Image"><![CDATA[org.openbravo.erpCommon.utility.Utility.showImageLogo("yourcompanylegal", $F{organizationid})]]></imageExpression>
			</image>
		</band>
	</title>
	<detail>
		<band height="22">
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="12" width="988" height="10"/>
				<subreportParameter name="fechadesde">
					<subreportParameterExpression><![CDATA[$P{fechadesde}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fechahasta">
					<subreportParameterExpression><![CDATA[$P{fechahasta}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="USER_CLIENT">
					<subreportParameterExpression><![CDATA[$P{USER_CLIENT}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="BASE_DESIGN">
					<subreportParameterExpression><![CDATA[$P{BASE_DESIGN}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN} + "/reports/Dontablero/FinalesDT/Rpt_AnalisisDimensionalFacVentas_Productos.jasper"]]></subreportExpression>
			</subreport>
			<rectangle>
				<reportElement x="1" y="0" width="475" height="12"/>
				<graphicElement>
					<pen lineColor="#FFFFFF"/>
				</graphicElement>
			</rectangle>
		</band>
		<band height="22">
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="12" width="988" height="10"/>
				<subreportParameter name="fechadesde">
					<subreportParameterExpression><![CDATA[$P{fechadesde}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fechahasta">
					<subreportParameterExpression><![CDATA[$P{fechahasta}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="USER_CLIENT">
					<subreportParameterExpression><![CDATA[$P{USER_CLIENT}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="BASE_DESIGN">
					<subreportParameterExpression><![CDATA[$P{BASE_DESIGN}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN} + "/reports/Dontablero/FinalesDT/Rpt_AnalisisDimensionalFacVentas_Servicios.jasper"]]></subreportExpression>
			</subreport>
			<rectangle>
				<reportElement x="1" y="2" width="475" height="10"/>
				<graphicElement>
					<pen lineColor="#FFFFFF"/>
				</graphicElement>
			</rectangle>
		</band>
		<band height="22">
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="12" width="988" height="10"/>
				<subreportParameter name="fechadesde">
					<subreportParameterExpression><![CDATA[$P{fechadesde}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fechahasta">
					<subreportParameterExpression><![CDATA[$P{fechahasta}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="USER_CLIENT">
					<subreportParameterExpression><![CDATA[$P{BASE_DESIGN}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="BASE_DESIGN">
					<subreportParameterExpression><![CDATA[$P{BASE_DESIGN}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN} + "/reports/Dontablero/FinalesDT/Rpt_AnalisisDimensionalFacVentas_Otros.jasper"]]></subreportExpression>
			</subreport>
			<rectangle>
				<reportElement x="0" y="0" width="475" height="12"/>
				<graphicElement>
					<pen lineColor="#FFFFFF"/>
				</graphicElement>
			</rectangle>
		</band>
	</detail>
	<columnFooter>
		<band height="42">
			<staticText>
				<reportElement x="0" y="16" width="175" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total:]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="15" width="474" height="1"/>
			</line>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="415" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{cantidad_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="175" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{totalventas_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="294" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{contribucion_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="354" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[($V{totalventas_1}.subtract( $V{costo_1})).divide($V{totalventas_1},2).multiply( new BigDecimal("100"))]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="509" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_efectivo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="869" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_tarjeta}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="929" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_tarjeta}.divide($V{totalventas_1}, 4)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="739" y="16" width="70" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_transferencia}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="569" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_efectivo}.divide($V{totalventas_1}, 4)]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="809" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_transferencia}.divide($V{totalventas_1}, 4)]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 %" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="679" y="16" width="60" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_cheque}.divide($V{totalventas_1}, 4)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="629" y="16" width="50" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{v_cheque}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="509" y="15" width="480" height="1"/>
			</line>
			<rectangle>
				<reportElement x="0" y="0" width="988" height="12"/>
				<graphicElement>
					<pen lineColor="#FFFFFF"/>
				</graphicElement>
			</rectangle>
			<textField pattern="#,##0.00">
				<reportElement x="235" y="16" width="59" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{costo_1}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
</jasperReport>
