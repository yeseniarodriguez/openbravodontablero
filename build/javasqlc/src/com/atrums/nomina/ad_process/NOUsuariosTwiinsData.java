//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NOUsuariosTwiinsData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOUsuariosTwiinsData.class);
  private String InitRecordNumber="0";
  public String uoeEmpresa;
  public String noContratoEmpleadoId;
  public String cBpartnerId;
  public String dgeTipoidentificacion;
  public String dgeNoidentificacion;
  public String primerNombre;
  public String segundoNombre;
  public String primerApellido;
  public String segundoApellido;
  public String dgePrimernombre;
  public String dgeApellidopaterno;
  public String dgeEmail;
  public String dgeTelefono;
  public String uoeDepartamento;
  public String uoeArea;
  public String uoeJerarquia;
  public String uoeCargo;
  public String ufeOficinalocal;
  public String dpeGenero;
  public String dpeEstadocivil;
  public String dpeFecnacimiento;
  public String dpeEmailpersonal;
  public String dpeCiudadnacimiento;
  public String dpePaisnacimiento;
  public String ddProvincia;
  public String ddCanton;
  public String ddCalleprincipal;
  public String ddTelfconvencional;
  public String ddTelfcelular;
  public String eeFechaingreso;
  public String eeEstado;
  public String dptSueldobase;
  public String dptEstanomina;
  public String dpsFormapago;
  public String dpsBanco;
  public String dpsTipocuenta;
  public String dpsNumerocuenta;
  public String denRegimenlaboral;
  public String dmDiscapacidad;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("uoe_empresa") || fieldName.equals("uoeEmpresa"))
      return uoeEmpresa;
    else if (fieldName.equalsIgnoreCase("no_contrato_empleado_id") || fieldName.equals("noContratoEmpleadoId"))
      return noContratoEmpleadoId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("dge_tipoidentificacion") || fieldName.equals("dgeTipoidentificacion"))
      return dgeTipoidentificacion;
    else if (fieldName.equalsIgnoreCase("dge_noidentificacion") || fieldName.equals("dgeNoidentificacion"))
      return dgeNoidentificacion;
    else if (fieldName.equalsIgnoreCase("primer_nombre") || fieldName.equals("primerNombre"))
      return primerNombre;
    else if (fieldName.equalsIgnoreCase("segundo_nombre") || fieldName.equals("segundoNombre"))
      return segundoNombre;
    else if (fieldName.equalsIgnoreCase("primer_apellido") || fieldName.equals("primerApellido"))
      return primerApellido;
    else if (fieldName.equalsIgnoreCase("segundo_apellido") || fieldName.equals("segundoApellido"))
      return segundoApellido;
    else if (fieldName.equalsIgnoreCase("dge_primernombre") || fieldName.equals("dgePrimernombre"))
      return dgePrimernombre;
    else if (fieldName.equalsIgnoreCase("dge_apellidopaterno") || fieldName.equals("dgeApellidopaterno"))
      return dgeApellidopaterno;
    else if (fieldName.equalsIgnoreCase("dge_email") || fieldName.equals("dgeEmail"))
      return dgeEmail;
    else if (fieldName.equalsIgnoreCase("dge_telefono") || fieldName.equals("dgeTelefono"))
      return dgeTelefono;
    else if (fieldName.equalsIgnoreCase("uoe_departamento") || fieldName.equals("uoeDepartamento"))
      return uoeDepartamento;
    else if (fieldName.equalsIgnoreCase("uoe_area") || fieldName.equals("uoeArea"))
      return uoeArea;
    else if (fieldName.equalsIgnoreCase("uoe_jerarquia") || fieldName.equals("uoeJerarquia"))
      return uoeJerarquia;
    else if (fieldName.equalsIgnoreCase("uoe_cargo") || fieldName.equals("uoeCargo"))
      return uoeCargo;
    else if (fieldName.equalsIgnoreCase("ufe_oficinalocal") || fieldName.equals("ufeOficinalocal"))
      return ufeOficinalocal;
    else if (fieldName.equalsIgnoreCase("dpe_genero") || fieldName.equals("dpeGenero"))
      return dpeGenero;
    else if (fieldName.equalsIgnoreCase("dpe_estadocivil") || fieldName.equals("dpeEstadocivil"))
      return dpeEstadocivil;
    else if (fieldName.equalsIgnoreCase("dpe_fecnacimiento") || fieldName.equals("dpeFecnacimiento"))
      return dpeFecnacimiento;
    else if (fieldName.equalsIgnoreCase("dpe_emailpersonal") || fieldName.equals("dpeEmailpersonal"))
      return dpeEmailpersonal;
    else if (fieldName.equalsIgnoreCase("dpe_ciudadnacimiento") || fieldName.equals("dpeCiudadnacimiento"))
      return dpeCiudadnacimiento;
    else if (fieldName.equalsIgnoreCase("dpe_paisnacimiento") || fieldName.equals("dpePaisnacimiento"))
      return dpePaisnacimiento;
    else if (fieldName.equalsIgnoreCase("dd_provincia") || fieldName.equals("ddProvincia"))
      return ddProvincia;
    else if (fieldName.equalsIgnoreCase("dd_canton") || fieldName.equals("ddCanton"))
      return ddCanton;
    else if (fieldName.equalsIgnoreCase("dd_calleprincipal") || fieldName.equals("ddCalleprincipal"))
      return ddCalleprincipal;
    else if (fieldName.equalsIgnoreCase("dd_telfconvencional") || fieldName.equals("ddTelfconvencional"))
      return ddTelfconvencional;
    else if (fieldName.equalsIgnoreCase("dd_telfcelular") || fieldName.equals("ddTelfcelular"))
      return ddTelfcelular;
    else if (fieldName.equalsIgnoreCase("ee_fechaingreso") || fieldName.equals("eeFechaingreso"))
      return eeFechaingreso;
    else if (fieldName.equalsIgnoreCase("ee_estado") || fieldName.equals("eeEstado"))
      return eeEstado;
    else if (fieldName.equalsIgnoreCase("dpt_sueldobase") || fieldName.equals("dptSueldobase"))
      return dptSueldobase;
    else if (fieldName.equalsIgnoreCase("dpt_estanomina") || fieldName.equals("dptEstanomina"))
      return dptEstanomina;
    else if (fieldName.equalsIgnoreCase("dps_formapago") || fieldName.equals("dpsFormapago"))
      return dpsFormapago;
    else if (fieldName.equalsIgnoreCase("dps_banco") || fieldName.equals("dpsBanco"))
      return dpsBanco;
    else if (fieldName.equalsIgnoreCase("dps_tipocuenta") || fieldName.equals("dpsTipocuenta"))
      return dpsTipocuenta;
    else if (fieldName.equalsIgnoreCase("dps_numerocuenta") || fieldName.equals("dpsNumerocuenta"))
      return dpsNumerocuenta;
    else if (fieldName.equalsIgnoreCase("den_regimenlaboral") || fieldName.equals("denRegimenlaboral"))
      return denRegimenlaboral;
    else if (fieldName.equalsIgnoreCase("dm_discapacidad") || fieldName.equals("dmDiscapacidad"))
      return dmDiscapacidad;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOUsuariosTwiinsData[] select(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    return select(connectionProvider, noContratoEmpleadoId, 0, 0);
  }

  public static NOUsuariosTwiinsData[] select(ConnectionProvider connectionProvider, String noContratoEmpleadoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select '1' as UOE_empresa," +
      "		       c.no_contrato_empleado_id," +
      "		       a.c_bpartner_id," +
      "		       (case when a.em_co_tipo_identificacion = '01' then 'R'" +
      "		             when a.em_co_tipo_identificacion = '02' then 'C'" +
      "		             when a.em_co_tipo_identificacion = '03' then 'P'" +
      "		        end) as DGE_tipoIdentificacion," +
      "		        a.taxid as DGE_noIdentificacion," +
      "		        coalesce(split_part(a.name,' ',1),' ')  as primer_nombre," +
      "		        coalesce(split_part(a.name,' ',2),' ')  as segundo_nombre," +
      "		        coalesce(split_part(a.name,' ',3),' ')  as primer_apellido," +
      "		        coalesce(split_part(a.name,' ',4),' ')  as segundo_apellido," +
      "		        coalesce(a.em_co_nombres,' ') as DGE_primerNombre," +
      "		        coalesce(a.em_co_apellidos,' ') as DGE_apellidoPaterno," +
      "		        a.em_co_email as DGE_email," +
      "		        g.phone as DGE_telefono," +
      "		        n.name as UOE_Departamento," +
      "		        trim(d.nombre) as UOE_area," +
      "		        trim(m.name) as UOE_jerarquia," +
      "		        trim(f.name) as UOE_cargo," +
      "		        upper(l.name) as UFE_oficinaLocal," +
      "		        (select upper(s.name)" +
      "		          from ad_ref_list s" +
      "		         where s.ad_reference_id = 'C3D96AF9E9BA410FB45333086B6AD567'" +
      "		           and value = a.em_no_genero) as DPE_genero," +
      "		        (select upper(split_part(t.name,'(',1))" +
      "		           from ad_ref_list t" +
      "		          where t.ad_reference_id = '9A3725A8049E4409B39AA3F54BF379A7'" +
      "		            and value = a.em_no_estadocivil) as DPE_estadoCivil," +
      "		       to_char(a.em_no_fechanacimiento,'YYYY/mm/dd') as DPE_fecNacimiento,    " +
      "		       a.em_co_email as DPE_emailPersonal, " +
      "		       upper(k.name) as DPE_ciudadNacimiento," +
      "		       upper(i.name) as DPE_paisNacimiento," +
      "		       upper(j.name) as DD_provincia," +
      "		       upper(k.name) as DD_canton," +
      "		       h.address1 as DD_callePrincipal," +
      "		       g.phone as DD_telfConvencional," +
      "		       g.phone2 as DD_telfCelular," +
      "		       to_char(c.fecha_inicio,'YYYY/mm/dd') as EE_fechaIngreso," +
      "		       (case when a.isactive = 'Y' then 'A' else 'I' end) as EE_estado," +
      "		       c.salario as DPT_sueldoBase," +
      "		       'S' as DPT_estaNomina, " +
      "		       'TRANSFERENCIA' as DPS_formaPago,		       " +
      "		       upper(e.bank_name) as DPS_banco, " +
      "		       (select upper(y.name)" +
      "				  from ad_ref_list y " +
      "				 where y.ad_reference_id = 'C123B7BF5B2C438D84D2E509734776B5'" +
      "				   and y.value = e.bankformat) as DPS_tipoCuenta," +
      "		       e.accountno as DPS_numeroCuenta," +
      "		       (select upper(split_part(t.name,'/',1))" +
      "		          from ad_ref_list t" +
      "		         where t.ad_reference_id = '5399B74563094E35B96D005012B4A6D9'" +
      "		           and value = c.em_ne_region) as DEN_regimenLaboral, 		      " +
      "		       (case when a.em_no_isdiscapacitado = 'Y' then 'S' else 'N' end) as DM_discapacidad      " +
      "		  from c_bpartner a" +
      "		  left join ad_user b on (a.c_bpartner_id = b.c_bpartner_id and b.em_no_rol_mail = 'Y')" +
      "		  left join no_contrato_empleado c on (a.c_bpartner_id = c.c_bpartner_id)" +
      "		  left join no_area_empresa d on (d.no_area_empresa_id = c.em_ne_area_empresa_id)" +
      "		  left join c_bp_bankaccount e on (e.c_bpartner_id = a.c_bpartner_id)" +
      "		  left join atnorh_cargo f on (f.atnorh_cargo_id = c.em_atnorh_cargo_id)" +
      "		  left join c_bpartner_location g on (g.c_bpartner_id = a.c_bpartner_id)" +
      "		  left join c_location h on (h.c_location_id = g.c_location_id)" +
      "		  left join c_country i on (i.c_country_id = h.c_country_id)" +
      "		  left join c_region j on (j.c_region_id = h.c_region_id)" +
      "		  left join c_city k on (k.c_city_id = h.c_city_id)" +
      "		  left join ad_org l on (c.ad_org_id = l.ad_org_id)" +
      "		  left join atnorh_cargo m on (m.atnorh_cargo_id = c.em_atnorh_cargo_je_id)" +
      "		  left join no_departamento n on (n.no_departamento_id = c.no_departamento_id)" +
      "		 where c.no_contrato_empleado_id = ?" +
      "		   and a.em_no_enviar_twiins = 'Y'" +
      " 		 order by 3		";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOUsuariosTwiinsData objectNOUsuariosTwiinsData = new NOUsuariosTwiinsData();
        objectNOUsuariosTwiinsData.uoeEmpresa = UtilSql.getValue(result, "uoe_empresa");
        objectNOUsuariosTwiinsData.noContratoEmpleadoId = UtilSql.getValue(result, "no_contrato_empleado_id");
        objectNOUsuariosTwiinsData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectNOUsuariosTwiinsData.dgeTipoidentificacion = UtilSql.getValue(result, "dge_tipoidentificacion");
        objectNOUsuariosTwiinsData.dgeNoidentificacion = UtilSql.getValue(result, "dge_noidentificacion");
        objectNOUsuariosTwiinsData.primerNombre = UtilSql.getValue(result, "primer_nombre");
        objectNOUsuariosTwiinsData.segundoNombre = UtilSql.getValue(result, "segundo_nombre");
        objectNOUsuariosTwiinsData.primerApellido = UtilSql.getValue(result, "primer_apellido");
        objectNOUsuariosTwiinsData.segundoApellido = UtilSql.getValue(result, "segundo_apellido");
        objectNOUsuariosTwiinsData.dgePrimernombre = UtilSql.getValue(result, "dge_primernombre");
        objectNOUsuariosTwiinsData.dgeApellidopaterno = UtilSql.getValue(result, "dge_apellidopaterno");
        objectNOUsuariosTwiinsData.dgeEmail = UtilSql.getValue(result, "dge_email");
        objectNOUsuariosTwiinsData.dgeTelefono = UtilSql.getValue(result, "dge_telefono");
        objectNOUsuariosTwiinsData.uoeDepartamento = UtilSql.getValue(result, "uoe_departamento");
        objectNOUsuariosTwiinsData.uoeArea = UtilSql.getValue(result, "uoe_area");
        objectNOUsuariosTwiinsData.uoeJerarquia = UtilSql.getValue(result, "uoe_jerarquia");
        objectNOUsuariosTwiinsData.uoeCargo = UtilSql.getValue(result, "uoe_cargo");
        objectNOUsuariosTwiinsData.ufeOficinalocal = UtilSql.getValue(result, "ufe_oficinalocal");
        objectNOUsuariosTwiinsData.dpeGenero = UtilSql.getValue(result, "dpe_genero");
        objectNOUsuariosTwiinsData.dpeEstadocivil = UtilSql.getValue(result, "dpe_estadocivil");
        objectNOUsuariosTwiinsData.dpeFecnacimiento = UtilSql.getValue(result, "dpe_fecnacimiento");
        objectNOUsuariosTwiinsData.dpeEmailpersonal = UtilSql.getValue(result, "dpe_emailpersonal");
        objectNOUsuariosTwiinsData.dpeCiudadnacimiento = UtilSql.getValue(result, "dpe_ciudadnacimiento");
        objectNOUsuariosTwiinsData.dpePaisnacimiento = UtilSql.getValue(result, "dpe_paisnacimiento");
        objectNOUsuariosTwiinsData.ddProvincia = UtilSql.getValue(result, "dd_provincia");
        objectNOUsuariosTwiinsData.ddCanton = UtilSql.getValue(result, "dd_canton");
        objectNOUsuariosTwiinsData.ddCalleprincipal = UtilSql.getValue(result, "dd_calleprincipal");
        objectNOUsuariosTwiinsData.ddTelfconvencional = UtilSql.getValue(result, "dd_telfconvencional");
        objectNOUsuariosTwiinsData.ddTelfcelular = UtilSql.getValue(result, "dd_telfcelular");
        objectNOUsuariosTwiinsData.eeFechaingreso = UtilSql.getValue(result, "ee_fechaingreso");
        objectNOUsuariosTwiinsData.eeEstado = UtilSql.getValue(result, "ee_estado");
        objectNOUsuariosTwiinsData.dptSueldobase = UtilSql.getValue(result, "dpt_sueldobase");
        objectNOUsuariosTwiinsData.dptEstanomina = UtilSql.getValue(result, "dpt_estanomina");
        objectNOUsuariosTwiinsData.dpsFormapago = UtilSql.getValue(result, "dps_formapago");
        objectNOUsuariosTwiinsData.dpsBanco = UtilSql.getValue(result, "dps_banco");
        objectNOUsuariosTwiinsData.dpsTipocuenta = UtilSql.getValue(result, "dps_tipocuenta");
        objectNOUsuariosTwiinsData.dpsNumerocuenta = UtilSql.getValue(result, "dps_numerocuenta");
        objectNOUsuariosTwiinsData.denRegimenlaboral = UtilSql.getValue(result, "den_regimenlaboral");
        objectNOUsuariosTwiinsData.dmDiscapacidad = UtilSql.getValue(result, "dm_discapacidad");
        objectNOUsuariosTwiinsData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOUsuariosTwiinsData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOUsuariosTwiinsData objectNOUsuariosTwiinsData[] = new NOUsuariosTwiinsData[vector.size()];
    vector.copyInto(objectNOUsuariosTwiinsData);
    return(objectNOUsuariosTwiinsData);
  }

  public static int actualizaTwiinsTercero(ConnectionProvider connectionProvider, String emNoMensajeTwiins, String emNoEnviarTwiins, String cBpartnerId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  		update c_bpartner" +
      "			   set em_no_mensaje_twiins = ?," +
      "			       em_no_enviar_twiins = substring(?,1,1)" +
      "			 where c_bpartner_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNoMensajeTwiins);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNoEnviarTwiins);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
