//Sqlc generated V1.O00-1
package com.atrums.contabilidad.erpCommon.ad_reports;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CODatosLibroMayorData implements FieldProvider {
static Logger log4j = Logger.getLogger(CODatosLibroMayorData.class);
  private String InitRecordNumber="0";
  public String cElementvalueId;
  public String fechainicio;
  public String fechafin;
  public String email;
  public String coEnvioReportesId;
  public String value;
  public String nombreCuenta;
  public String valueHasta;
  public String nombreCuentaHasta;
  public String empresa;
  public String cElementvaluehastaId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("c_elementvalue_id") || fieldName.equals("cElementvalueId"))
      return cElementvalueId;
    else if (fieldName.equalsIgnoreCase("fechainicio"))
      return fechainicio;
    else if (fieldName.equalsIgnoreCase("fechafin"))
      return fechafin;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("co_envio_reportes_id") || fieldName.equals("coEnvioReportesId"))
      return coEnvioReportesId;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("nombre_cuenta") || fieldName.equals("nombreCuenta"))
      return nombreCuenta;
    else if (fieldName.equalsIgnoreCase("value_hasta") || fieldName.equals("valueHasta"))
      return valueHasta;
    else if (fieldName.equalsIgnoreCase("nombre_cuenta_hasta") || fieldName.equals("nombreCuentaHasta"))
      return nombreCuentaHasta;
    else if (fieldName.equalsIgnoreCase("empresa"))
      return empresa;
    else if (fieldName.equalsIgnoreCase("c_elementvaluehasta_id") || fieldName.equals("cElementvaluehastaId"))
      return cElementvaluehastaId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CODatosLibroMayorData[] selecionarDatosReporte(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return selecionarDatosReporte(connectionProvider, adClientId, 0, 0);
  }

  public static CODatosLibroMayorData[] selecionarDatosReporte(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select a.c_elementvalue_id," +
      "	         a.fechainicio," +
      "	         a.fechafin," +
      "	         a.email," +
      "	         a.co_envio_reportes_id," +
      "	         (select e.value" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvalue_id) as value," +
      "			 (select e.name" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvalue_id) as nombre_cuenta," +
      "			 (select e.value" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvaluehasta_id) as value_hasta," +
      "			 (select e.name" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvaluehasta_id) as nombre_cuenta_hasta," +
      "			 (select d.name" +
      "			    from ad_client d" +
      "			   where d.ad_client_id = a.ad_client_id) as empresa," +
      "			 a.c_elementvaluehasta_id" +
      "	    from co_envio_reportes a" +
      "	   where a.ad_client_id = ?" +
      "	     and a.isactive = 'Y'" +
      "	   order by created asc" +
      "	   limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosLibroMayorData objectCODatosLibroMayorData = new CODatosLibroMayorData();
        objectCODatosLibroMayorData.cElementvalueId = UtilSql.getValue(result, "c_elementvalue_id");
        objectCODatosLibroMayorData.fechainicio = UtilSql.getDateValue(result, "fechainicio", "dd-MM-yyyy");
        objectCODatosLibroMayorData.fechafin = UtilSql.getDateValue(result, "fechafin", "dd-MM-yyyy");
        objectCODatosLibroMayorData.email = UtilSql.getValue(result, "email");
        objectCODatosLibroMayorData.coEnvioReportesId = UtilSql.getValue(result, "co_envio_reportes_id");
        objectCODatosLibroMayorData.value = UtilSql.getValue(result, "value");
        objectCODatosLibroMayorData.nombreCuenta = UtilSql.getValue(result, "nombre_cuenta");
        objectCODatosLibroMayorData.valueHasta = UtilSql.getValue(result, "value_hasta");
        objectCODatosLibroMayorData.nombreCuentaHasta = UtilSql.getValue(result, "nombre_cuenta_hasta");
        objectCODatosLibroMayorData.empresa = UtilSql.getValue(result, "empresa");
        objectCODatosLibroMayorData.cElementvaluehastaId = UtilSql.getValue(result, "c_elementvaluehasta_id");
        objectCODatosLibroMayorData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosLibroMayorData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosLibroMayorData objectCODatosLibroMayorData[] = new CODatosLibroMayorData[vector.size()];
    vector.copyInto(objectCODatosLibroMayorData);
    return(objectCODatosLibroMayorData);
  }

  public static int actualizarEnvioLibro(ConnectionProvider connectionProvider, String coEnvioReportesId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	      update co_envio_reportes" +
      "			 set isactive = 'N', mensaje = 'El reporte ha sido enviado.', updated=now()" +
      "		   where co_envio_reportes_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coEnvioReportesId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarNoEnvioLibro(ConnectionProvider connectionProvider, String coEnvioReportesId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	      update co_envio_reportes" +
      "			 set isactive = 'N', mensaje = 'No se logra enviar el reporte.', updated=now()" +
      "		   where co_envio_reportes_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coEnvioReportesId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int insertarLibroMayor(ConnectionProvider connectionProvider, String name, String ad_record_id, String path)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	      INSERT INTO c_file(c_file_id, ad_client_id, ad_org_id, isactive, " +
      "                          created, createdby, updated, updatedby, " +
      "                          name, c_datatype_id, seqno, text, " +
      "                          ad_table_id, ad_record_id, path, c_attachment_conf_id)" +
      "			  VALUES(get_uuid(), '60BCDFE538D94442895A861EF02829D7', '0', 'Y', " +
      "			         now(), '100', now(), '100', " +
      "			         ?, " +
      "			         'application/zip', 10, NULL, " +
      "			         'D5ACD79BE6D84F79B8E9E1C2339A3171', " +
      "			         ?, ?, NULL)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_record_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, path);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
