//Sqlc generated V1.O00-1
package com.atrums.contabilidad.erpCommon.ad_reports;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CODatosReporteData implements FieldProvider {
static Logger log4j = Logger.getLogger(CODatosReporteData.class);
  private String InitRecordNumber="0";
  public String fechainicio;
  public String fechafin;
  public String estado;
  public String mensaje;
  public String email;
  public String reporte;
  public String direccionReporte;
  public String coOpcionDescargaReporteId;
  public String nombrereporte;
  public String fecha;
  public String cElementvalueId;
  public String cElementvaluehastaId;
  public String value;
  public String nombreCuenta;
  public String valueHasta;
  public String nombreCuentaHasta;
  public String empresa;
  public String nombreXml;
  public String nombreArchivo;
  public String tipoInformacion;
  public String procedimiento;
  public String adOrgId;
  public String cBpartnerId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("fechainicio"))
      return fechainicio;
    else if (fieldName.equalsIgnoreCase("fechafin"))
      return fechafin;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("mensaje"))
      return mensaje;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("reporte"))
      return reporte;
    else if (fieldName.equalsIgnoreCase("direccion_reporte") || fieldName.equals("direccionReporte"))
      return direccionReporte;
    else if (fieldName.equalsIgnoreCase("co_opcion_descarga_reporte_id") || fieldName.equals("coOpcionDescargaReporteId"))
      return coOpcionDescargaReporteId;
    else if (fieldName.equalsIgnoreCase("nombrereporte"))
      return nombrereporte;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("c_elementvalue_id") || fieldName.equals("cElementvalueId"))
      return cElementvalueId;
    else if (fieldName.equalsIgnoreCase("c_elementvaluehasta_id") || fieldName.equals("cElementvaluehastaId"))
      return cElementvaluehastaId;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("nombre_cuenta") || fieldName.equals("nombreCuenta"))
      return nombreCuenta;
    else if (fieldName.equalsIgnoreCase("value_hasta") || fieldName.equals("valueHasta"))
      return valueHasta;
    else if (fieldName.equalsIgnoreCase("nombre_cuenta_hasta") || fieldName.equals("nombreCuentaHasta"))
      return nombreCuentaHasta;
    else if (fieldName.equalsIgnoreCase("empresa"))
      return empresa;
    else if (fieldName.equalsIgnoreCase("nombre_xml") || fieldName.equals("nombreXml"))
      return nombreXml;
    else if (fieldName.equalsIgnoreCase("nombre_archivo") || fieldName.equals("nombreArchivo"))
      return nombreArchivo;
    else if (fieldName.equalsIgnoreCase("tipo_informacion") || fieldName.equals("tipoInformacion"))
      return tipoInformacion;
    else if (fieldName.equalsIgnoreCase("procedimiento"))
      return procedimiento;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CODatosReporteData[] selecionarInfoReporte(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return selecionarInfoReporte(connectionProvider, adClientId, 0, 0);
  }

  public static CODatosReporteData[] selecionarInfoReporte(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select a.fechainicio,         " +
      "		       a.fechafin,            " +
      "		       a.estado,   " +
      "		       a.mensaje,     " +
      "		       a.email, " +
      "		       a.reporte," +
      "		       a.direccion_reporte," +
      "		       a.co_opcion_descarga_reporte_id," +
      "		       ((select b.name" +
      "		          from ad_ref_list b" +
      "		         where b.value = a.reporte" +
      "		           and b.ad_reference_id = '1A60D0714F29484996E833D2DB0BC0E9')||'_'||to_char(CURRENT_DATE,'DDMMYYYY')) as nombrereporte," +
      "		       to_char(CURRENT_DATE,'DD_MM_YYYY') as fecha," +
      "		       '' as c_elementvalue_id," +
      "			   '' as c_elementvaluehasta_id," +
      "			   '' as value," +
      "			   '' as nombre_cuenta," +
      "			   '' as value_hasta," +
      "			   '' as nombre_cuenta_hasta," +
      "			 (select d.name" +
      "			    from ad_client d" +
      "			   where d.ad_client_id = a.ad_client_id) as empresa," +
      "			   '' as nombre_xml," +
      "			   '' as nombre_archivo," +
      "			   '' as tipo_informacion," +
      "			   '' as procedimiento," +
      "			   '' as ad_org_id," +
      "			   '' as c_bpartner_id" +
      "		  from co_opcion_descarga_reporte a" +
      "		 where a.mensaje = 'EE'" +
      "		   and a.ad_client_id = ?" +
      "		 order by a.created asc limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosReporteData objectCODatosReporteData = new CODatosReporteData();
        objectCODatosReporteData.fechainicio = UtilSql.getDateValue(result, "fechainicio", "dd-MM-yyyy");
        objectCODatosReporteData.fechafin = UtilSql.getDateValue(result, "fechafin", "dd-MM-yyyy");
        objectCODatosReporteData.estado = UtilSql.getValue(result, "estado");
        objectCODatosReporteData.mensaje = UtilSql.getValue(result, "mensaje");
        objectCODatosReporteData.email = UtilSql.getValue(result, "email");
        objectCODatosReporteData.reporte = UtilSql.getValue(result, "reporte");
        objectCODatosReporteData.direccionReporte = UtilSql.getValue(result, "direccion_reporte");
        objectCODatosReporteData.coOpcionDescargaReporteId = UtilSql.getValue(result, "co_opcion_descarga_reporte_id");
        objectCODatosReporteData.nombrereporte = UtilSql.getValue(result, "nombrereporte");
        objectCODatosReporteData.fecha = UtilSql.getValue(result, "fecha");
        objectCODatosReporteData.cElementvalueId = UtilSql.getValue(result, "c_elementvalue_id");
        objectCODatosReporteData.cElementvaluehastaId = UtilSql.getValue(result, "c_elementvaluehasta_id");
        objectCODatosReporteData.value = UtilSql.getValue(result, "value");
        objectCODatosReporteData.nombreCuenta = UtilSql.getValue(result, "nombre_cuenta");
        objectCODatosReporteData.valueHasta = UtilSql.getValue(result, "value_hasta");
        objectCODatosReporteData.nombreCuentaHasta = UtilSql.getValue(result, "nombre_cuenta_hasta");
        objectCODatosReporteData.empresa = UtilSql.getValue(result, "empresa");
        objectCODatosReporteData.nombreXml = UtilSql.getValue(result, "nombre_xml");
        objectCODatosReporteData.nombreArchivo = UtilSql.getValue(result, "nombre_archivo");
        objectCODatosReporteData.tipoInformacion = UtilSql.getValue(result, "tipo_informacion");
        objectCODatosReporteData.procedimiento = UtilSql.getValue(result, "procedimiento");
        objectCODatosReporteData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCODatosReporteData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectCODatosReporteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosReporteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosReporteData objectCODatosReporteData[] = new CODatosReporteData[vector.size()];
    vector.copyInto(objectCODatosReporteData);
    return(objectCODatosReporteData);
  }

  public static int actualizarReporteEnEjecucion(ConnectionProvider connectionProvider, String coOpcionDescargaReporteId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_opcion_descarga_reporte" +
      "   				set mensaje = 'EJ', updated = now()" +
      " 			  where co_opcion_descarga_reporte_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coOpcionDescargaReporteId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarReporteError(ConnectionProvider connectionProvider, String coOpcionDescargaReporteId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_opcion_descarga_reporte" +
      "                set mensaje = 'ER', updated = now()" +
      "              where co_opcion_descarga_reporte_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coOpcionDescargaReporteId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarReporteEnviado(ConnectionProvider connectionProvider, String coOpcionDescargaReporteId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_opcion_descarga_reporte" +
      "                set mensaje = 'EN', updated = now()" +
      "              where co_opcion_descarga_reporte_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coOpcionDescargaReporteId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static CODatosReporteData[] validarReportesEnEjecucion(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return validarReportesEnEjecucion(connectionProvider, adClientId, 0, 0);
  }

  public static CODatosReporteData[] validarReportesEnEjecucion(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 select a.co_opcion_descarga_reporte_id" +
      "			   from co_opcion_descarga_reporte a, ad_ref_list b" +
      "			  where a.mensaje = b.value" +
      "			    and b.ad_reference_id = 'B062BA11925C45898076B05579757F32'" +
      "			    and a.mensaje = 'EJ'" +
      "			    and a.ad_client_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosReporteData objectCODatosReporteData = new CODatosReporteData();
        objectCODatosReporteData.coOpcionDescargaReporteId = UtilSql.getValue(result, "co_opcion_descarga_reporte_id");
        objectCODatosReporteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosReporteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosReporteData objectCODatosReporteData[] = new CODatosReporteData[vector.size()];
    vector.copyInto(objectCODatosReporteData);
    return(objectCODatosReporteData);
  }

  public static CODatosReporteData[] selecionarReporteContabilidad(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return selecionarReporteContabilidad(connectionProvider, adClientId, 0, 0);
  }

  public static CODatosReporteData[] selecionarReporteContabilidad(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select a.fechainicio,         " +
      "		       a.fechafin,            " +
      "		       a.estado,   " +
      "		       a.mensaje,     " +
      "		       a.email, " +
      "		       a.reporte," +
      "		       a.direccion_reporte," +
      "		       a.co_reportes_contabilidad_id as co_opcion_descarga_reporte_id," +
      "		      (case when a.reporte = 'ATS' then" +
      "                    (select b.name" +
      "                      from ad_ref_list b" +
      "                     where b.value = a.reporte" +
      "                       and b.ad_reference_id = 'AFF3FEBD7B3F471CBFEF9E51A96AC7F8')" +
      "		       else" +
      "                   ((select b.name" +
      "                      from ad_ref_list b" +
      "                     where b.value = a.reporte" +
      "                       and b.ad_reference_id = 'AFF3FEBD7B3F471CBFEF9E51A96AC7F8')||'_'||to_char(CURRENT_DATE,'DDMMYYYY'))" +
      "		       end)  as nombrereporte," +
      "		        to_char(CURRENT_DATE,'DD_MM_YYYY') as fecha," +
      "		       a.c_elementvalue_id," +
      "			   a.c_elementvaluehasta_id," +
      "			   (select e.value" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvalue_id) as value," +
      "			 (select e.name" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvalue_id) as nombre_cuenta," +
      "			 (select e.value" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvaluehasta_id) as value_hasta," +
      "			 (select e.name" +
      "			    from c_elementvalue e" +
      "			   where e.c_elementvalue_id = a.c_elementvaluehasta_id) as nombre_cuenta_hasta," +
      "			 (select d.name" +
      "			    from ad_client d" +
      "			   where d.ad_client_id = a.ad_client_id) as empresa," +
      "			   a.ad_org_id," +
      "			   a.c_bpartner_id" +
      "		  from co_reportes_contabilidad a" +
      "		 where a.mensaje = 'EE'" +
      "		   and a.ad_client_id = ?" +
      "		 order by a.created asc limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosReporteData objectCODatosReporteData = new CODatosReporteData();
        objectCODatosReporteData.fechainicio = UtilSql.getDateValue(result, "fechainicio", "dd-MM-yyyy");
        objectCODatosReporteData.fechafin = UtilSql.getDateValue(result, "fechafin", "dd-MM-yyyy");
        objectCODatosReporteData.estado = UtilSql.getValue(result, "estado");
        objectCODatosReporteData.mensaje = UtilSql.getValue(result, "mensaje");
        objectCODatosReporteData.email = UtilSql.getValue(result, "email");
        objectCODatosReporteData.reporte = UtilSql.getValue(result, "reporte");
        objectCODatosReporteData.direccionReporte = UtilSql.getValue(result, "direccion_reporte");
        objectCODatosReporteData.coOpcionDescargaReporteId = UtilSql.getValue(result, "co_opcion_descarga_reporte_id");
        objectCODatosReporteData.nombrereporte = UtilSql.getValue(result, "nombrereporte");
        objectCODatosReporteData.fecha = UtilSql.getValue(result, "fecha");
        objectCODatosReporteData.cElementvalueId = UtilSql.getValue(result, "c_elementvalue_id");
        objectCODatosReporteData.cElementvaluehastaId = UtilSql.getValue(result, "c_elementvaluehasta_id");
        objectCODatosReporteData.value = UtilSql.getValue(result, "value");
        objectCODatosReporteData.nombreCuenta = UtilSql.getValue(result, "nombre_cuenta");
        objectCODatosReporteData.valueHasta = UtilSql.getValue(result, "value_hasta");
        objectCODatosReporteData.nombreCuentaHasta = UtilSql.getValue(result, "nombre_cuenta_hasta");
        objectCODatosReporteData.empresa = UtilSql.getValue(result, "empresa");
        objectCODatosReporteData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCODatosReporteData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectCODatosReporteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosReporteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosReporteData objectCODatosReporteData[] = new CODatosReporteData[vector.size()];
    vector.copyInto(objectCODatosReporteData);
    return(objectCODatosReporteData);
  }

  public static int actualizarReporteEnEjecucionCont(ConnectionProvider connectionProvider, String coReportesContabilidadId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_reportes_contabilidad" +
      "   				set mensaje = 'EJ', updated = now()" +
      " 			  where co_reportes_contabilidad_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coReportesContabilidadId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarReporteErrorCont(ConnectionProvider connectionProvider, String coReportesContabilidadId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_reportes_contabilidad" +
      "                set mensaje = 'ER', updated = now()" +
      "              where co_reportes_contabilidad_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coReportesContabilidadId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarReporteEnviadoCont(ConnectionProvider connectionProvider, String coReportesContabilidadId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 update co_reportes_contabilidad" +
      "			    set mensaje = (case when (reporte = 'LM' or reporte = 'LMPDF') then 'DS' else 'EN' end), updated = now()" +
      "			  where co_reportes_contabilidad_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coReportesContabilidadId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static CODatosReporteData[] validarReportesEnEjecucionCont(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return validarReportesEnEjecucionCont(connectionProvider, adClientId, 0, 0);
  }

  public static CODatosReporteData[] validarReportesEnEjecucionCont(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			 select a.co_reportes_contabilidad_id as co_opcion_descarga_reporte_id" +
      "			   from co_reportes_contabilidad a, ad_ref_list b" +
      "			  where a.mensaje = b.value" +
      "			    and b.ad_reference_id = 'AFF3FEBD7B3F471CBFEF9E51A96AC7F8'" +
      "			    and a.mensaje = 'EJ'" +
      "			    and a.ad_client_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosReporteData objectCODatosReporteData = new CODatosReporteData();
        objectCODatosReporteData.coOpcionDescargaReporteId = UtilSql.getValue(result, "co_opcion_descarga_reporte_id");
        objectCODatosReporteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosReporteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosReporteData objectCODatosReporteData[] = new CODatosReporteData[vector.size()];
    vector.copyInto(objectCODatosReporteData);
    return(objectCODatosReporteData);
  }

  public static CODatosReporteData[] selectConfigATS(ConnectionProvider connectionProvider)    throws ServletException {
    return selectConfigATS(connectionProvider, 0, 0);
  }

  public static CODatosReporteData[] selectConfigATS(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT 'iva' as nombre_xml, " +
      "	           'ATS' as nombre_archivo," +
      "	           ac.tipo_informacion, " +
      "	           ac.procedimiento," +
      "	           (select ad_org_id" +
      "				  from ad_org" +
      "				 where lower(name) like lower('%S.A. MATRIZ%')" +
      "				   and issummary = 'Y'" +
      "				 limit 1)  as ad_org_id" +
      "	      from ats_anexo a," +
      "	           ats_anexo_config ac" +
      "	     where ac.ats_anexo_id = a.ats_anexo_id" +
      "	       and a.isactive = 'Y'" +
      "	     order by ac.line asc     ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosReporteData objectCODatosReporteData = new CODatosReporteData();
        objectCODatosReporteData.nombreXml = UtilSql.getValue(result, "nombre_xml");
        objectCODatosReporteData.nombreArchivo = UtilSql.getValue(result, "nombre_archivo");
        objectCODatosReporteData.tipoInformacion = UtilSql.getValue(result, "tipo_informacion");
        objectCODatosReporteData.procedimiento = UtilSql.getValue(result, "procedimiento");
        objectCODatosReporteData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCODatosReporteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosReporteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosReporteData objectCODatosReporteData[] = new CODatosReporteData[vector.size()];
    vector.copyInto(objectCODatosReporteData);
    return(objectCODatosReporteData);
  }
}
