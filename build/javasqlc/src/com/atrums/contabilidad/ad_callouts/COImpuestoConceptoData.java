//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COImpuestoConceptoData implements FieldProvider {
static Logger log4j = Logger.getLogger(COImpuestoConceptoData.class);
  private String InitRecordNumber="0";
  public String cTaxId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("c_tax_id") || fieldName.equals("cTaxId"))
      return cTaxId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COImpuestoConceptoData[] selectImpuesto(ConnectionProvider connectionProvider, String cGlitemId)    throws ServletException {
    return selectImpuesto(connectionProvider, cGlitemId, 0, 0);
  }

  public static COImpuestoConceptoData[] selectImpuesto(ConnectionProvider connectionProvider, String cGlitemId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select a.c_tax_id" +
      "		  from c_tax a, c_taxcategory b, c_glitem c" +
      "		 where a.c_taxcategory_id = b.c_taxcategory_id" +
      "		   and b.c_taxcategory_id = c.c_taxcategory_id" +
      "		   and c.c_glitem_id = ?" +
      "		 limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COImpuestoConceptoData objectCOImpuestoConceptoData = new COImpuestoConceptoData();
        objectCOImpuestoConceptoData.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectCOImpuestoConceptoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOImpuestoConceptoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COImpuestoConceptoData objectCOImpuestoConceptoData[] = new COImpuestoConceptoData[vector.size()];
    vector.copyInto(objectCOImpuestoConceptoData);
    return(objectCOImpuestoConceptoData);
  }
}
