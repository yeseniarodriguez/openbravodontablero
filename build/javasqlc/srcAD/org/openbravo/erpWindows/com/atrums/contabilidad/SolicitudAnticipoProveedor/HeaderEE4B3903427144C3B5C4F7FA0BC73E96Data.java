//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.contabilidad.SolicitudAnticipoProveedor;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data implements FieldProvider {
static Logger log4j = Logger.getLogger(HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String emCoCDoctypeId;
  public String emCoCDoctypeIdr;
  public String generatetemplate;
  public String documentno;
  public String cReturnReasonId;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String dateordered;
  public String grandtotal;
  public String datepromised;
  public String description;
  public String docaction;
  public String docactionBtn;
  public String rmReceivematerials;
  public String rmCreateinvoice;
  public String mPricelistId;
  public String docstatus;
  public String totallines;
  public String cCurrencyId;
  public String adUserId;
  public String salesrepId;
  public String cIncotermsId;
  public String incotermsdescription;
  public String freightamt;
  public String deliveryviarule;
  public String cCampaignId;
  public String mShipperId;
  public String priorityrule;
  public String chargeamt;
  public String cChargeId;
  public String cActivityId;
  public String freightcostrule;
  public String adOrgtrxId;
  public String copyfrom;
  public String copyfrompo;
  public String istaxincluded;
  public String createPolines;
  public String dropshipLocationId;
  public String dropshipBpartnerId;
  public String dropshipUserId;
  public String posted;
  public String quotationId;
  public String cCostcenterId;
  public String invoicerule;
  public String rmAddorphanline;
  public String aAssetId;
  public String isactive;
  public String soResStatus;
  public String paymentrule;
  public String deliverynotes;
  public String adClientId;
  public String cPaymenttermId;
  public String emAteccoFechaPago;
  public String rmPickfromshipment;
  public String deliveryrule;
  public String processed;
  public String mWarehouseId;
  public String user2Id;
  public String cProjectId;
  public String processing;
  public String isdiscountprinted;
  public String poreference;
  public String cBpartnerLocationId;
  public String billtoId;
  public String dateacct;
  public String dateprinted;
  public String isselected;
  public String isprinted;
  public String isinvoiced;
  public String isdelivered;
  public String issotrx;
  public String cOrderId;
  public String calculatePromotions;
  public String emAprmAddpayment;
  public String cRejectReasonId;
  public String cDoctypeId;
  public String convertquotation;
  public String deliveryLocationId;
  public String validuntil;
  public String user1Id;
  public String isselfservice;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("em_co_c_doctype_id") || fieldName.equals("emCoCDoctypeId"))
      return emCoCDoctypeId;
    else if (fieldName.equalsIgnoreCase("em_co_c_doctype_idr") || fieldName.equals("emCoCDoctypeIdr"))
      return emCoCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("generatetemplate"))
      return generatetemplate;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("docaction_btn") || fieldName.equals("docactionBtn"))
      return docactionBtn;
    else if (fieldName.equalsIgnoreCase("rm_receivematerials") || fieldName.equals("rmReceivematerials"))
      return rmReceivematerials;
    else if (fieldName.equalsIgnoreCase("rm_createinvoice") || fieldName.equals("rmCreateinvoice"))
      return rmCreateinvoice;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("incotermsdescription"))
      return incotermsdescription;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("priorityrule"))
      return priorityrule;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("freightcostrule"))
      return freightcostrule;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("copyfrompo"))
      return copyfrompo;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("create_polines") || fieldName.equals("createPolines"))
      return createPolines;
    else if (fieldName.equalsIgnoreCase("dropship_location_id") || fieldName.equals("dropshipLocationId"))
      return dropshipLocationId;
    else if (fieldName.equalsIgnoreCase("dropship_bpartner_id") || fieldName.equals("dropshipBpartnerId"))
      return dropshipBpartnerId;
    else if (fieldName.equalsIgnoreCase("dropship_user_id") || fieldName.equals("dropshipUserId"))
      return dropshipUserId;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("quotation_id") || fieldName.equals("quotationId"))
      return quotationId;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("rm_addorphanline") || fieldName.equals("rmAddorphanline"))
      return rmAddorphanline;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("deliverynotes"))
      return deliverynotes;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("em_atecco_fecha_pago") || fieldName.equals("emAteccoFechaPago"))
      return emAteccoFechaPago;
    else if (fieldName.equalsIgnoreCase("rm_pickfromshipment") || fieldName.equals("rmPickfromshipment"))
      return rmPickfromshipment;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("billto_id") || fieldName.equals("billtoId"))
      return billtoId;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("isselected"))
      return isselected;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("isinvoiced"))
      return isinvoiced;
    else if (fieldName.equalsIgnoreCase("isdelivered"))
      return isdelivered;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("em_aprm_addpayment") || fieldName.equals("emAprmAddpayment"))
      return emAprmAddpayment;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_id") || fieldName.equals("cRejectReasonId"))
      return cRejectReasonId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("convertquotation"))
      return convertquotation;
    else if (fieldName.equalsIgnoreCase("delivery_location_id") || fieldName.equals("deliveryLocationId"))
      return deliveryLocationId;
    else if (fieldName.equalsIgnoreCase("validuntil"))
      return validuntil;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.CreatedBy) as CreatedByR, " +
      "        to_char(C_Order.Updated, ?) as updated, " +
      "        to_char(C_Order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.UpdatedBy) as UpdatedByR," +
      "        C_Order.AD_Org_ID, " +
      "(CASE WHEN C_Order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_Order.EM_Co_C_Doctype_ID, " +
      "(CASE WHEN C_Order.EM_Co_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS EM_Co_C_Doctype_IDR, " +
      "C_Order.Generatetemplate, " +
      "C_Order.DocumentNo, " +
      "C_Order.C_Return_Reason_ID, " +
      "C_Order.C_BPartner_ID, " +
      "(CASE WHEN C_Order.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name2), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Order.FIN_Paymentmethod_ID, " +
      "(CASE WHEN C_Order.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "C_Order.DateOrdered, " +
      "C_Order.GrandTotal, " +
      "C_Order.DatePromised, " +
      "C_Order.Description, " +
      "C_Order.DocAction, " +
      "list1.name as DocAction_BTN, " +
      "C_Order.RM_ReceiveMaterials, " +
      "C_Order.RM_CreateInvoice, " +
      "C_Order.M_PriceList_ID, " +
      "C_Order.DocStatus, " +
      "C_Order.TotalLines, " +
      "C_Order.C_Currency_ID, " +
      "C_Order.AD_User_ID, " +
      "C_Order.SalesRep_ID, " +
      "C_Order.C_Incoterms_ID, " +
      "C_Order.Incotermsdescription, " +
      "C_Order.FreightAmt, " +
      "C_Order.DeliveryViaRule, " +
      "C_Order.C_Campaign_ID, " +
      "C_Order.M_Shipper_ID, " +
      "C_Order.PriorityRule, " +
      "C_Order.ChargeAmt, " +
      "C_Order.C_Charge_ID, " +
      "C_Order.C_Activity_ID, " +
      "C_Order.FreightCostRule, " +
      "C_Order.AD_OrgTrx_ID, " +
      "C_Order.CopyFrom, " +
      "C_Order.CopyFromPO, " +
      "COALESCE(C_Order.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "C_Order.Create_POLines, " +
      "C_Order.DropShip_Location_ID, " +
      "C_Order.DropShip_BPartner_ID, " +
      "C_Order.DropShip_User_ID, " +
      "C_Order.Posted, " +
      "C_Order.Quotation_ID, " +
      "C_Order.C_Costcenter_ID, " +
      "C_Order.InvoiceRule, " +
      "C_Order.RM_AddOrphanLine, " +
      "C_Order.A_Asset_ID, " +
      "COALESCE(C_Order.IsActive, 'N') AS IsActive, " +
      "C_Order.SO_Res_Status, " +
      "C_Order.PaymentRule, " +
      "C_Order.Deliverynotes, " +
      "C_Order.AD_Client_ID, " +
      "C_Order.C_PaymentTerm_ID, " +
      "C_Order.EM_Atecco_Fecha_Pago, " +
      "C_Order.RM_PickFromShipment, " +
      "C_Order.DeliveryRule, " +
      "COALESCE(C_Order.Processed, 'N') AS Processed, " +
      "C_Order.M_Warehouse_ID, " +
      "C_Order.User2_ID, " +
      "C_Order.C_Project_ID, " +
      "C_Order.Processing, " +
      "COALESCE(C_Order.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "C_Order.POReference, " +
      "C_Order.C_BPartner_Location_ID, " +
      "C_Order.BillTo_ID, " +
      "C_Order.DateAcct, " +
      "C_Order.DatePrinted, " +
      "COALESCE(C_Order.IsSelected, 'N') AS IsSelected, " +
      "COALESCE(C_Order.IsPrinted, 'N') AS IsPrinted, " +
      "COALESCE(C_Order.IsInvoiced, 'N') AS IsInvoiced, " +
      "COALESCE(C_Order.IsDelivered, 'N') AS IsDelivered, " +
      "COALESCE(C_Order.IsSOTrx, 'N') AS IsSOTrx, " +
      "C_Order.C_Order_ID, " +
      "C_Order.Calculate_Promotions, " +
      "C_Order.EM_APRM_AddPayment, " +
      "C_Order.C_Reject_Reason_ID, " +
      "C_Order.C_DocType_ID, " +
      "C_Order.Convertquotation, " +
      "C_Order.Delivery_Location_ID, " +
      "C_Order.validuntil, " +
      "C_Order.User1_ID, " +
      "COALESCE(C_Order.IsSelfService, 'N') AS IsSelfService, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Order left join (select AD_Org_ID, Name from AD_Org) table1 on (C_Order.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (C_Order.EM_Co_C_Doctype_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table4 on (C_Order.C_BPartner_ID = table4.C_BPartner_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table5 on (C_Order.FIN_Paymentmethod_ID = table5.FIN_Paymentmethod_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = 'FF80818130217A35013021A672400035' and list1.ad_language = ?  AND (CASE C_Order.DocAction WHEN '--' THEN 'CL' ELSE TO_CHAR(C_Order.DocAction) END) = list1.value)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data = new HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data();
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.created = UtilSql.getValue(result, "created");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.updated = UtilSql.getValue(result, "updated");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.emCoCDoctypeId = UtilSql.getValue(result, "em_co_c_doctype_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.emCoCDoctypeIdr = UtilSql.getValue(result, "em_co_c_doctype_idr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.generatetemplate = UtilSql.getValue(result, "generatetemplate");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.documentno = UtilSql.getValue(result, "documentno");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.description = UtilSql.getValue(result, "description");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.docaction = UtilSql.getValue(result, "docaction");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.docactionBtn = UtilSql.getValue(result, "docaction_btn");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.rmReceivematerials = UtilSql.getValue(result, "rm_receivematerials");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.rmCreateinvoice = UtilSql.getValue(result, "rm_createinvoice");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.totallines = UtilSql.getValue(result, "totallines");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.incotermsdescription = UtilSql.getValue(result, "incotermsdescription");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.freightamt = UtilSql.getValue(result, "freightamt");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.priorityrule = UtilSql.getValue(result, "priorityrule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.freightcostrule = UtilSql.getValue(result, "freightcostrule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.copyfrompo = UtilSql.getValue(result, "copyfrompo");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.createPolines = UtilSql.getValue(result, "create_polines");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dropshipLocationId = UtilSql.getValue(result, "dropship_location_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dropshipBpartnerId = UtilSql.getValue(result, "dropship_bpartner_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dropshipUserId = UtilSql.getValue(result, "dropship_user_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.posted = UtilSql.getValue(result, "posted");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.quotationId = UtilSql.getValue(result, "quotation_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.rmAddorphanline = UtilSql.getValue(result, "rm_addorphanline");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isactive = UtilSql.getValue(result, "isactive");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.deliverynotes = UtilSql.getValue(result, "deliverynotes");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.emAteccoFechaPago = UtilSql.getDateValue(result, "em_atecco_fecha_pago", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.rmPickfromshipment = UtilSql.getValue(result, "rm_pickfromshipment");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.processed = UtilSql.getValue(result, "processed");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.processing = UtilSql.getValue(result, "processing");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.poreference = UtilSql.getValue(result, "poreference");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.billtoId = UtilSql.getValue(result, "billto_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isselected = UtilSql.getValue(result, "isselected");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isprinted = UtilSql.getValue(result, "isprinted");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isinvoiced = UtilSql.getValue(result, "isinvoiced");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isdelivered = UtilSql.getValue(result, "isdelivered");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.issotrx = UtilSql.getValue(result, "issotrx");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.emAprmAddpayment = UtilSql.getValue(result, "em_aprm_addpayment");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cRejectReasonId = UtilSql.getValue(result, "c_reject_reason_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.convertquotation = UtilSql.getValue(result, "convertquotation");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.deliveryLocationId = UtilSql.getValue(result, "delivery_location_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.validuntil = UtilSql.getDateValue(result, "validuntil", "dd-MM-yyyy");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.language = UtilSql.getValue(result, "language");
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adUserClient = "";
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.adOrgClient = "";
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.createdby = "";
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.trBgcolor = "";
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.totalCount = "";
        objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[] = new HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[vector.size()];
    vector.copyInto(objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data);
    return(objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data);
  }

/**
Create a registry
 */
  public static HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[] set(String convertquotation, String validuntil, String aAssetId, String cOrderId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String docstatus, String docaction, String docactionBtn, String cDoctypeId, String description, String isdelivered, String isinvoiced, String isprinted, String dateordered, String datepromised, String dateacct, String salesrepId, String cPaymenttermId, String billtoId, String cCurrencyId, String invoicerule, String freightamt, String deliveryviarule, String mShipperId, String priorityrule, String totallines, String grandtotal, String mWarehouseId, String mPricelistId, String processing, String cCampaignId, String cBpartnerId, String cBpartnerIdr, String adUserId, String poreference, String cChargeId, String chargeamt, String processed, String cBpartnerLocationId, String cProjectId, String cActivityId, String quotationId, String issotrx, String dateprinted, String deliveryrule, String freightcostrule, String paymentrule, String isdiscountprinted, String posted, String postedBtn, String istaxincluded, String isselected, String emCoCDoctypeId, String emAteccoFechaPago, String cCostcenterId, String deliverynotes, String cIncotermsId, String incotermsdescription, String generatetemplate, String deliveryLocationId, String copyfrompo, String finPaymentmethodId, String dropshipUserId, String dropshipBpartnerId, String copyfrom, String dropshipLocationId, String isselfservice, String adOrgtrxId, String user2Id, String user1Id, String calculatePromotions, String rmPickfromshipment, String rmReceivematerials, String rmCreateinvoice, String cReturnReasonId, String rmAddorphanline, String soResStatus, String createPolines, String cRejectReasonId, String emAprmAddpayment)    throws ServletException {
    HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[] = new HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[1];
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0] = new HeaderEE4B3903427144C3B5C4F7FA0BC73E96Data();
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].created = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].createdbyr = createdbyr;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].updated = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].updatedTimeStamp = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].updatedby = updatedby;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].updatedbyr = updatedbyr;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].adOrgId = adOrgId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].adOrgIdr = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].emCoCDoctypeId = emCoCDoctypeId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].emCoCDoctypeIdr = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].generatetemplate = generatetemplate;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].documentno = documentno;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cReturnReasonId = cReturnReasonId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cBpartnerId = cBpartnerId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cBpartnerIdr = cBpartnerIdr;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].finPaymentmethodId = finPaymentmethodId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].finPaymentmethodIdr = "";
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dateordered = dateordered;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].grandtotal = grandtotal;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].datepromised = datepromised;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].description = description;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].docaction = docaction;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].docactionBtn = docactionBtn;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].rmReceivematerials = rmReceivematerials;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].rmCreateinvoice = rmCreateinvoice;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].mPricelistId = mPricelistId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].docstatus = docstatus;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].totallines = totallines;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cCurrencyId = cCurrencyId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].adUserId = adUserId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].salesrepId = salesrepId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cIncotermsId = cIncotermsId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].incotermsdescription = incotermsdescription;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].freightamt = freightamt;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].deliveryviarule = deliveryviarule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cCampaignId = cCampaignId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].mShipperId = mShipperId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].priorityrule = priorityrule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].chargeamt = chargeamt;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cChargeId = cChargeId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cActivityId = cActivityId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].freightcostrule = freightcostrule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].adOrgtrxId = adOrgtrxId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].copyfrom = copyfrom;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].copyfrompo = copyfrompo;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].istaxincluded = istaxincluded;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].createPolines = createPolines;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dropshipLocationId = dropshipLocationId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dropshipBpartnerId = dropshipBpartnerId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dropshipUserId = dropshipUserId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].posted = posted;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].quotationId = quotationId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cCostcenterId = cCostcenterId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].invoicerule = invoicerule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].rmAddorphanline = rmAddorphanline;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].aAssetId = aAssetId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isactive = isactive;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].soResStatus = soResStatus;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].paymentrule = paymentrule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].deliverynotes = deliverynotes;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].adClientId = adClientId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cPaymenttermId = cPaymenttermId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].emAteccoFechaPago = emAteccoFechaPago;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].rmPickfromshipment = rmPickfromshipment;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].deliveryrule = deliveryrule;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].processed = processed;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].mWarehouseId = mWarehouseId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].user2Id = user2Id;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cProjectId = cProjectId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].processing = processing;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isdiscountprinted = isdiscountprinted;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].poreference = poreference;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].billtoId = billtoId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dateacct = dateacct;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].dateprinted = dateprinted;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isselected = isselected;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isprinted = isprinted;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isinvoiced = isinvoiced;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isdelivered = isdelivered;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].issotrx = issotrx;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cOrderId = cOrderId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].calculatePromotions = calculatePromotions;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].emAprmAddpayment = emAprmAddpayment;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cRejectReasonId = cRejectReasonId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].cDoctypeId = cDoctypeId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].convertquotation = convertquotation;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].deliveryLocationId = deliveryLocationId;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].validuntil = validuntil;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].user1Id = user1Id;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].isselfservice = isselfservice;
    objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data[0].language = "";
    return objectHeaderEE4B3903427144C3B5C4F7FA0BC73E96Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2166_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2168_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2762_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef828EE0AE802C5FA1E040007F010067C7(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT fin_paymentmethod_id as defaultValue  FROM fin_paymentmethod WHERE upper(name) LIKE 'EFECTIVO' ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static int updateDocAction(ConnectionProvider connectionProvider, String docaction, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET docaction = ? " +
      "        WHERE C_Order.C_Order_ID = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for action search
 */
  public static String selectActDefM_AttributeSetInstance_ID(ConnectionProvider connectionProvider, String M_AttributeSetInstance_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Description FROM M_AttributeSetInstance WHERE isActive='Y' AND M_AttributeSetInstance_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_AttributeSetInstance_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "description");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET AD_Org_ID = (?) , EM_Co_C_Doctype_ID = (?) , Generatetemplate = (?) , DocumentNo = (?) , C_Return_Reason_ID = (?) , C_BPartner_ID = (?) , FIN_Paymentmethod_ID = (?) , DateOrdered = TO_DATE(?) , GrandTotal = TO_NUMBER(?) , DatePromised = TO_DATE(?) , Description = (?) , DocAction = (?) , RM_ReceiveMaterials = (?) , RM_CreateInvoice = (?) , M_PriceList_ID = (?) , DocStatus = (?) , TotalLines = TO_NUMBER(?) , C_Currency_ID = (?) , AD_User_ID = (?) , SalesRep_ID = (?) , C_Incoterms_ID = (?) , Incotermsdescription = (?) , FreightAmt = TO_NUMBER(?) , DeliveryViaRule = (?) , C_Campaign_ID = (?) , M_Shipper_ID = (?) , PriorityRule = (?) , ChargeAmt = TO_NUMBER(?) , C_Charge_ID = (?) , C_Activity_ID = (?) , FreightCostRule = (?) , AD_OrgTrx_ID = (?) , CopyFrom = (?) , CopyFromPO = (?) , IsTaxIncluded = (?) , Create_POLines = (?) , DropShip_Location_ID = (?) , DropShip_BPartner_ID = (?) , DropShip_User_ID = (?) , Posted = (?) , Quotation_ID = (?) , C_Costcenter_ID = (?) , InvoiceRule = (?) , RM_AddOrphanLine = (?) , A_Asset_ID = (?) , IsActive = (?) , SO_Res_Status = (?) , PaymentRule = (?) , Deliverynotes = (?) , AD_Client_ID = (?) , C_PaymentTerm_ID = (?) , EM_Atecco_Fecha_Pago = TO_DATE(?) , RM_PickFromShipment = (?) , DeliveryRule = (?) , Processed = (?) , M_Warehouse_ID = (?) , User2_ID = (?) , C_Project_ID = (?) , Processing = (?) , IsDiscountPrinted = (?) , POReference = (?) , C_BPartner_Location_ID = (?) , BillTo_ID = (?) , DateAcct = TO_DATE(?) , DatePrinted = TO_DATE(?) , IsSelected = (?) , IsPrinted = (?) , IsInvoiced = (?) , IsDelivered = (?) , IsSOTrx = (?) , C_Order_ID = (?) , Calculate_Promotions = (?) , EM_APRM_AddPayment = (?) , C_Reject_Reason_ID = (?) , C_DocType_ID = (?) , Convertquotation = (?) , Delivery_Location_ID = (?) , validuntil = TO_DATE(?) , User1_ID = (?) , IsSelfService = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createPolines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Order " +
      "        (AD_Org_ID, EM_Co_C_Doctype_ID, Generatetemplate, DocumentNo, C_Return_Reason_ID, C_BPartner_ID, FIN_Paymentmethod_ID, DateOrdered, GrandTotal, DatePromised, Description, DocAction, RM_ReceiveMaterials, RM_CreateInvoice, M_PriceList_ID, DocStatus, TotalLines, C_Currency_ID, AD_User_ID, SalesRep_ID, C_Incoterms_ID, Incotermsdescription, FreightAmt, DeliveryViaRule, C_Campaign_ID, M_Shipper_ID, PriorityRule, ChargeAmt, C_Charge_ID, C_Activity_ID, FreightCostRule, AD_OrgTrx_ID, CopyFrom, CopyFromPO, IsTaxIncluded, Create_POLines, DropShip_Location_ID, DropShip_BPartner_ID, DropShip_User_ID, Posted, Quotation_ID, C_Costcenter_ID, InvoiceRule, RM_AddOrphanLine, A_Asset_ID, IsActive, SO_Res_Status, PaymentRule, Deliverynotes, AD_Client_ID, C_PaymentTerm_ID, EM_Atecco_Fecha_Pago, RM_PickFromShipment, DeliveryRule, Processed, M_Warehouse_ID, User2_ID, C_Project_ID, Processing, IsDiscountPrinted, POReference, C_BPartner_Location_ID, BillTo_ID, DateAcct, DatePrinted, IsSelected, IsPrinted, IsInvoiced, IsDelivered, IsSOTrx, C_Order_ID, Calculate_Promotions, EM_APRM_AddPayment, C_Reject_Reason_ID, C_DocType_ID, Convertquotation, Delivery_Location_ID, validuntil, User1_ID, IsSelfService, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_NUMBER(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createPolines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Order" +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
