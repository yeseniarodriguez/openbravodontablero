//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.Anticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Anticipos8DBD2512913C43F2A8A27445BED4243BData implements FieldProvider {
static Logger log4j = Logger.getLogger(Anticipos8DBD2512913C43F2A8A27445BED4243BData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String isactive;
  public String amount;
  public String fechaAnticipo;
  public String referenceno;
  public String nroCheque;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String description;
  public String cOrderId;
  public String emAteccoBorrarAnticipo;
  public String emAteccoBorrarAnticipoBtn;
  public String adClientId;
  public String ateccoAnticipoPagoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("amount"))
      return amount;
    else if (fieldName.equalsIgnoreCase("fecha_anticipo") || fieldName.equals("fechaAnticipo"))
      return fechaAnticipo;
    else if (fieldName.equalsIgnoreCase("referenceno"))
      return referenceno;
    else if (fieldName.equalsIgnoreCase("nro_cheque") || fieldName.equals("nroCheque"))
      return nroCheque;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("em_atecco_borrar_anticipo") || fieldName.equals("emAteccoBorrarAnticipo"))
      return emAteccoBorrarAnticipo;
    else if (fieldName.equalsIgnoreCase("em_atecco_borrar_anticipo_btn") || fieldName.equals("emAteccoBorrarAnticipoBtn"))
      return emAteccoBorrarAnticipoBtn;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("atecco_anticipo_pago_id") || fieldName.equals("ateccoAnticipoPagoId"))
      return ateccoAnticipoPagoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Anticipos8DBD2512913C43F2A8A27445BED4243BData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, cOrderId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Anticipos8DBD2512913C43F2A8A27445BED4243BData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atecco_anticipo_pago.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_anticipo_pago.CreatedBy) as CreatedByR, " +
      "        to_char(atecco_anticipo_pago.Updated, ?) as updated, " +
      "        to_char(atecco_anticipo_pago.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atecco_anticipo_pago.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_anticipo_pago.UpdatedBy) as UpdatedByR," +
      "        atecco_anticipo_pago.AD_Org_ID, " +
      "COALESCE(atecco_anticipo_pago.Isactive, 'N') AS Isactive, " +
      "atecco_anticipo_pago.Amount, " +
      "atecco_anticipo_pago.Fecha_Anticipo, " +
      "atecco_anticipo_pago.Referenceno, " +
      "atecco_anticipo_pago.NRO_Cheque, " +
      "atecco_anticipo_pago.FIN_Paymentmethod_ID, " +
      "(CASE WHEN atecco_anticipo_pago.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "atecco_anticipo_pago.Description, " +
      "atecco_anticipo_pago.C_Order_ID, " +
      "atecco_anticipo_pago.EM_Atecco_Borrar_Anticipo, " +
      "list1.name as EM_Atecco_Borrar_Anticipo_BTN, " +
      "atecco_anticipo_pago.AD_Client_ID, " +
      "atecco_anticipo_pago.Atecco_Anticipo_Pago_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atecco_anticipo_pago left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table1 on (atecco_anticipo_pago.FIN_Paymentmethod_ID = table1.FIN_Paymentmethod_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = 'E4563EC48A3B477186E31E10EB34D938' and list1.ad_language = ?  AND atecco_anticipo_pago.EM_Atecco_Borrar_Anticipo = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((cOrderId==null || cOrderId.equals(""))?"":"  AND atecco_anticipo_pago.C_Order_ID = ?  ");
    strSql = strSql + 
      "        AND atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ? " +
      "        AND atecco_anticipo_pago.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atecco_anticipo_pago.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (cOrderId != null && !(cOrderId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Anticipos8DBD2512913C43F2A8A27445BED4243BData objectAnticipos8DBD2512913C43F2A8A27445BED4243BData = new Anticipos8DBD2512913C43F2A8A27445BED4243BData();
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.created = UtilSql.getValue(result, "created");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.updated = UtilSql.getValue(result, "updated");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.updatedby = UtilSql.getValue(result, "updatedby");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.isactive = UtilSql.getValue(result, "isactive");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.amount = UtilSql.getValue(result, "amount");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.fechaAnticipo = UtilSql.getDateValue(result, "fecha_anticipo", "dd-MM-yyyy");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.referenceno = UtilSql.getValue(result, "referenceno");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.nroCheque = UtilSql.getValue(result, "nro_cheque");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.description = UtilSql.getValue(result, "description");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.emAteccoBorrarAnticipo = UtilSql.getValue(result, "em_atecco_borrar_anticipo");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.emAteccoBorrarAnticipoBtn = UtilSql.getValue(result, "em_atecco_borrar_anticipo_btn");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.ateccoAnticipoPagoId = UtilSql.getValue(result, "atecco_anticipo_pago_id");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.language = UtilSql.getValue(result, "language");
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.adUserClient = "";
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.adOrgClient = "";
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.createdby = "";
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.trBgcolor = "";
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.totalCount = "";
        objectAnticipos8DBD2512913C43F2A8A27445BED4243BData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAnticipos8DBD2512913C43F2A8A27445BED4243BData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Anticipos8DBD2512913C43F2A8A27445BED4243BData objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[] = new Anticipos8DBD2512913C43F2A8A27445BED4243BData[vector.size()];
    vector.copyInto(objectAnticipos8DBD2512913C43F2A8A27445BED4243BData);
    return(objectAnticipos8DBD2512913C43F2A8A27445BED4243BData);
  }

/**
Create a registry
 */
  public static Anticipos8DBD2512913C43F2A8A27445BED4243BData[] set(String cOrderId, String emAteccoBorrarAnticipo, String emAteccoBorrarAnticipoBtn, String adClientId, String description, String isactive, String finPaymentmethodId, String ateccoAnticipoPagoId, String nroCheque, String amount, String updatedby, String updatedbyr, String createdby, String createdbyr, String referenceno, String adOrgId, String fechaAnticipo)    throws ServletException {
    Anticipos8DBD2512913C43F2A8A27445BED4243BData objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[] = new Anticipos8DBD2512913C43F2A8A27445BED4243BData[1];
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0] = new Anticipos8DBD2512913C43F2A8A27445BED4243BData();
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].created = "";
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].createdbyr = createdbyr;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].updated = "";
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].updatedTimeStamp = "";
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].updatedby = updatedby;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].updatedbyr = updatedbyr;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].adOrgId = adOrgId;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].isactive = isactive;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].amount = amount;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].fechaAnticipo = fechaAnticipo;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].referenceno = referenceno;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].nroCheque = nroCheque;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].finPaymentmethodId = finPaymentmethodId;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].finPaymentmethodIdr = "";
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].description = description;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].cOrderId = cOrderId;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].emAteccoBorrarAnticipo = emAteccoBorrarAnticipo;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].emAteccoBorrarAnticipoBtn = emAteccoBorrarAnticipoBtn;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].adClientId = adClientId;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].ateccoAnticipoPagoId = ateccoAnticipoPagoId;
    objectAnticipos8DBD2512913C43F2A8A27445BED4243BData[0].language = "";
    return objectAnticipos8DBD2512913C43F2A8A27445BED4243BData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefC1006FB798DC4F1787FEEF7FABB9F752_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEB28D0F4760B4F0493890092217DAF7C_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT atecco_anticipo_pago.C_Order_ID AS NAME" +
      "        FROM atecco_anticipo_pago" +
      "        WHERE atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atecco_anticipo_pago" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , Amount = TO_NUMBER(?) , Fecha_Anticipo = TO_DATE(?) , Referenceno = (?) , NRO_Cheque = (?) , FIN_Paymentmethod_ID = (?) , Description = (?) , C_Order_ID = (?) , EM_Atecco_Borrar_Anticipo = (?) , AD_Client_ID = (?) , Atecco_Anticipo_Pago_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ? " +
      "                 AND atecco_anticipo_pago.C_Order_ID = ? " +
      "        AND atecco_anticipo_pago.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_anticipo_pago.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenceno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroCheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorrarAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoAnticipoPagoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoAnticipoPagoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atecco_anticipo_pago " +
      "        (AD_Org_ID, Isactive, Amount, Fecha_Anticipo, Referenceno, NRO_Cheque, FIN_Paymentmethod_ID, Description, C_Order_ID, EM_Atecco_Borrar_Anticipo, AD_Client_ID, Atecco_Anticipo_Pago_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_NUMBER(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenceno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroCheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBorrarAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoAnticipoPagoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String cOrderId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atecco_anticipo_pago" +
      "        WHERE atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ? " +
      "                 AND atecco_anticipo_pago.C_Order_ID = ? " +
      "        AND atecco_anticipo_pago.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_anticipo_pago.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atecco_anticipo_pago" +
      "         WHERE atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atecco_anticipo_pago" +
      "         WHERE atecco_anticipo_pago.Atecco_Anticipo_Pago_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
