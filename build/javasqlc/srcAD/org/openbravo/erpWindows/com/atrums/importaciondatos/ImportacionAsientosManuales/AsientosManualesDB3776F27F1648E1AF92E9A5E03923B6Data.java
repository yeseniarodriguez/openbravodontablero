//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.importaciondatos.ImportacionAsientosManuales;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data implements FieldProvider {
static Logger log4j = Logger.getLogger(AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String sucursal;
  public String referencia;
  public String fecha;
  public String cuentaContable;
  public String debe;
  public String haber;
  public String tercero;
  public String descripcion;
  public String descripcionLinea;
  public String errormsg;
  public String procesar;
  public String isactive;
  public String adClientId;
  public String idtCoAsientoManualId;
  public String adOrgId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("sucursal"))
      return sucursal;
    else if (fieldName.equalsIgnoreCase("referencia"))
      return referencia;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("cuenta_contable") || fieldName.equals("cuentaContable"))
      return cuentaContable;
    else if (fieldName.equalsIgnoreCase("debe"))
      return debe;
    else if (fieldName.equalsIgnoreCase("haber"))
      return haber;
    else if (fieldName.equalsIgnoreCase("tercero"))
      return tercero;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("descripcion_linea") || fieldName.equals("descripcionLinea"))
      return descripcionLinea;
    else if (fieldName.equalsIgnoreCase("errormsg"))
      return errormsg;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("idt_co_asiento_manual_id") || fieldName.equals("idtCoAsientoManualId"))
      return idtCoAsientoManualId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(IDT_co_asiento_manual.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_co_asiento_manual.CreatedBy) as CreatedByR, " +
      "        to_char(IDT_co_asiento_manual.Updated, ?) as updated, " +
      "        to_char(IDT_co_asiento_manual.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        IDT_co_asiento_manual.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_co_asiento_manual.UpdatedBy) as UpdatedByR," +
      "        IDT_co_asiento_manual.Sucursal, " +
      "IDT_co_asiento_manual.Referencia, " +
      "IDT_co_asiento_manual.Fecha, " +
      "IDT_co_asiento_manual.Cuenta_Contable, " +
      "IDT_co_asiento_manual.Debe, " +
      "IDT_co_asiento_manual.Haber, " +
      "IDT_co_asiento_manual.Tercero, " +
      "IDT_co_asiento_manual.Descripcion, " +
      "IDT_co_asiento_manual.Descripcion_Linea, " +
      "IDT_co_asiento_manual.Errormsg, " +
      "IDT_co_asiento_manual.Procesar, " +
      "COALESCE(IDT_co_asiento_manual.Isactive, 'N') AS Isactive, " +
      "IDT_co_asiento_manual.AD_Client_ID, " +
      "IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID, " +
      "IDT_co_asiento_manual.AD_Org_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM IDT_co_asiento_manual" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID = ? " +
      "        AND IDT_co_asiento_manual.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND IDT_co_asiento_manual.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data = new AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data();
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.created = UtilSql.getValue(result, "created");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.updated = UtilSql.getValue(result, "updated");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.sucursal = UtilSql.getValue(result, "sucursal");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.referencia = UtilSql.getValue(result, "referencia");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.cuentaContable = UtilSql.getValue(result, "cuenta_contable");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.debe = UtilSql.getValue(result, "debe");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.haber = UtilSql.getValue(result, "haber");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.tercero = UtilSql.getValue(result, "tercero");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.descripcion = UtilSql.getValue(result, "descripcion");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.descripcionLinea = UtilSql.getValue(result, "descripcion_linea");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.errormsg = UtilSql.getValue(result, "errormsg");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.procesar = UtilSql.getValue(result, "procesar");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.isactive = UtilSql.getValue(result, "isactive");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.idtCoAsientoManualId = UtilSql.getValue(result, "idt_co_asiento_manual_id");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.language = UtilSql.getValue(result, "language");
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.adUserClient = "";
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.adOrgClient = "";
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.createdby = "";
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.trBgcolor = "";
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.totalCount = "";
        objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[] = new AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[vector.size()];
    vector.copyInto(objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data);
    return(objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data);
  }

/**
Create a registry
 */
  public static AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[] set(String adClientId, String errormsg, String adOrgId, String haber, String fecha, String tercero, String isactive, String idtCoAsientoManualId, String descripcionLinea, String descripcion, String referencia, String procesar, String sucursal, String debe, String createdby, String createdbyr, String updatedby, String updatedbyr, String cuentaContable)    throws ServletException {
    AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[] = new AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[1];
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0] = new AsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data();
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].created = "";
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].createdbyr = createdbyr;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].updated = "";
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].updatedTimeStamp = "";
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].updatedby = updatedby;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].updatedbyr = updatedbyr;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].sucursal = sucursal;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].referencia = referencia;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].fecha = fecha;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].cuentaContable = cuentaContable;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].debe = debe;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].haber = haber;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].tercero = tercero;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].descripcion = descripcion;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].descripcionLinea = descripcionLinea;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].errormsg = errormsg;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].procesar = procesar;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].isactive = isactive;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].adClientId = adClientId;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].idtCoAsientoManualId = idtCoAsientoManualId;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].adOrgId = adOrgId;
    objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data[0].language = "";
    return objectAsientosManualesDB3776F27F1648E1AF92E9A5E03923B6Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDefBEC6CDBB663F4B7190D703C887C5EB84_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC751FB9AAC9D4925A9EBE29127BDC743_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE IDT_co_asiento_manual" +
      "        SET Sucursal = (?) , Referencia = (?) , Fecha = TO_DATE(?) , Cuenta_Contable = (?) , Debe = TO_NUMBER(?) , Haber = TO_NUMBER(?) , Tercero = (?) , Descripcion = (?) , Descripcion_Linea = (?) , Errormsg = (?) , Procesar = (?) , Isactive = (?) , AD_Client_ID = (?) , IDT_Co_Asiento_Manual_ID = (?) , AD_Org_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID = ? " +
      "        AND IDT_co_asiento_manual.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_co_asiento_manual.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sucursal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cuentaContable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, debe);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, haber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcionLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, errormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtCoAsientoManualId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtCoAsientoManualId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO IDT_co_asiento_manual " +
      "        (Sucursal, Referencia, Fecha, Cuenta_Contable, Debe, Haber, Tercero, Descripcion, Descripcion_Linea, Errormsg, Procesar, Isactive, AD_Client_ID, IDT_Co_Asiento_Manual_ID, AD_Org_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_DATE(?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sucursal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cuentaContable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, debe);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, haber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcionLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, errormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtCoAsientoManualId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM IDT_co_asiento_manual" +
      "        WHERE IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID = ? " +
      "        AND IDT_co_asiento_manual.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_co_asiento_manual.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM IDT_co_asiento_manual" +
      "         WHERE IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM IDT_co_asiento_manual" +
      "         WHERE IDT_co_asiento_manual.IDT_Co_Asiento_Manual_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
